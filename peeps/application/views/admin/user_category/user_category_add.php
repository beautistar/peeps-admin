<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body with-border">
        <div class="col-md-6">
          <h4><i class="fa fa-plus"></i> &nbsp; Add New User Category</h4>
        </div>
        <div class="col-md-6 text-right">
          <a href="<?= base_url('admin/user_category'); ?>" class="btn btn-success"><i class="fa fa-list"></i>User Category List</a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
			<?php if(isset($msg) || validation_errors() !== ''): ?>
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-warning"></i> Alert!</h4>
					<?= validation_errors();?>
					<?= isset($msg)? $msg: ''; ?>
				</div>
            <?php endif; ?>
            <?php echo form_open_multipart(base_url('admin/user_category/add'), 'class="form-horizontal"');  ?> 
            <?php //echo form_open_multipart(base_url('admin/video/video_upload'), 'class="form-horizontal"');  ?> 
				<div class="form-group">
					<div class="col-sm-3">
						<label for="user_category_name" control-label">Category Name</label>
					</div>	
					<div class="col-sm-9">
						<input type="text" name="user_category_name" class="form-control" id="user_category_name" placeholder="">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-3">Access to remote playlist or not</div>
					<div class="col-sm-9">
						<input type="radio" name="user_category_check" value="Yes" checked> 	Yes &nbsp;
						<input type="radio" name="user_category_check" value="No"> No &nbsp;
					</div>
				</div>
				<div class="form-group">
                   <label for="role" class="col-sm-3 control-label">Discount</label>
                   <div class="col-sm-9">
				      <input type="text" name="user_discount" class="form-control" id="user_discount" placeholder="">
                   </div>
                </div>
				
				<div class="form-group">
					<div class="col-md-11">
					  <input type="submit" name="submit" value="Add User category" class="btn btn-info pull-right">
					</div>
				</div>
            <?php echo form_close( ); ?>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 