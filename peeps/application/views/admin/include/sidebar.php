<?php
$cur_tab = $this->uri->segment(2)==''?'dashboard': $this->uri->segment(2);
?>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url() ?>public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= ucwords($this->session->userdata('name')); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php /*<form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> */?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->


    <ul class="sidebar-menu">
        <li id="users" class="treeview">
            <a href="#">
              <i class="fa fa-users" aria-hidden="true"></i><span>Users</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="add_user"><a href="<?= base_url('admin/users/add'); ?>"><i class="fa fa-user-plus" aria-hidden="true"></i> Add User</a></li>
              <li id="view_users" class=""><a href="<?= base_url('admin/users'); ?>"><i class="fa fa-eye" aria-hidden="true"></i>View Users</a></li>
             <?php /* <li id="user_history" class=""><a href="<?= base_url('admin/users'); ?>"><i class="fa fa-eye" aria-hidden="true"></i>User Watched History</a></li> */?>
			  <?php /*<li id="user_group" class=""><a href="<?= base_url('admin/group'); ?>"><i class="fa fa-circle-o"></i> User Groups</a></li> */?>
			  	<li id="users" class="treeview">
					<a href="#">
					 <i class="fa fa-circle-o"></i><span>Users Category</span>
					  <span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					  </span>
					</a>
					<ul class="treeview-menu"> 
					  <li id="add_category"><a href="<?= base_url('admin/user_category/add'); ?>"><i class="fa fa-plus-circle" aria-hidden="true"></i> Create Category</a></li>
					  <li id="view_category" class=""><a href="<?= base_url('admin/user_category'); ?>"><i class="fa fa-eye" aria-hidden="true"></i> View Category</a></li>
					 <!-- <li id="add_user"><a href="<?= base_url('admin/subcategory/add'); ?>"><i class="fa fa-circle-o"></i> Add Sub Category</a></li>
					  <li id="view_users" class=""><a href="<?= base_url('admin/subcategory'); ?>"><i class="fa fa-circle-o"></i> View Sub Category</a></li>-->
					</ul>
				</li>
            </ul>
          </li>
      </ul>
	  <ul class="sidebar-menu">
        <li id="users" class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Category</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="add_category"><a href="<?= base_url('admin/category/add'); ?>"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Category</a></li>
              <li id="view_category" class=""><a href="<?= base_url('admin/category'); ?>"><i class="fa fa-eye" aria-hidden="true"></i> View Category</a></li>
              <li id="add_subcategory"><a href="<?= base_url('admin/subcategory/add'); ?>"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Sub Category</a></li>
              <li id="view_subcategory" class=""><a href="<?= base_url('admin/subcategory'); ?>"><i class="fa fa-eye" aria-hidden="true"></i> View Sub Category</a></li>
            </ul>
          </li>
      </ul>
	  <?php /* ?><ul class="sidebar-menu"!-->
        <li id="users" class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Sub Category</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="add_user"><a href="<?= base_url('admin/subcategory/add'); ?>"><i class="fa fa-circle-o"></i> Add Sub Category</a></li>
              <li id="view_users" class=""><a href="<?= base_url('admin/subcategory'); ?>"><i class="fa fa-circle-o"></i> View Sub Category</a></li>
            </ul>
          </li>
      </ul><?php */ ?>

    <ul class="sidebar-menu">
      <li id="video" class="treeview">
        <a href="#">
          <i class="fa fa-music" aria-hidden="true"></i>
          <span>Playlist</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li id="view_videos" class=""><a href="<?= base_url('admin/videos_listing/playlists/'); ?>"><i class="fa fa-eye" aria-hidden="true"></i> View Playlist</a></li>
        </ul>
      </li>
    </ul>
	  <ul class="sidebar-menu">
        <li id="users" class="treeview">
            <a href="#">
              <i class="fa fa-video-camera" aria-hidden="true"></i> <span>Videos</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
           <?php /* <ul class="treeview-menu">
              <li id="view_videos" class="">
                  <a href="<?= base_url('admin/videos'); ?>">
                      <i class="fa fa-circle-o"></i>Videos
                  </a>				  
              </li>
            </ul>
			<ul class="treeview-menu">
              <li id="view_videos" class="">
                  <a href="<?= base_url('admin/videos/add'); ?>">
                      <i class="fa fa-circle-o"></i>Add Video
                  </a>				  
              </li>
            </ul>*/?>
			<ul class="treeview-menu">
              <li id="view_videos" class="">
                  <a href="<?= base_url('admin/tvshows_videos'); ?>">
                      <i class="fa fa-television" aria-hidden="true"></i>TV Shows Videos
                  </a>				  
              </li>
            </ul>
			<ul class="treeview-menu">
              <li id="view_videos" class="">
                  <a href="<?= base_url('admin/movies'); ?>">
                      <i class="fa fa-film" aria-hidden="true"></i>Movies Videos
                  </a>				  
              </li>
            </ul>
          </li>
      </ul>
      <ul class="sidebar-menu">
          <li id="users-notifications" class="treeview">
              <a href="#">
                <i class="fa fa-bell" aria-hidden="true"></i><span>Notifications</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li id="view_videos" class="">
                    <a href="<?= base_url('admin/notifications'); ?>">
                        <i class="fa fa-list" aria-hidden="true"></i> Notifications List
                    </a>
                    <li id="view_videos" class="">
                        <a href="<?= base_url('admin/notifications/add'); ?>">
                            <i class="fa fa-share-square-o" aria-hidden="true"></i> Send Notifications
                        </a>
                    </li> 
					<?php /*<li id="view_videos" class="">
                        <a href="<?= base_url('admin/presaved_notifications'); ?>">
                            <i class="fa fa-bookmark" aria-hidden="true"></i> Pre-saved Notifications
                        </a>
                    </li>*/?>
                </li>
              </ul>
            </li>
        </ul>
		<?php /*<ul class="sidebar-menu">
        <li id="users" class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Users Category</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu"> 
              <li id="add_user"><a href="<?= base_url('admin/user_category/add'); ?>"><i class="fa fa-circle-o"></i>Create Category</a></li>
              <li id="view_users" class=""><a href="<?= base_url('admin/user_category'); ?>"><i class="fa fa-circle-o"></i>View Category</a></li>
             <!-- <li id="add_user"><a href="<?= base_url('admin/subcategory/add'); ?>"><i class="fa fa-circle-o"></i> Add Sub Category</a></li>
              <li id="view_users" class=""><a href="<?= base_url('admin/subcategory'); ?>"><i class="fa fa-circle-o"></i> View Sub Category</a></li>-->
            </ul>
          </li>
      </ul> */?>



<?php /* ?>
      <ul class="sidebar-menu">
        <li id="export" class="treeview">
            <a href="#">
              <i class="fa fa-life-ring"></i> <span>Backup & Export</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class=""><a href="<?= base_url('admin/export'); ?>"><i class="fa fa-circle-o"></i> Database Backup </a></li>
            </ul>
          </li>
      </ul>

      <ul class="sidebar-menu">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Multilevel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            <li>
              <a href="#"><i class="fa fa-circle-o"></i> Level One
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>

        <li><a target="_blank" href="../documentation_adminlte/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>

      </ul>

	  <?php */ ?>


    </section>
    <!-- /.sidebar -->
  </aside>


<script>
  $("#<?= $cur_tab; ?>").addClass('active');
</script>
