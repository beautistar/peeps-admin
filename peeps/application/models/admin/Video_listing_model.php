<?php
	class Video_listing_model extends CI_Model{

		public function add_video_list($data){
			$this->db->insert('ci_video_list', $data);
			return true;
		}
		
		public function get_videos(){
			$this->db->select ('ci_users.firstname,ci_users.lastname,ci_video.*,ci_video_list.*');
			$this->db->from('ci_video');
			$this->db->join('ci_users','ci_users.id = ci_video.user_id');
			$this->db->join('ci_video_list', 'ci_video_list.id = ci_video.playlist_id');
			$this->db->where(array('ci_video.id !=' => NULL));
			$query = $this->db->get();  
			$last = $this->db->last_query();
			$result = $query->result_array();	
			return $result;
		}
		
		public function get_listing_by_id($id){
			$this->db->select('ci_video.*, ci_video_list.*');
			$this->db->join('ci_video','ci_video.user_id = ci_video_list.user_id');
		//	$this->db->join('ci_video_list','ci_video_list.id = ci_video.playlist_id');
			$this->db->where('ci_video_list.id', $id);
			$query = $this->db->get('ci_video_list');
			$last = $this->db->last_query();
			$result = $query->result_array();
			return $result;	
		}
		
		public function get_all_videoslist(){
			//$this->db->where('is_admin', 0);
			$query = $this->db->get('ci_video_list');
			return $result = $query->result_array();
		}
		
		
		public function get_all_videoslist_by_user($id){
			$this->db->where('user_id', $id);
			$query = $this->db->get('ci_video_list');
			$last = $this->db->last_query();
			$result = $query->result_array();
			return $result;
		}	
		
		
	/* 	public function listing_by_user_id($id){
			$this->db->select('ci_subcategory.subcategory_name,ci_video.*,ci_video_list.video_title,ci_category.category_name');
			$this->db->from('ci_video_list');
			$this->db->join('ci_video', 'ci_video.playlist_id = ci_video_list.id',"INNER");
			$this->db->join('ci_category', 'ci_category.id = ci_video.category_id',"LEFT");
			$this->db->join('ci_subcategory', 'ci_subcategory.id = ci_video.subcategory_id',"RIGHT");
			$this->db->where('ci_video_list.user_id', $id);
			$query = $this->db->get();
			$result = $query->result_array();
			return $result;
		}	 */
		
		
		public function listing_by_user_id($id){
			$this->db->select ('ci_category.category_name,ci_video.video_description,ci_video.video_upload,ci_video.file_type,ci_subcategory.subcategory_name,ci_video_list.*');
			$this->db->from('ci_video');
			$this->db->join('ci_category','ci_category.id = ci_video.category_id',"LEFT");
			$this->db->join('ci_subcategory','ci_subcategory.id = ci_video.subcategory_id',"LEFT");
			$this->db->join('ci_video_list', 'ci_video_list.id = ci_video.playlist_id');
			$this->db->where('ci_video.playlist_id', $id);
			$query = $this->db->get();  
			$last = $this->db->last_query();
			$result = $query->result_array();	
			return $result;
		}
		
		public function edit_playlist($data, $id){
			$this->db->where('id', $id);
			$this->db->update('ci_video_list', $data);
			return true;
		}
		
		public function get_videos_by_playlistid($id){
			$this->db->select ('ci_subcategory.subcategory_name,ci_video.*,ci_category.category_name');
			$this->db->from('ci_video');
			$this->db->join('ci_category', 'ci_category.id = ci_video.category_id');
			$this->db->join('ci_subcategory', 'ci_subcategory.id = ci_video.subcategory_id');
			$this->db->where('ci_video.playlist_id', $id);
			$query = $this->db->get();
			$last = $this->db->last_query();
			return $result = $query->row_array();
		}
		
		public function edit_video($data, $id){
			$this->db->select('video_upload');
			$this->db->from('ci_video');
			$this->db->where('id', $id);
			$query = $this->db->get();
			$last = $this->db->last_query();
			$result = $query->row_array();
			$image = $result['video_upload'];
			if($result>0){
				unlink('uploads/videos/'.$image);
				$this->db->where('id', $id);
				$this->db->update('ci_video', $data);
				return true;
			}else{
				$this->db->where('id', $id);
				$this->db->update('ci_video', $data);
				return true;
			}
		}
		
		public function get_user_id($id){
			$this->db->select('user_id');
			$this->db->from('ci_video_list');
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $result = $query->row_array();
		}
	}

?>