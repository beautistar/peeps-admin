-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 30, 2019 at 03:44 PM
-- Server version: 5.7.21-20-beget-5.7.21-20-1-log
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uaecono7_adminlt`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_category`
--
-- Creation: Jan 24, 2019 at 10:00 AM
-- Last update: Jan 28, 2019 at 02:29 PM
--

DROP TABLE IF EXISTS `ci_category`;
CREATE TABLE `ci_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(200) NOT NULL,
  `subcatid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_category`
--

INSERT INTO `ci_category` (`id`, `category_name`, `subcatid`) VALUES
(1, 'News', 1),
(2, 'Educations', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ci_subcategory`
--
-- Creation: Jan 24, 2019 at 06:31 AM
-- Last update: Jan 29, 2019 at 08:29 AM
--

DROP TABLE IF EXISTS `ci_subcategory`;
CREATE TABLE `ci_subcategory` (
  `id` int(11) NOT NULL,
  `subcategory_name` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_subcategory`
--

INSERT INTO `ci_subcategory` (`id`, `subcategory_name`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Fashion', 1, '2019-01-23 03:01:34', '2019-01-23 04:01:52'),
(2, 'History', 2, '2019-01-24 04:01:47', '2019-01-24 04:01:47'),
(3, 'Sports', 1, '2019-01-24 04:01:10', '2019-01-24 04:01:10'),
(4, 'Development', 2, '2019-01-28 05:01:25', '2019-01-28 05:01:25'),
(5, 'Development', 2, '2019-01-29 11:01:49', '2019-01-29 11:01:49');

-- --------------------------------------------------------

--
-- Table structure for table `ci_users`
--
-- Creation: Jan 29, 2019 at 04:49 AM
-- Last update: Jan 29, 2019 at 07:06 PM
--

DROP TABLE IF EXISTS `ci_users`;
CREATE TABLE `ci_users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `photo_url` varchar(255) NOT NULL,
  `role` tinyint(4) DEFAULT '1',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_users`
--

INSERT INTO `ci_users` (`id`, `username`, `firstname`, `lastname`, `email`, `mobile_no`, `password`, `photo_url`, `role`, `is_active`, `is_admin`, `last_ip`, `created_at`, `updated_at`) VALUES
(3, 'Admin', 'admin', 'admin', 'admin@admin.com', '12345', '$2y$10$kqiwRiCcIBsS/i0FC9k3YOE.sSVgu/PKCcO.baV8T4EDru4.qMXrS', '', 1, 1, 1, '', '2017-09-29 10:09:44', '2017-10-17 10:10:55'),
(15, 'demo3', 'test', 'test', 'test2@gmai.com', '12345', '$2y$10$1rzqcv/3K1EUlF2J1zgDiuS3nIN6/YLZ1ABLuMiZysFDTCkcyJuTC', '', 1, 0, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:04'),
(16, 'demo4', 'test', 'test', 'test3@gmai.com', '12345', '$2y$10$kqiwRiCcIBsS/i0FC9k3YOE.sSVgu/PKCcO.baV8T4EDru4.qMXrS', '', 2, 1, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:12'),
(17, 'demo5', 'test', 'test', 'test4@gmai.com', '12345', '$2y$10$iQW7TKdrHNnl3jDtjYBadOxTuWRBdBcZhk2lxZ2lrIX1M4uJu6pFO', '', 4, 1, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:17'),
(18, 'demo6', 'test', 'test', 'test5@gmai.com', '12345', '$2y$10$UEkLtiOP8Lt13vwC8KXsKOOJMnWukPPP7L/NJIpFn49rQKuA6oXD6', '', 1, 1, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:24'),
(19, 'demo7', 'test', 'test', 'test6@gmai.com', '12345', '$2y$10$GSVyEzAbMjdhONCVPevbGOMAjzSGpPk62pg58W8DtG4PCSdTF5Ooy', '', 1, 0, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:30'),
(20, 'demo8', 'test', 'test', 'test7@gmai.com', '12345', '$2y$10$6415cSa21VXXyD3vsaPwyepPyaDpPMgOJkPbZMt/AtKvNx5hRxbLy', '', 2, 1, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:39'),
(21, 'demo9', 'test', 'test', 'test8@gmai.com', '12345', '$2y$10$.4.73sLxhwRHYZoSxbFTMu/iaKHgDYJqz9Lx3js6dmlJxZMuPG8AG', '', 1, 0, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:47'),
(22, 'demo10', 'test', 'test', 'test9@gmai.com', '12345', '$2y$10$.Rr.YeXQ/M4QJ031O3r2k.Tc2ztOvRfCsG2EjYUxY.KZ96WjwR57O', '', 1, 1, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:53'),
(23, 'demo11', 'test', 'test', 'test10@gmai.com', '12345', '$2y$10$cyuI4z5NWt/S1X9Z3vOgWuDKm/ZJGOPy72kZHrrOP0TisXobhaIri', '', 2, 1, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:00'),
(24, 'demo12', 'test', 'test', 'test1@gmai.com', '12345', '$2y$10$JlHihhr5LiGtx7rwZYbLLefdmi3JACN/yDkoJKWWESwELAGFeWOWe', '', 4, 1, 0, '', '2017-10-12 07:10:10', '2017-10-13 10:10:22'),
(25, 'rizwan', 'rizwan', 'shahid', 'rizwan@gmail.com', '12345', '$2y$10$DAqm4Fd9ptlTeKybHqdie.dsJcZVM54Gd7iN7FHBzC8WU.NS5Gczq', '', 4, 1, 0, '', '2017-10-13 09:10:37', '2019-01-22 09:01:19'),
(41, '123123', '123', '123', '123@gmail.com', NULL, '$2y$10$ta7buBLFEdFkWxLu59SI5.yYEZO4V5Palzq2Ylu/HH6XKXACLEwBS', 'http://project-9.uaecono7.beget.tech/uploadfiles/2019/01/20190115487859521.jpg', 1, 1, 0, '', '2019-01-29 09:01:12', '0000-00-00 00:00:00'),
(29, '', 'test', '1', 'test100@user.com', '', '$2y$10$hUQmUmGIFXfPbWLQI.fK5.ihrib.j6gDBrdCmNEHUsPa5KgpV3Mfa', 'http://project-9.uaecono7.beget.tech/uploadfiles/2019/01/20190115484285573.jpg', 1, 1, 0, '', '2019-01-25 06:01:37', '0000-00-00 00:00:00'),
(30, '', 'test', '200', 'test200@user.com', '', '$2y$10$55M3fv5BmCnFQaAq5JEGoerVsMfxWF.KkHDWKCwla302SRBtfW6OO', 'http://project-9.uaecono7.beget.tech/uploadfiles/2019/01/20190115484362117.jpg', 1, 1, 0, '', '2019-01-25 08:01:11', '0000-00-00 00:00:00'),
(31, '', 'test1', '11', 'test101@user.com', '', '$2y$10$7xtb5pPQv4L8bGcZYuNaUufkzp6lxfm6MFuJP1EiweG8/edzkfwxe', 'http://project-9.uaecono7.beget.tech/uploadfiles/2019/01/20190115484392545.png', 1, 1, 0, '', '2019-01-25 09:01:54', '0000-00-00 00:00:00'),
(32, '', 'dhanveer', 'thakur', 'test111@gmail.com', '', '$2y$10$r0Jr7Hyqttk74jdxDjXeP.fMxDisiv0zVf24jIL0hKxvfqP9mO1eu', 'http://project-9.uaecono7.beget.tech/uploadfiles/2019/01/20190115484936386.jpg', 1, 1, 0, '', '2019-01-26 12:01:18', '0000-00-00 00:00:00'),
(33, '', 'dhanveer', 'thakur', 'dhanveer@gmail.com', '', '$2y$10$O2Wlx5Jkx2wD699/b5Br5e3qOIJ9hX3LkepAOWPuEMOTkvjI9HnyK', 'http://project-9.uaecono7.beget.tech/uploadfiles/2019/01/20190115484940786.jpg', 1, 1, 0, '', '2019-01-26 12:01:38', '0000-00-00 00:00:00'),
(34, '', 'dhanveer', 'thakur', 'dhanveer5@gmail.com', '', '$2y$10$fJw78MjatcoybNLDVNYB5e8DdWvpt6HrhHXx.HFZkzEUSZonFCvsi', 'http://project-9.uaecono7.beget.tech/uploadfiles/2019/01/20190115484949330.jpg', 1, 1, 0, '', '2019-01-26 12:01:53', '0000-00-00 00:00:00'),
(35, '', 'yin', '123', 'yin@user.com', '', '$2y$10$mqfAcSsr7UhSVrIHQruYnek3arqOmtOYzWCcweH49mJbO9bLtMHiG', 'http://project-9.uaecono7.beget.tech/uploadfiles/2019/01/20190115486634423.jpg', 1, 1, 0, '', '2019-01-28 11:01:22', '0000-00-00 00:00:00'),
(40, 'Yousif Bassit', 'Yousif ', 'Bassit', 'Yousifbassit@gmail.com', NULL, '$2y$10$Sfdh7aUB.R99ncNGcz26WOntLi5vNAsYsZutVb7RCWR2QIOSci7C.', 'http://project-9.uaecono7.beget.tech/uploadfiles/2019/01/20190115487594711.jpg', 1, 1, 0, '', '2019-01-29 01:01:51', '0000-00-00 00:00:00'),
(37, '', 'test', '300', 'test300@user.com', '', '$2y$10$bPVRkXEwe8ZBMZXrjLj4OuMsjJbvcKn0QzpNDnLogkW3CVogSBOee', 'http://project-9.uaecono7.beget.tech/uploadfiles/2019/01/20190115486827263.jpg', 1, 1, 0, '', '2019-01-28 04:01:46', '0000-00-00 00:00:00'),
(39, 'abc@gmail.com', 'testing', 'test', 'abc@gmail.com', NULL, '$2y$10$H/gpOKZZ9w1OytozngQYsevCETRAlNJkiVG2Eq9x9Y.IOLcJn/d3q', '', 1, 1, 0, '', '2019-01-29 09:01:43', '2019-01-29 09:01:43'),
(42, '111', '1', '11', '1@gmail.com', NULL, '$2y$10$FZv8EgZKRFWAaTuy7qLdRekNNTUEcozYVkGz09TxVka771Od.3K4O', 'http://project-9.uaecono7.beget.tech/uploadfiles/2019/01/20190115487871904.png', 1, 1, 0, '', '2019-01-29 09:01:50', '0000-00-00 00:00:00'),
(43, 'MNmom', 'MN', 'mom', 'mm@gmail.com', NULL, '$2y$10$I9NJ9tVYUNm95VCsfKH2R.IjEU15aJ4ev9HaPtGL4/62xYTaiHO0K', 'http://project-9.uaecono7.beget.tech/uploadfiles/2019/01/20190115487887816.jpg', 1, 1, 0, '', '2019-01-29 10:01:21', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ci_user_groups`
--
-- Creation: Jan 18, 2019 at 11:52 AM
-- Last update: Jan 18, 2019 at 11:52 AM
--

DROP TABLE IF EXISTS `ci_user_groups`;
CREATE TABLE `ci_user_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_user_groups`
--

INSERT INTO `ci_user_groups` (`id`, `group_name`) VALUES
(1, 'member'),
(2, 'customer'),
(4, 'accountant');

-- --------------------------------------------------------

--
-- Table structure for table `ci_video`
--
-- Creation: Jan 29, 2019 at 10:53 AM
-- Last update: Jan 30, 2019 at 11:06 AM
--

DROP TABLE IF EXISTS `ci_video`;
CREATE TABLE `ci_video` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `video_title` varchar(200) NOT NULL,
  `video_description` varchar(400) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `video_upload` varchar(500) NOT NULL,
  `file_type` text NOT NULL,
  `playlist_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_video`
--

INSERT INTO `ci_video` (`id`, `user_id`, `video_title`, `video_description`, `category_id`, `subcategory_id`, `video_upload`, `file_type`, `playlist_id`, `created_at`, `updated_at`) VALUES
(15, 32, 'test1111', 'test111', 2, 2, 'https://www.youtube.com/watch?v=G8o8u7OtuZY', 'URL', 10, '2019-01-29', '2019-01-30'),
(16, 33, 'list111', 'list111', 1, 1, 'https://www.youtube.com/watch?v=G8o8u7OtuZY', 'URL', 14, '2019-01-29', '2019-01-30'),
(17, 34, 'test33', 'test33', 1, 2, 'https://www.youtube.com/watch?v=G8o8u7OtuZY', 'URL', 9, '2019-01-29', '2019-01-30'),
(19, 35, 'test video 2', 'test2', 1, 1, 'http://web24.live:8080/get.php?username=2493496367&password=xrhJ1Dgy9U&type=m3u_plus&output=ts', 'URL', 15, '2019-01-30', '2019-01-30'),
(20, 33, 'test video with URL', 'This is test with URL', 2, 4, 'http://web24.live:8080/get.php?username=2493496367&password=xrhJ1Dgy9U&type=m3u_plus&output=ts', 'URL', 13, '2019-01-30', '2019-01-30'),
(21, 33, 'test video file', 'This is file', 1, 1, 'http://project-9.uaecono7.beget.tech/uploads/videos/1548846098.mp4', 'File', 13, '2019-01-30', '2019-01-30'),
(22, 32, 'My Test', 'My Test', 1, 3, 'http://project-9.uaecono7.beget.tech/uploads/videos/1548845029.mp4', 'File', 10, '2019-01-30', '2019-01-30'),
(23, 35, 'test video file', 'A', 2, 4, 'http://project-9.uaecono7.beget.tech/uploads/videos/1548845999.MP4', 'File', 15, '2019-01-30', '2019-01-30');

-- --------------------------------------------------------

--
-- Table structure for table `ci_video_list`
--
-- Creation: Jan 24, 2019 at 08:23 AM
-- Last update: Jan 30, 2019 at 08:01 AM
--

DROP TABLE IF EXISTS `ci_video_list`;
CREATE TABLE `ci_video_list` (
  `id` int(11) NOT NULL,
  `video_title` varchar(200) NOT NULL,
  `videos` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_video_list`
--

INSERT INTO `ci_video_list` (`id`, `video_title`, `videos`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 'Playlist1', 'test1', 25, '2019-01-24', '2019-01-29'),
(3, 'Playlist2', 'test3', 15, '2019-01-24', '2019-01-24'),
(4, 'Playlist3', 'test3', 16, '2019-01-24', '2019-01-24'),
(5, 'Playlist4', 'test4', 17, '2019-01-24', '2019-01-24'),
(7, 'list-2', '', 29, '2019-01-25', '0000-00-00'),
(8, 'list-1', '', 29, '2019-01-26', '0000-00-00'),
(9, 'list-5', '', 34, '2019-01-26', '2019-01-29'),
(10, 'tre', '', 32, '2019-01-26', '0000-00-00'),
(12, 'test', '', 33, '2019-01-26', '0000-00-00'),
(13, 'testddddd', '', 33, '2019-01-26', '0000-00-00'),
(15, 'remote', '', 35, '2019-01-28', '0000-00-00'),
(16, 'test', '', 36, '2019-01-28', '0000-00-00'),
(17, '123', '', 12, '2019-01-29', '0000-00-00'),
(18, 'qer', '', 43, '2019-01-29', '0000-00-00'),
(19, 'gffdx', '', 33, '2019-01-30', '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_category`
--
ALTER TABLE `ci_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_subcategory`
--
ALTER TABLE `ci_subcategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `ci_users`
--
ALTER TABLE `ci_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_user_groups`
--
ALTER TABLE `ci_user_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_video`
--
ALTER TABLE `ci_video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `ci_video_list`
--
ALTER TABLE `ci_video_list`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ci_category`
--
ALTER TABLE `ci_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ci_subcategory`
--
ALTER TABLE `ci_subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ci_users`
--
ALTER TABLE `ci_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `ci_user_groups`
--
ALTER TABLE `ci_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ci_video`
--
ALTER TABLE `ci_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `ci_video_list`
--
ALTER TABLE `ci_video_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
