<?php
class Movie_model extends CI_Model{
	
	public function add($data){	
		$this->db->insert('ci_movies', $data);
		return true;
	}
	

	public function get_all_movies(){
	/* 	$query = $this->db->get('ci_movies');
		return $result = $query->result_array(); */
		$this->db->select('ci_subcategory.subcategory_name, ci_movies.*, ci_category.category_name');
		$this->db->from('ci_movies');
		$this->db->join('ci_category','ci_category.id = ci_movies.category_id',"LEFT");
		$this->db->join('ci_subcategory','ci_subcategory.id = ci_movies.subcategory_id',"LEFT");
		$query = $this->db->get();
		$last = $this->db->last_query();
		return $result = $query->result_array();
	}
	
	public function get_all_episodes(){
		$this->db->select('ci_episode.*,ci_tv_series.series_name');
		$this->db->from('ci_episode');
		$this->db->join('ci_tv_series','ci_tv_series.id = ci_episode.season_id',"LEFT");
		$this->db->order_by("ci_episode.name", "desc");
		$query = $this->db->get();
		$last = $this->db->last_query();
		return $result = $query->result_array();
	}
	
	public function get_seasons_by_id($id){
		$this->db->select("*");
		$this->db->from('ci_tv_series');
		$this->db->where('ci_tv_series.id',$id);
		$query = $this->db->get();
		$last = $this->db->last_query();
		return $result = $query->result_array();
		
	}
	
	public function get_movies_by_id($id){
		$this->db->select('*');
		$this->db->from('ci_movies');
		$this->db->where('ci_movies.id', $id);
		$query = $this->db->get();
		return $result = $query->result_array();
	}
	
	public function edit_movie($data, $id){
		$this->db->where('id', $id);
		$this->db->update('ci_movies', $data);
		return true;
		
	}
	
	/* public function get_all_subcategories(){
		$this->db->select('*');
		$this->db->from('ci_subcategory');
		$this->db->join('ci_subcategory','ci_subcategory.id = ci_movies.subcategory_id');
		$query = $this->db->get();
		$last = $this->db->last_query();
		return $result = $query->result_array();
	} */
}



?>