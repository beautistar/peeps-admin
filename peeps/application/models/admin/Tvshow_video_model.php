<?php
class Tvshow_video_model extends CI_Model{
	public function add_season($data){	
		$this->db->insert('ci_tv_series', $data);
		return true;
	}
	

	public function get_all_seasons(){
		$query = $this->db->get('ci_tv_series');
		return $result = $query->result_array();
	}
	
	public function get_all_seasons_by_id($id){
		$this->db->select('ci_episode.*, ci_tv_series.series_name');
		$this->db->from('ci_episode');
		$this->db->join('ci_tv_series','ci_tv_series.id = ci_episode.series_id',"LEFT");
		$this->db->where('ci_tv_series.id', $id);
		$query = $this->db->get();
		$last = $this->db->last_query();
		return $result = $query->result_array();
	}
	
	public function get_all_seasons_id($id){
		$this->db->select('ci_subcategory.subcategory_name,ci_tv_series.*,ci_category.category_name');
		$this->db->from('ci_tv_series');
		$this->db->join('ci_category','ci_category.id = ci_tv_series.category_id',"LEFT");
		$this->db->join('ci_subcategory','ci_subcategory.id = ci_tv_series.subcategory_id',"LEFT");
		$this->db->where('ci_tv_series.id',$id);	
		$query = $this->db->get();
		$last = $this->db->last_query();
		$result = $query->row_array();
		return $result;
	}
	
	public function get_all_videos(){
		$this->db->select ('ci_subcategory.subcategory_name,ci_video.*,ci_category.category_name,ci_users.firstname');
		$this->db->from('ci_video');
		$this->db->join('ci_category', 'ci_category.id = ci_video.category_id',"LEFT");
		$this->db->join('ci_subcategory', 'ci_subcategory.id = ci_video.subcategory_id',"RIGHT");
		$this->db->join('ci_users', 'ci_users.id = ci_video.user_id');
		$this->db->where(array('ci_video.id !=' => NULL));
		$query = $this->db->get();  
		$last = $this->db->last_query();
		$result = $query->result_array();	
		return $result;
	}
	
	public function get_videos_by_id($id){
		$this->db->select ('ci_subcategory.subcategory_name,ci_video.*,ci_category.category_name');
		$this->db->from('ci_video');
		$this->db->join('ci_category', 'ci_category.id = ci_video.category_id',"LEFT");
		$this->db->join('ci_subcategory', 'ci_subcategory.id = ci_video.subcategory_id',"RIGHT");
		$this->db->where_in('ci_video.id', array('id' => $id));
		$query = $this->db->get();
		$last = $this->db->last_query();
		return $result = $query->row_array();
	}

	public function edit_season($data, $id){
		$this->db->where('id', $id);
		$this->db->update('ci_tv_series', $data);
		return true;
	}
	
	
	public function get_all_category(){
		$query = $this->db->get('ci_category');
		return $result = $query->result_array();	
	}
	
	
	public function get_user_by_id($id){
		$query = $this->db->get_where('ci_users', array('id' => $id));
		$result = $query->row_array();
		return $result;
	}
	
	public function get_videos_by_rowid($id){
		$this->db->select ('*');
		$this->db->from('ci_video');
		$this->db->where_in('id', array('id' => $id));
		$query = $this->db->get();
		$last = $this->db->last_query();
		return $result = $query->row_array();
	}
	
	public function get_tvshows_videos(){
		$this->db->select('*');
		$this->db->from('ci_video');
		$query = $this->db->get();
		return $result = $query->row_array();
	}
	
	/* public function edit($id, $data){
		$this->db->where('id', $id);
		$this->db->update('ci_tv_series', $data);
		if ($this->db->affected_rows() > 0) {
		return true;
		} else {
		return false;
		}
	} */
	
	public function all_users(){
		$this->db->select('*'); 
		$this->db->from('ci_users');
		$query = $this->db->get();
		return $result = $query->result_array();
	}
    
    
}



?>