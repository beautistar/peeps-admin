<?php

class Api_model extends CI_Model {

   public function existEmail($email) {

       $query = $this->db->get_where('ci_users', array('email' => $email));
       if ($query->num_rows() == 0){
            return false;
       } else {
            return true;
       }
   }

   public function addUser($data) {

      $this->db->insert('ci_users', $data);
      return $this->db->insert_id();
   }

   public function login($data){

       $query = $this->db->get_where('ci_users', array('email' => $data['email']));
       if ($query->num_rows() == 0){
           return false;
       }
       else{
           $result = $query->row_array();
           $validPassword = password_verify($data['password'], $result['password']);
           if($validPassword){
               return $result = $query->row_array();
           }
       }
   }

   public function submitPlayList($data) {

       $this->db->insert('ci_video_list', $data);
       $this->db->insert_id();
   }

   public function getPlayList($user_id) {

       $this->db->where('user_id', $user_id);
       $query = $this->db->get('ci_video_list');

       return $query->result_array();

   }

   public function getVideos($playlist_id) {

        $this->db->where('playlist_id', $playlist_id);
        $query = $this->db->get('ci_video');

        return $query->result_array();

   }
   
   function registerToken($user_id, $token) {
		
		$this->db->where('id', $user_id);
		$this->db->set('token', $token);
		$this->db->update('ci_users');
   }
   
   function getTVShows() {
       
       $sql = 'SELECT A.id series_id, A.series_name, A.cover_image, A.image_appearance, A.description,
               A.season_id season_count, A.episode_id episode_count, A.category_id, B.category_name, A.subcategory_id,
               C.subcategory_name, A.created_at, A.updated_at, B.display_order
               FROM ci_tv_series A
               JOIN ci_category B ON A.category_id = B.id
               JOIN ci_subcategory C ON A.subcategory_id = C.id
               ORDER BY B.display_order ASC
               ';
       $query = $this->db->query($sql);
       
       return $query->result_array();
       
   }
   
   function getSeasons($series_id) {
       
       $this->db->select('id as season_id, name as season_name, description');
       $this->db->where('series_id', $series_id);
       $query = $this->db->get('ci_seasons');            
       return $query->result_array(); 
       
   }
   
   function getEpisode($season_id) {
       
       $this->db->select('id as episode_id, name as episode_name, description, hashtags, episode_video, episode_number, duration');
       $this->db->where('season_id', $season_id);
       return $this->db->get('ci_episode')->result_array();
   }
   
   function getMovies() {
       
       return $this->db->select('A.*, B.category_name, C.subcategory_name, B.display_order')
                    ->from('ci_movies as A')
                    ->join('ci_category as B', 'A.category_id = B.id')
                    ->join('ci_subcategory as C', 'A.subcategory_id = C.id')
                    ->order_by('B.display_order', 'ASC')
                    ->get()
                    ->result_array();
   }
   
   function setFavorite($data) {
       
       $this->db->where(array('series_id' => $data['series_id'],
                              'user_id' => $data['user_id']));
       $query = $this->db->get('ci_favourite');
       
       if ($query->num_rows() == 0) {
           $this->db->insert('ci_favourite', $data);
           return true;
       } else {
           $this->db->where(array('series_id' => $data['series_id'],
                              'user_id' => $data['user_id']));
           $this->db->update('ci_favourite', $data);
           return true;
       }
       
   }
   
   function getFavoriteTVShow($user_id) {
       
       return $this->db->select('A.id series_id, A.series_name, A.cover_image, A.image_appearance, A.description,
                A.season_id season_count, A.episode_id episode_count, A.category_id, B.category_name, A.subcategory_id,
                C.subcategory_name, A.created_at, A.updated_at, B.display_order')
                ->from('ci_favourite as F')
                ->where(array('F.user_id' => $user_id, 'mark_fav' => 'yes'))
                ->join('ci_tv_series as A', 'A.id = F.series_id')
                ->join('ci_category as B', 'A.category_id = B.id')
                ->join('ci_subcategory as C', 'A.subcategory_id = C.id')
                ->order_by('B.display_order', 'ASC')
                ->get()
                ->result_array();    
   }
   
   function watchHistory($data) {
       
       $this->db->insert('ci_user_watch_history', $data);
       $this->db->insert_id();
   }
}
   

?>
