<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Category extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/category_model', 'category_model');
			$this->load->model('admin/user_model', 'user_model');
			$this->load->model('admin/user_category_model', 'user_category_model');
		}

		public function index(){
			$data['all_category'] =  $this->category_model->get_all_category();
			$data['title'] = 'Category List';
			$data['view'] = 'admin/category/category_list';
			$this->load->view('admin/layout', $data);
		}
		
		//---------------------------------------------------------------
		//  Add Category
		public function add(){
			if($this->input->post('submit')){
				//$user_cats = $this->input->post('category_access');
				$this->form_validation->set_rules('category_name', 'Category Name', 'trim|min_length[5]|required');
				$this->form_validation->set_rules('belongs_to', 'Select Movies or TV Shows', 'trim|required');
				//$this->form_validation->set_rules('category_access', 'User Category Access', 'trim|required');
				if ($this->form_validation->run() == FALSE) {
					$data['title'] = 'Add Category';
					//$data['user_groups'] = $this->user_model->get_user_groups();
					$data['user_category'] = $this->user_category_model->get_all_user_category();
					$data['view'] = 'admin/category/category_add';
					$this->load->view('admin/layout', $data);
				}
				else{
					/* foreach($user_cats as $cat){ */
					$data = array(
						'category_name' => $this->input->post('category_name'),
						'belongs_to' => $this->input->post('belongs_to'),
						//'user_category_id' => $cat,
						'display_order' =>$this->input->post('display_order')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->category_model->add_category($data);
					/* } */
					if($result){
						$this->session->set_flashdata('msg', 'Category has been Added Successfully!');
						redirect(base_url('admin/category'));
					}
				}
			}
			else{
				$data['user_category'] = $this->user_category_model->get_all_user_category();
				$data['all_category'] = $this->category_model->get_all_categories();
				$data['title'] = 'Add Category';
				$data['view'] = 'admin/category/category_add';
				$this->load->view('admin/layout', $data);
			}
			
		}

		//---------------------------------------------------------------
		//  Edit Category
		public function edit($id = 0){
			if($this->input->post('submit')){
				$this->form_validation->set_rules('category_name', 'category_name', 'trim|required');
				//$this->form_validation->set_rules('firstname', 'Username', 'trim|required');
				//$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
				//$this->form_validation->set_rules('email', 'Email', 'trim|required');
				//$this->form_validation->set_rules('mobile_no', 'Number', 'trim|required');
				//$this->form_validation->set_rules('status', 'Status', 'trim|required');
				//$this->form_validation->set_rules('group', 'Group', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['category'] = $this->category_model->get_category_by_id($id);
					//$data['user_groups'] = $this->user_model->get_user_groups();
					$data['user_category'] = $this->user_category_model->get_all_user_category();
					
					$data['title'] = 'Edit Category';
					$data['view'] = 'admin/category/category_edit';
					$this->load->view('admin/layout', $data);
				}
				else{
					$data = array(
						'category_name' => $this->input->post('category_name'),
						'belongs_to' => $this->input->post('belongs_to'),
						//'user_category_id' => $this->input->post('category_access')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->category_model->edit_category($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'Category has been Updated Successfully!');
						redirect(base_url('admin/category'));
					}
				}
			}
			else{
				$data['category'] = $this->category_model->get_category_by_id($id);
				//$data['user_groups'] = $this->user_model->get_user_groups();
				$data['user_category'] = $this->user_category_model->get_all_user_category();
				$data['title'] = 'Edit Category';
				$data['view'] = 'admin/category/category_edit';
				$this->load->view('admin/layout', $data);
			}
		}

		//---------------------------------------------------------------
		//  Delete Users
		public function del($id = 0){
			$this->db->delete('ci_category', array('id' => $id));
			$this->session->set_flashdata('msg', 'Category has been Deleted Successfully!');
			redirect(base_url('admin/category'));
		}

		//---------------------------------------------------------------
		//  Export Users PDF 
		/*public function create_users_pdf(){
			$this->load->helper('pdf_helper'); // loaded pdf helper
			$data['all_users'] = $this->user_model->get_all_users();
			$this->load->view('admin/users/users_pdf', $data);
		}*/

		//---------------------------------------------------------------	
		// Export data in CSV format 
		/* public function export_csv(){
		   // file name 
		   $filename = 'users_'.date('Y-m-d').'.csv'; 
		   header("Content-Description: File Transfer"); 
		   header("Content-Disposition: attachment; filename=$filename"); 
		   header("Content-Type: application/csv; ");
		   
		   // get data 
		   $user_data = $this->user_model->get_all_users_for_csv();

		   // file creation 
		   $file = fopen('php://output', 'w');
		 
		   $header = array("ID", "Username", "First Name", "Last Name", "Email", "Mobile_no", "Created Date"); 
		   fputcsv($file, $header);
		   foreach ($user_data as $key=>$line){ 
		     fputcsv($file,$line); 
		   }
		   fclose($file); 
		   exit; 
		  } */
	}


?>