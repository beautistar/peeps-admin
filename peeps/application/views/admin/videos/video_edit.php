<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-5">
          <h4><i class="fa fa-pencil"></i> &nbsp; Edit Video</h4>
        </div>
		<div class="col-md-3 text-right">
			<a href="<?= base_url('admin/videos'); ?>" class="btn btn-success"> Back</a>
		</div>
        <div class="col-md-4 text-left">
          <a href="<?= base_url('admin/videos'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Video List</a>
        </div>
        
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
            <?php echo form_open_multipart(base_url('admin/videos/edit/'.$video['id']), 'class="form-horizontal"' )?> 
				<div class="form-group">
					<label for="video_title" class="col-sm-2 control-label">Video Title</label>
					<div class="col-sm-9">
					  <input type="text" name="video_title_update" value="<?= $video['video_title']; ?>" class="form-control" id="video_title" placeholder="">
					</div>
				</div>
				<div class="form-group">
					<label for="video_description" class="col-sm-2 control-label">Video Description</label>
					<div class="col-sm-9">
					  <textarea type="text" id="video_description" name="video_description_update" class="form-control" rows="3" placeholder=""><?php echo $video['video_description']; ?></textarea>
					</div>
				</div> 
				<div class="form-group">
					<label for="video_description" class="col-sm-2 control-label">Select Category</label>
					<div class="col-sm-9">
						<select class="form-control" id="category-list" name="category_id_updated">
							<option value="">Select Category</option>
							<?php foreach($category as $cat):
								if($cat['id'] == $video['category_id']){ ?>
									<option value="<?= $video['category_id']; ?>" selected><?= $video['category_name']; ?></option>
								<?php }else{ ?>
									<option value="<?= $cat['id']; ?>"><?= $cat['category_name']; ?></option>
								<?php } ?>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="form-group" id="subcategory_id_div" >
					<label for="title" class="col-sm-2 control-label">Select Subcategory</label>
					<div class="col-sm-9">
							<select name="subcategory_id" id="subcategory_id" class="form-control">
							</select>
					</div>
				</div>
				<div class="form-group">
					<label for="video" class="col-sm-2 control-label">Type</label>
					<div class="col-sm-4">
					   <input id="file" name="file" type="radio" class="file" value="file" <?php if($video['file_type'] == "File"){echo "checked"; } ?>/> File
					</div>
					<div class="col-sm-4">
					  <input id="url" name="file" type="radio" class="url" value="url" <?php if($video['file_type'] == "URL"){echo "checked"; } ?>/> Url
					</div>
				</div>
				<div class="form-group">
					<label for="video" class="col-sm-2 control-label">Current Video File/URL</label>
					<div class="col-sm-9">	
						<label style="padding:6px 12px;"><?php echo $video['video_upload']; ?></label>
					</div>
				</div>
				
				<div class="form-group">
					<label for="video" class="col-sm-2 control-label">Add a video</label>
					<div class="col-sm-9">	
						<input type="file" name="video_update" class="form-control" id="video_file" placeholder="" />
						<input type="text" id="video_url"  name="video_update" class="form-control" style="display:none;">
					</div>
				</div>
				<?php 
					$userid = $all_videos['user_id'];
					$playlist_id = $all_videos['playlist_id'];
				?>
				<input type="hidden"  id="old"  name="old"  value="<?php echo $video['video_upload'];?>">
				<input type="hidden"  id="old"  name="old_filetype"  value="<?php echo $video['file_type'];?>">
				<input type="hidden"  id="old"  name="user_id"  value="<?php echo $userid;?>">
				<input type="hidden"  id="old"  name="playlist_id"  value="<?php echo $playlist_id;?>">
				<div class="form-group">
					<div class="col-md-11">
					  <input type="submit" name="submit" value="Update" class="btn btn-info pull-right">
					</div>
				</div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  
</section> 
<script>
/* JQuery to bind Subcategory according to Category selection */
$(document).ready(function() {
  //$('#subcategory_id_div').hide();
   $('select[name="subcategory_id"]').append('<option value="<?php echo $video['subcategory_id']; ?>"><?php echo $video['subcategory_name'];?></option>');
	$('#category-list').on('change', function() {
		var category_id = $(this).val();
		if(category_id) {
			$.ajax({
				url:"<?php echo base_url(); ?>admin/videos/subcategory/"+category_id,
				type: "GET",
				dataType: "json",
				success:function(response)
        {
          //('#subcategory-list').empty();
          $('#subcategory_id_div').show();
          $('#subcategory_id').empty();
          $.each(response, function(index,value)
          {
            //alert(data.subcategory_name);
            $('#subcategory_id').append('<option value="'+ value.id +'">'+ value.subcategory_name +'</option>');
          });
				}
			});
		}else{
      //$('#subcategory_id_div').hide();
			$('#subcategory_id').empty();
		}
	});
});
</script>
<script>
$(document).ready(function(){
/* 	if ($('#file').attr("value") == "file") {
		//$('#filetype').val('File');
		$("#video_url").hide();
		$("#video_file").show();
	}else if($('#url').attr("value")== "url"){
		//$('#filetype').val('URL');
		$("#video_file").hide();
		$("#video_url").show();
	} */
	$('#url').on('click',function(){
		$('#video_file').hide();
		$('#video_url').show();
	});
	$('#file').on('click',function(){
		$('#video_url').hide();
		$('#video_file').show();
	});
});
</script>
