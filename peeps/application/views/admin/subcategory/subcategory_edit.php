<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h4><i class="fa fa-pencil"></i> &nbsp; Edit Sub Category</h4>
        </div>
        <div class="col-md-6 text-right" style="display:none;">
          <a href="<?= base_url('admin/subcategory'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Sub Category List</a>
          <a href="<?= base_url('admin/subcategory/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add New Invoice</a>
        </div>

      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>

            <?php echo form_open(base_url('admin/subcategory/edit/'.$subcategory['id']), 'class="form-horizontal"' )?>

            <div class="form-group">
              <div class="col-sm-3">
                <label for="Category_name" class="control-label">Category Name</label>
              </div>
              <div class="col-sm-9">
                <select name="category_id" class="form-control">
                  <option value="">Select Category</option>
                    <?php foreach($category_groups as $cateogry): ?>
                    <?php if($cateogry['id'] == $subcategory['category_id']): ?>
                    <option value="<?= $cateogry['id']; ?>" selected><?= $cateogry['category_name']; ?></option>
                    <?php else: ?>
                    <option value="<?= $cateogry['id']; ?>"><?= $cateogry['category_name']; ?></option>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
			<div class="form-group">
				<label for="subcategory_name" class="col-sm-3 control-label">Sub Category Name</label>
				<div class="col-sm-9">
				  <input type="text" name="subcategory_name" value="<?= $subcategory['subcategory_name']; ?>" class="form-control" id="subcategory_name" placeholder="">
				</div>
			</div>
			<?php /*<div class="form-group">
				<div class="col-sm-3">
					<label for="category_access" class="control-label">User Category Access</label>
				</div>
				<div class="col-sm-9">
					<select name="category_access" class="form-control">
						<option value="">Select User Category</option>
						<?php foreach($user_category as $role): 
							if($role['id'] == $subcategory['user_category_id']){ ?>
								<option value="<?= $role['id']; ?>" selected><?= $role['user_category_name']; ?></option>
							<?php }else{ ?>
								<option value="<?= $role['id']; ?>"><?= $role['user_category_name']; ?></option>
							<?php } ?>
						<?php endforeach; ?>
					</select>
				</div>
			</div> */?>
			<div class="form-group">
				<div class="col-md-12">
				  <input type="submit" name="submit" value="Update Subcategory" class="btn btn-info pull-right">
				</div>
			</div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
