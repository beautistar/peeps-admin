<?php
class Season_model extends CI_Model{
    
    function get_seasons_by_series_id($id) {
        
        $this->db->where('series_id', $id);
        $query = $this->db->get('ci_seasons');
        return $query->result_array();
        
        
    }
    
    public function add($data){    
        $this->db->insert('ci_seasons', $data);
        return true;
    }
    
    public function get_season_by_id($id){

        $this->db->where('ci_seasons.id',$id);
        $query = $this->db->get('ci_seasons');
        return $result = $query->row_array();
        
    }
    
    public function edit_season($data, $id){
        
        $this->db->where('id', $id);
        $this->db->update('ci_seasons', $data);
        return true;
    }

    public function get_all_seasons(){
        $query = $this->db->get('ci_tv_series');
        return $result = $query->result_array();
    }
    
    public function get_all_episodes(){
        $this->db->select('ci_episode.*,ci_tv_series.series_name');
        $this->db->from('ci_episode');
        $this->db->join('ci_tv_series','ci_tv_series.id = ci_episode.season_id',"LEFT");
        $this->db->order_by("ci_episode.name", "desc");
        $query = $this->db->get();
        $last = $this->db->last_query();
        return $result = $query->result_array();
    }
    

    
    public function get_episodes_by_id($id){
        $this->db->select('*');
        $this->db->from('ci_episode');
        $this->db->where('ci_episode.id', $id);
        $query = $this->db->get();
        return $result = $query->result_array();
    }
    
    
    
    
}



?>