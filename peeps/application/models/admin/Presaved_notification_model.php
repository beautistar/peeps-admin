<?php
	class Presaved_notification_model extends CI_Model{

		public function add_notification($data){
			$this->db->insert('ci_pre-saved_notifications', $data);
			return true;
		}

		public function get_all_notifications(){
		//	$this->db->where('is_admin', 0);
			$query = $this->db->get('ci_pre-saved_notifications');
			return $result = $query->result_array();
		}
		public function get_all_notifications_list(){
			//$this->db->where('is_admin', 0);
			$this->db->select ('ci_notifications.*,ci_users.username, ci_users.firstname, ci_users.lastname');
			$this->db->from('ci_notifications');
			$this->db->join('ci_users', 'ci_users.id = ci_notifications.to_user_id',"LEFT");
			$query = $this->db->get();
			//$result = $query->result_array();
			return $result = $query->result_array();
		}
		public function get_all_notifications_for_csv(){
			$this->db->where('is_admin', 0);
			$this->db->select('id, username, firstname, lastname, email, mobile_no, created_at');
			$this->db->from('ci_notifications');
			$query = $this->db->get();
			return $result = $query->result_array();
		}


		public function get_notification_by_id($id){
			$query = $this->db->get_where('ci_pre-saved_notifications', array('id' => $id));
			return $result = $query->row_array();
		}

		public function edit_notification($data, $id){
			$this->db->where('id', $id);
			$this->db->update('ci_pre-saved_notifications', $data);
			return true;
		}

		/*public function get_notification_groups(){
			$query = $this->db->get('ci_notification_groups');
			return $result = $query->result_array();
		}*/
	}
?>
