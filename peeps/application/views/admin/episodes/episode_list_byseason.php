 <!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h4><i class="fa fa-list"></i> &nbsp; Episode List</h4>
        </div>
		<div class="col-md-6 text-right">
			 <a href="<?= base_url('admin/tvshows_videos'); ?>" class="btn btn-success">Back</a>
		</div>
        <?php /*<div class="col-md-6 text-right">
          <div class="btn-group margin-bottom-20" style="display:none;">
            <a href="<?= base_url('admin/videos/create_users_pdf'); ?>" class="btn btn-success">Export as PDF</a>
            <a href="<?= base_url('admin/videos/export_csv'); ?>" class="btn btn-success">Export as CSV</a>
          </div>
        </div> */?>

      </div>
    </div>
  </div>
   <div class="box border-top-solid">
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
			<th>Name</th>
			<th>Description</th>
			<th>Series Name</th>
			<th>Season Number</th>
			<th>Episode Number</th>
			<th>Video</th>
			<th>Duration</th>
			<th class="text-right">Option</th>
        </tr>
        </thead>
        <tbody>
          <?php foreach($all_seasons_id as $row):
		  ?>
          <tr>
            <td><?= $row['name']; ?></td>
            <td><?= $row['description']; ?></td>
			<td><?= $row['series_name']; ?></td>
            <td><?= $row['season_id']; ?></td>
            <td><?= $row['episode_number']; ?></td>
            <td>
				<form method="post">
					<input type="hidden" name="episode_id" value="<?= $row['id']; ?>" id="episode_id">
					<input type="hidden" name="season_id" value="<?= $row['season_id']; ?>" id="season_id">
					<input type="hidden" name="user_id" value="<?= $this->session->userdata('admin_id'); ?>" id="user_id">
					<input type="hidden" name="episode_video" value="<?= $row['episode_video']; ?>" id="episode_video">
				<?php /*<a target="_blank" href="<? = $row['episode_video']; ?>" onClick="details('<?= $row['id']; ?>')"><?= $row['episode_video']; ?></a> */?>
				<a id="video" href="#" onClick="details('<?= $row['id']; ?>')"><?= $row['episode_video']; ?></a>
				</form>
            </td>
			<td><?= $row['duration']; ?></td>
            <td>
              <a href="<?= base_url('admin/episodes/edit/'.$row['id']); ?>" class="btn btn-info btn-flat btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>
              <a data-href="<?= base_url('admin/episodes/del/'.$row['id']); ?>" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
            </td>
		  </tr>
          <?php endforeach; ?>
        </tbody>

      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>


<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>As you sure you want to delete.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
  <script type="text/javascript">
      $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
  </script>

<script>
$("#view_users").addClass('active');

function details(id){
	var id = id;
	var user_id = $("input#user_id").val();
	var season_id = $("input#season_id").val();
	var video = $("input#episode_video").val();
	  $.ajax({
		url: "<?php echo base_url(); ?>" + "admin/episodes/user_history",
		type: 'POST',
		dataType: 'json',
		data: {id: id, user_id: user_id, season_id: season_id, video: video},
		success: function (result) {
			if(result.video != ""){
				window.open(result.video, '_blank');
			}
		}
	});  
}
</script>
