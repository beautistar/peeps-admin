<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body with-border">
        <div class="col-md-6">
          <h4><i class="fa fa-plus"></i> &nbsp; Add New Movie</h4>
        </div>
		
        <div class="col-md-6 text-right">
          <a href="<?= base_url('admin/movies'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Movies List</a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>

            <?php echo form_open_multipart(base_url('admin/movies/add'), 'class="form-horizontal"');  ?>	
				<div class="form-group">
					<label for="name" class="col-sm-3 control-label">Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="name">
					</div>
				</div>
				<div class="form-group">
					<label for="description" class="col-sm-3 control-label">Description</label>
					<div class="col-sm-9">
						<textarea type="text" id="description" name="description" class="form-control" rows="3"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="hashtags" class="col-sm-3 control-label">Hashtags</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="hashtags">
					</div>
				</div>
				<div class="form-group">
					<label for="cover_image" class="col-sm-3 control-label">Cover Image</label>
					<div class="col-sm-9">
					  <input type="file" class="form-control" name="cover_image"  id="cover_image">
					</div>
				</div>
				<div class="form-group">
					<label for="image_appearance" class="col-sm-3 control-label">Image Size Appearance</label>
					<div class="col-sm-9">
						<select name="image_appearance" class="form-control">
							<option value="" selected>Select Image Size</option>
							<option value="small">Small</option>
							<option value="medium">Medium</option>
							<option value="large">Large</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="category_id" class="col-sm-3 control-label">Select Category</label>
					<div class="col-sm-9">
							<?php /*<option value="tvshowsvideo">TV Shows Videos</option>
							<option value="moviesvideo">Movies Videos</option>
						</select> */ ?>
						<select class="form-control" id="category-list" name="category_id">
						<option value="" selected>Select Category</option>
						<?php foreach($category as $cat): if($cat['belongs_to'] == 'movies'){?>
								<option value="<?= $cat['id']; ?>"><?= $cat['category_name']; ?></option>
							<?php }
						 endforeach; ?>
						</select>
					</div>
				</div>
				<div class="form-group" id="subcategory_id_div" >
					<label for="subcategory_id" class="col-sm-3 control-label">Select Subcategory</label>
					<div class="col-sm-9">
						<select name="subcategory_id" id="subcategory_id" class="form-control">
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="movie_video" class="col-sm-3 control-label">Add a video</label>
					<div class="col-sm-9">
					  <input type="file" class="form-control" name="movie_video"  id="movie_video">
					</div>
				</div>	
				<div class="form-group">
					<label for="duration" class="col-sm-3 control-label">Duration</label>
					<div class="col-sm-9">
					  <input type="text" class="form-control" name="duration"  id="duration">
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-11">
						  <input type="submit" name="submit" value="Upload" class="btn btn-info pull-right">
						</div>
					</div>
				</div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
<script>
/* JQuery to bind Subcategory according to Category selection */
$(document).ready(function() {
  //$('#subcategory_id_div').hide();
	$('#category-list').on('change', function() {
		var category_id = $(this).val();
		if(category_id) {
			$.ajax({
				url:"<?php echo base_url(); ?>admin/videos/subcategory/"+category_id,
				type: "GET",
				dataType: "json",
				success:function(response)
        {
          //('#subcategory-list').empty();
          $('#subcategory_id_div').show();
          $('select[name="subcategory_id"]').empty();
          $.each(response, function(index,value)
          {
            //alert(data.subcategory_name);
            $('select[name="subcategory_id"]').append('<option value="'+ value.id +'">'+ value.subcategory_name +'</option>');
          });
				}
			});
		}else{
      //$('#subcategory_id_div').hide();
			$('select[name="subcategory_id"]').empty();
		}
	});
});
</script>
