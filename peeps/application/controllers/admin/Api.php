<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('admin/Api_model', 'Api_model');
        $this->load->library('session');
    }

    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    public function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    public function doRespondSuccess($result) {

        $result['message'] = "Success.";
        $this->doRespond(200, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    public function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }

    public function version() {
        phpinfo();
    }

    public function getUser($user_array) {

        $user_object = array('user_id' => $user_array['id'],
                             'first_name' => $user_array['firstname'],
                             'last_name' => $user_array['lastname'],
                             'email' => $user_array['email'],
                             'photo_url' => $user_array['photo_url'],
                             'created_at' => date('Y-m-d h:m:s'),
                             );
        return $user_object;
    }

    public function register() {

        $result = array();

        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        if ($this->Api_model->existEmail($email)) {
             $result['message'] = "Email already exist.";
             $this->doRespond(201, $result);
        } else {

            if(!is_dir("uploadfiles/")) {
                mkdir("uploadfiles/");
            }
            $upload_path = "uploadfiles/";

            $cur_time = time();

            $dateY = date("Y", $cur_time);
            $dateM = date("m", $cur_time);

            if(!is_dir($upload_path."/".$dateY)){
                mkdir($upload_path."/".$dateY);
            }
            if(!is_dir($upload_path."/".$dateY."/".$dateM)){
                mkdir($upload_path."/".$dateY."/".$dateM);
            }

            $upload_path .= $dateY."/".$dateM."/";
            $upload_url = base_url().$upload_path;

            // Upload file.

            $w_uploadConfig = array(
                'upload_path' => $upload_path,
                'upload_url' => $upload_url,
                'allowed_types' => "*",
                'overwrite' => TRUE,
                'max_size' => "100000KB",
                'max_width' => 4000,
                'max_height' => 4000,
                'file_name' => $dateY.$dateM.intval(microtime(true) * 10)
            );

            $this->load->library('upload', $w_uploadConfig);

            if ($this->upload->do_upload('photo')) {

                $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
                $data = array(
                              'username' => $first_name.$last_name,
                              'firstname' => $first_name,
                              'lastname' => $last_name,
                              'email' => $email,
                              'photo_url' => $file_url,
                              'password' => password_hash($password, PASSWORD_BCRYPT),
                              'created_at' => date('Y-m-d h:m:s'),
                          );
                $id = $this->Api_model->addUser($data);
                $result['image_url'] = $file_url;
                $result['id'] = $id;
                $this->doRespondSuccess($result);

            } else {

                $this->doRespond(201, $result);// upload fail
                return;
            }
        }
    }

    public function login() {

        $result = array();

        $email = $_POST['email'];
        $password = $_POST['password'];

        $data = array(
            'email' => $email,
            'password' => $password
        );
        $q_result = $this->Api_model->login($data);
        if ($q_result == TRUE) {
            $result['user_model'] = $this->getUser($q_result);
            $this->doRespondSuccess($result);
        } else{
            $result['message'] = 'Invalid Email or Password!';
            $this->doRespond(202, $result);
        }
    }
    
    public function submitPlayList() {

        $result = array();

        $user_id = $this->input->post('user_id');
        $name = $this->input->post('name');

        $data = array('video_title' => $name,
                      'user_id' => $user_id,
                      'created_at' => date('Y-m-d h:m:s'),
                  );
        $id = $this->Api_model->submitPlayList($data);
        $this->doRespondSuccess($result);

    }

    public function getPlayList() {

		$t_result = array();
		$result = array();

        $user_id = $this->input->post('user_id');

        $q_result = $this->Api_model->getPlayList($user_id);
		foreach ($q_result as $list) {

			$playlist = array('playlist_id' => $list['id'],
							  'playlist_name' => $list['video_title'],
							  'created_at' => $list['created_at']
							  );
			array_push($t_result, $playlist);
		}

		$result['play_list'] = $t_result;
        $this->doRespondSuccess($result);
    }

    public function getVideos() {

        $playlist_id = $this->input->post('playlist_id');

        $result['video_list'] = $this->Api_model->getVideos($playlist_id);

        $this->doRespondSuccess($result);


    }
	
	function registerToken() {
	    $result = array();	
		$user_id = $this->input->post('user_id');
		$token = $this->input->post('token');
        
        $this->Api_model->registerToken($user_id, $token);
        
        $this->doRespondSuccess($result);
	}
    
    function getTVShowList() {
        
        $result['tvshow_list'] =  $this->Api_model->getTVShows();
        $this->doRespondSuccess($result);
       
    }
    
    function getTVShowContent() {
        
        $content_list = array();
        $series_id = $this->input->post('series_id');
        $seasons = $this->Api_model->getSeasons($series_id);
        
        if (count($seasons) > 0) {
            
            foreach ($seasons as $season) {
            
                $season['episode_list'] = $this->Api_model->getEpisode($season['season_id']);
                
                array_push($content_list, $season);
            }
            
        }
        
        $c_result['season_list'] = $content_list;
        $result['content_list'] = $c_result;
        $this->doRespondSuccess($result);
        
    }
    
    function getMovies() {
        
        $result['movie_list'] = $this->Api_model->getMovies();
        $this->doRespondSuccess($result);
    }
    
    function setFavorite() {
        
        $data = array('user_id' => $this->input->post('user_id'),
                      'series_id' => $this->input->post('series_id'),
                      'mark_fav' => $this->input->post('mark'),
                      'date' => date('Y-m-d h:m:s'));
        
        $this->Api_model->setFavorite($data);
        $this->doRespondSuccess(array());        
    }
    
    function getFavoriteTVShow() {
        
        $user_id = $this->input->post('user_id');
        $result['tvshow_list'] = $this->Api_model->getFavoriteTVShow($user_id);
        $this->doRespondSuccess($result);
    }
    
    function watchEpisode() {
        
        $data = array('episode_id' => $this->input->post('episode_id'),
                      'user_id' => $this->input->post('user_id'),
                      'created_at' => date('Y-m-d h:m:s'),
                      'updated_at' => date('Y-m-d h:m:s'),
                      );
                      
        $this->Api_model->watchHistory($data);
        $this->doRespondSuccess(array());
    }
    
    function watchMovie() {
        
        $data = array('movie_id' => $this->input->post('movie_id'),
                      'user_id' => $this->input->post('user_id'),
                      'created_at' => date('Y-m-d h:m:s'),
                      'updated_at' => date('Y-m-d h:m:s'),
                      );
                      
        $this->Api_model->watchHistory($data);
        $this->doRespondSuccess(array());
    }

}
?>
