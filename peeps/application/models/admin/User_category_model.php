<?php
	class User_category_model extends CI_Model{

		public function add_user_category($data){
			$this->db->insert('ci_user_category', $data);
			return true;
		}

		public function get_all_user_category(){
			//$this->db->where('is_admin', 0);
			$query = $this->db->get('ci_user_category');
			return $result = $query->result_array();
		}
		/* public function get_all_category_for_csv(){
			$this->db->where('is_admin', 0);
			$this->db->select('id, category_name');
			$this->db->from('ci_user_category');
			$query = $this->db->get();
			return $result = $query->result_array();
		} */
		

		public function get_user_category_by_id($id){
			$query = $this->db->get_where('ci_user_category', array('id' => $id));
			return $result = $query->row_array();
		}

		public function edit_user_category($data, $id){
			$this->db->where('id', $id);
			$this->db->update('ci_user_category', $data);
			return true;
		}
		
		/* public function get_user_groups(){
			$query = $this->db->get('ci_user_groups');
			return $result = $query->result_array();
		} */

	}

?>