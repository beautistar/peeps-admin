<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('upload_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->database();
		
	}
	
	public function index(){
		$this->load->view('upload_file', array('error'=>''));
	}	
	
	public function video_upload(){
		if($this->input->post('upload')){
			$this->form_validation->set_rules('file_name', '', 'callback_file_check');
			$config = array(
				'upload_path'   => "./uploads/videos/",
				//'allowed_types' => 'jpg|png|pdf',
				//'max-size' => '2048',
				'allowed_types' =>  'wmv|mp4|avi|mov',
				'max_size' => '0',
				'overwrite'  => TRUE,                       
			);
			$this->load->library('upload', $config);
			if(! $this->upload->do_upload('file_name')){
				$error = array('error'=>$this->upload->display_errors());
				$this->load->view('upload_file',$error);
			}else{
				//$this->upload->do_upload('file_name');
				$filename = $this->upload->data();
				$data = array('file_upload'=> $filename['file_name']);
				$this->upload_model->uploadfile($data);
			}
		}
	}
	
	 public function file_check($str){
		$allowed_mime_type_arr = array('video/mp4','video/3gp','video/mp3');
        $mime = get_mime_by_extension($_FILES['file_name']['name']);
        if(isset($_FILES['file_name']['name']) && $_FILES['file_name']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Please select only mp4/3gp/mp3 formats.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    } 
}

?>