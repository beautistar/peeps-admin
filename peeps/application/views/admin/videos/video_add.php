<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body with-border">
        <div class="col-md-6">
          <h4><i class="fa fa-plus"></i> &nbsp; Add New Video</h4>
        </div>
		<div class="col-md-3 text-right">
			<a href="<?= base_url('admin/users/view_playlist/'); ?>" class="btn btn-success"> Back</a>
		</div>
        <div class="col-md-3 text-left">
          <a href="<?= base_url('admin/videos'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Video List</a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>

            <?php echo form_open_multipart(base_url('admin/videos/add'), 'class="form-horizontal"');  ?>
				<?php if($playlistData != ""){
						foreach($playlistData as $list){
							$playlistid = $list['id'];
							$userid = $list['user_id'];
							$title = $list['video_title']; 
						}?>
						<input type="hidden" name="playlist_id" value="<?php  echo $playlistid; ?>">
						<input type="hidden" name="user_id" value="<?php  echo $userid; ?>">
					<?php } ?>			
				<div class="form-group">
					<label for="title" class="col-sm-3 control-label">Video Title</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="video_title">
					</div>
				</div>
				<div class="form-group">
					<label for="title" class="col-sm-3 control-label">Video Description</label>
					<div class="col-sm-9">
						<textarea type="text" id="video_description" name="video_description" class="form-control" rows="3"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="title" class="col-sm-3 control-label">Select Category</label>
					<div class="col-sm-9">
							<?php /*<option value="tvshowsvideo">TV Shows Videos</option>
							<option value="moviesvideo">Movies Videos</option>
						</select> */ ?>
						<select class="form-control" id="category-list" name="category_id">
						<option value="" selected>Select Category</option>
						<?php foreach($category as $cat): ?>
							<option value="<?= $cat['id']; ?>"><?= $cat['category_name']; ?></option>
						<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="form-group" id="subcategory_id_div" >
					<label for="title" class="col-sm-3 control-label">Select Subcategory</label>
					<div class="col-sm-9">
						<select name="subcategory_id" id="subcategory_id" class="form-control">
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="video" class="col-sm-3 control-label">Type</label>
					<div class="col-sm-4">
					   <input id="file" name="file" type="radio" class="file" value="file" checked/> File
					</div>
					<div class="col-sm-4">
					  <input id="url" name="file" type="radio" class="url" value="url"/> Url
					</div>
				</div>
				<div class="form-group">
					<label for="video" class="col-sm-3 control-label">Add a video</label>
					<div class="col-sm-9">
					  <input type="file" class="form-control" name="video_upload"  id="video_file">
					  <input type="text" class="form-control" name="video_upload" id="video_url" style="display:none;">
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-11">
						  <input type="submit" name="submit" value="Upload" class="btn btn-info pull-right">
						</div>
					</div>
				</div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
<script>
/* JQuery to bind Subcategory according to Category selection */
$(document).ready(function() {
  //$('#subcategory_id_div').hide();
	$('#category-list').on('change', function() {
		var category_id = $(this).val();
		if(category_id) {
			$.ajax({
				url:"<?php echo base_url(); ?>admin/videos/subcategory/"+category_id,
				type: "GET",
				dataType: "json",
				success:function(response)
        {
          //('#subcategory-list').empty();
          $('#subcategory_id_div').show();
          $('select[name="subcategory_id"]').empty();
          $.each(response, function(index,value)
          {
            //alert(data.subcategory_name);
            $('select[name="subcategory_id"]').append('<option value="'+ value.id +'">'+ value.subcategory_name +'</option>');
          });
				}
			});
		}else{
      //$('#subcategory_id_div').hide();
			$('select[name="subcategory_id"]').empty();
		}
	});
});
</script>
<script>
$(document).ready(function(){
	$('#url').on('click',function(){
		$('#video_file').hide();
		$('#video_url').show();
	});
	$('#file').on('click',function(){
		$('#video_url').hide();
		$('#video_file').show();
	});
});
</script>
