<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body with-border">
        <div class="col-md-6">
          <h4><i class="fa fa-plus"></i> &nbsp; Add New Episode</h4>
        </div>
		<div class="col-md-3 text-right">
			<a href="<?= base_url('admin/tvshows_videos/'); ?>" class="btn btn-success"> Back</a>
		</div>
        <div class="col-md-3 text-left">
          <a href="<?= base_url('admin/episodes'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Episode List</a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== '' || isset($error)): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($error)? $error: '';?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?> 			
            <?php echo form_open_multipart(base_url('admin/episodes/add/'.$season_id), 'class="form-horizontal"');  ?>
            <input type="hidden" name="season_id" value="<?= $season_id; ?>">	
				<div class="form-group">
					<label for="name" class="col-sm-3 control-label">Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="name">
					</div>
				</div>
				<div class="form-group">
					<label for="description" class="col-sm-3 control-label">Description</label>
					<div class="col-sm-9">
						<textarea type="text" id="description" name="description" class="form-control" rows="3"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="hashtags" class="col-sm-3 control-label">Hashtags</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="hashtags">
					</div>
				</div>
				<!--<div class="form-group">
					<label for="season_id" class="col-sm-3 control-label">Season no.</label>
					<div class="col-sm-9">
						<select name="season_id" class="form-control"> 
							<option vlaue="">Select Season</option>
							<?php foreach($all_seasons as $season){?>
								<option value="<?= $season['id']; ?>"><?= $season['name'];?></option>
							<?php } ?>
						</select>	
					</div>
				</div>-->
				<div class="form-group">
					<label for="episode_number" class="col-sm-3 control-label">Episode no.</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="episode_number">
					</div>	
				</div>
				<div class="form-group">
					<label for="episode_video" class="col-sm-3 control-label">Add a video</label>
					<div class="col-sm-9">
					  <input type="file" class="form-control" name="episode_video"  id="episode_video">
					</div>
				</div>	
				<div class="form-group">
					<label for="duration" class="col-sm-3 control-label">Duration</label>
					<div class="col-sm-9">
					  <input type="text" class="form-control" name="duration"  id="duration">
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-11">
						  <input type="submit" name="submit" value="Upload" class="btn btn-info pull-right">
						</div>
					</div>
				</div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
