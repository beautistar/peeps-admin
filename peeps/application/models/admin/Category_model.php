<?php
	class Category_model extends CI_Model{

		public function add_category($data){
			$this->db->insert('ci_category', $data);
			return true;
		}
		
		public function get_all_categories(){
			$this->db->select('*');
			$this->db->from('ci_category');
			$query = $this->db->get();	
			$last = $this->db->last_query();
			return $result = $query->result_array();
		}
		
		public function get_all_category(){
			//$this->db->where('is_admin', 0);
		/* 	$query = $this->db->get('ci_category');
			$last = $this->db->last_query();
			return $result = $query->result_array(); */
			$this->db->select('*');
			$this->db->from('ci_user_category');
			$this->db->join('ci_category','ci_user_category.id = ci_category.user_category_id',"RIGHT");
			$this->db->order_by('ci_category.display_order','ASC'); 
			$query = $this->db->get();	
			$last = $this->db->last_query();
			return $result = $query->result_array();
		}
		/* public function get_all_category_for_csv(){
			$this->db->where('is_admin', 0);
			$this->db->select('id, category_name');
			$this->db->from('ci_category');
			$query = $this->db->get();
			return $result = $query->result_array();
		} */
		

		public function get_category_by_id($id){
			$query = $this->db->get_where('ci_category', array('id' => $id));
			return $result = $query->row_array();
		}

		public function edit_category($data, $id){
			$this->db->where('id', $id);
			$this->db->update('ci_category', $data);
			return true;
		}
		
		/* public function get_user_groups(){
			$query = $this->db->get('ci_user_groups');
			return $result = $query->result_array();
		} */
	}

?>