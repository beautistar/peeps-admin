<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h4><i class="fa fa-pencil"></i> &nbsp; Edit Category</h4>
        </div>
        <div class="col-md-6 text-right" style="display:none;">
          <a href="<?= base_url('admin/category'); ?>" class="btn btn-success"><i class="fa fa-list"></i>SubCategory List</a>
          <a href="<?= base_url('admin/category/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add New Invoice</a>
        </div>
        
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open(base_url('admin/category/edit/'.$category['id']), 'class="form-horizontal"' )?> 
			<div class="form-group">
				<div class="col-sm-3">
					<label for="category_name" class="control-label">Category Name</label>
				</div>
				<div class="col-sm-9">
					<input type="text" name="category_name" value="<?= $category['category_name']; ?>" class="form-control" id="category_name" placeholder="">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-3">
					<label for="belongs_to" class="control-label">Belongs To</label>
				</div>
				<div class="col-sm-9">
					<input type="radio" name="belongs_to" value="movies" <?php if($category['belongs_to']=="movies"){echo "checked";} ?>> Movies &nbsp;
					<input type="radio" name="belongs_to" value="tvshows" <?php if($category['belongs_to']=="tvshows"){echo "checked";} ?>> TV Shows &nbsp;
				</div>
			</div>
			<?php /*<div class="form-group">
				<div class="col-sm-3">
					<label for="category_access" class="control-label">User Category Access</label>
				</div>
				<div class="col-sm-9">
					<select name="category_access" class="form-control">
						<option value="">Select User Category</option>
						<?php foreach($user_category as $role): 
							if($role['id'] == $category['user_category_id']){ ?>
								<option value="<?= $role['id']; ?>" selected><?= $role['user_category_name']; ?></option>
							<?php }else{ ?>
								<option value="<?= $role['id']; ?>"><?= $role['user_category_name']; ?></option>
							<?php } ?>
						<?php endforeach; ?>
					</select>
				</div>
			</div> */?>
			<div class="form-group">
				<div class="col-md-11">
				  <input type="submit" name="submit" value="Update Category" class="btn btn-info pull-right">
				</div>
			</div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  
</section> 