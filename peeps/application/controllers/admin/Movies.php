<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Movies extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('admin/video_model', 'video_model');
		$this->load->model('admin/video_listing_model', 'video_listing_model');
		$this->load->model('admin/subcategory_model', 'subcategory_model');
		$this->load->model('admin/category_model', 'category_model');
		$this->load->model('admin/tvshow_video_model', 'tvshow_video_model');
		$this->load->model('admin/episode_model', 'episode_model');
		$this->load->model('admin/movie_model', 'movie_model');

	}

	public function index()
	{
		$data['all_movies'] =  $this->movie_model->get_all_movies();
		$data['all_episodes'] = $this->movie_model->get_all_episodes();
		$data['title'] = 'Movies List';
		$data['view'] = 'admin/movies/movie_list';
		$this->load->view('admin/layout', $data);
	}

	public function add(){
		if($this->input->post('submit'))
		{
		$this->form_validation->set_rules('name', ' Name', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_rules('category_id', 'Category', 'trim|required');
		$this->form_validation->set_rules('image_appearance', 'Image Size', 'trim|required');
		$this->form_validation->set_rules('cover_image', '', 'callback_file_check2');
		$this->form_validation->set_rules('movie_video', '', 'callback_file_check3');
		if ($this->form_validation->run() == FALSE)
		{
			$data['title'] = 'Add Video';
			$data['category'] = $this->video_model->get_all_category();
			$data['view'] = 'admin/movies/movie_add';
			$this->load->view('admin/layout', $data);
		}else
		{
			$config1 = array
				(
					'upload_path'   => "./uploads/images/",
					'allowed_types' => 'jpg|jpeg|bmp|png',
					'overwrite' => TRUE,
					'max_size' => "30720"
				);
				$this->load->library('upload', $config1);
				$this->upload->do_upload('cover_image');
				$cover_upload = $this->upload->data();
				$new_name = time();
				$file_ext = pathinfo($_FILES["movie_video"]['name'],PATHINFO_EXTENSION);
				$video_name = $new_name.".".$file_ext;
				$config3 = array
				(
					'file_name' => $video_name,
					'upload_path' => './uploads/videos/',
					'allowed_types' => "wmv|mp4|avi|mov|webm|3gp",
					'overwrite' => TRUE,
					'max_size' => "100000",
				);

				$this->upload->initialize($config3);
				$this->upload->do_upload('movie_video');
				$video = $this->upload->data();

				$image = $_FILES["image_appearance"]['name'];
				$cover_image = $_FILES["cover_image"]['name'];
				$data = array(
					'name' => $this->input->post('name'),
					'cover_image' => base_url().'/uploads/images/'.$cover_image,
					'image_appearance' => $this->input->post('image_appearance'),
					'description' => $this->input->post('description'),
					'hashtags'  => $this->input->post('hashtags'),
					'category_id' => $this->input->post('category_id'),
					'subcategory_id' => $this->input->post('subcategory_id'),
					'duration' => $this->input->post('duration'),
					'movie_video' => base_url()."uploads/videos/".$video_name,
					'created_at' => date('Y-m-d h:m:s'),
					'updated_at' => date('Y-m-d h:m:s')
				);
				$data = $this->security->xss_clean($data);
				$result = $this->movie_model->add($data);
				if($result)
				{
					$this->session->set_flashdata('msg', 'Movie has been added sucessfully!');
					redirect(base_url('admin/movies'));
				}

			}
		}
		else
		{
			//$data['playlistData']=$this->video_listing_model->get_listing_by_id($id);
			$data['category'] = $this->video_model->get_all_category();
			$data['title'] = 'Add Movies';
			$data['view'] = 'admin/movies/movie_add';
			$this->load->view('admin/layout', $data);
		}
	}


	//Edit
	public function edit($id = 0){
		if($this->input->post('submit')){
			$this->form_validation->set_rules('name', ' Name', 'trim|required');
            $this->form_validation->set_rules('description', 'Description', 'trim|required');
			if(!empty($_FILES['cover_image']['name'])){
				$this->form_validation->set_rules('cover_image', '', 'callback_file_check2');
			}
			if(!empty($_FILES['movie_video']['name'])){
				$this->form_validation->set_rules('movie_video', '', 'callback_file_check3');
			}
			if ($this->form_validation->run() == FALSE) {
				$data['video'] = $this->movie_model->get_movies_by_id($id);
				$data['title'] = 'Edit Movie';
				$data['view'] = 'admin/movies/movie_edit';
				$this->load->view('admin/layout', $data);
			}
			else{
				if(!empty($_FILES['cover_image']['name']) && empty($_FILES['movie_video']['name'])){
					$video_name = $this->input->post('video_old');
					$cover_image = $_FILES["cover_image"]['name'];
					$config1 = array
					(
						'upload_path'   => "./uploads/images/",
						'allowed_types' => 'jpg|jpeg|bmp|png',
						'overwrite' => TRUE,
						'max_size' => "30720"
					);
					$this->load->library('upload', $config1);
					$this->upload->do_upload('cover_image');
					$cover_upload = $this->upload->data();
					$data = array(
						'name' => $this->input->post('name'),
						'cover_image' => base_url().'/uploads/images/'.$cover_image,
						'image_appearance' => $this->input->post('image_appearance'),
						'category_id' => $this->input->post('category_id'),
						'subcategory_id' => $this->input->post('subcategory_id'),
						'description' => $this->input->post('description'),
						'hashtags'  => $this->input->post('hashtags'),
						'duration' => $this->input->post('duration'),
						'movie_video' => $video_name,
						'updated_at' => date('Y-m-d h:m:s')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->movie_model->edit_movie($data, $id);
					if($result)
					{
						$this->session->set_flashdata('msg', 'Movie has been updated sucessfully!');
						redirect(base_url('admin/movies'));
					}
				}else if(empty($_FILES['cover_image']['name']) && empty($_FILES['movie_video']['name'])){
					$cover_image = $this->input->post('cover_image_old');
					$video_name = $this->input->post('video_old');
					$data = array(
						'name' => $this->input->post('name'),
						'cover_image' => $cover_image,
						'image_appearance' => $this->input->post('image_appearance'),
						'category_id' => $this->input->post('category_id'),
						'subcategory_id' => $this->input->post('subcategory_id'),
						'description' => $this->input->post('description'),
						'hashtags'  => $this->input->post('hashtags'),
						'duration' => $this->input->post('duration'),
						'movie_video' => $video_name,
						'updated_at' => date('Y-m-d h:m:s')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->movie_model->edit_movie($data, $id);
					if($result)
					{
						$this->session->set_flashdata('msg', 'Movie has been updated sucessfully!');
						redirect(base_url('admin/movies'));
					}

				}else if(!empty($_FILES['movie_video']['name']) && empty($_FILES['cover_image']['name'])){
					$new_name = time();
					$file_ext = pathinfo($_FILES["movie_video"]['name'],PATHINFO_EXTENSION);
					$video_name = $new_name.".".$file_ext;
					$config3 = array
					(
						'file_name' => $video_name,
						'upload_path' => './uploads/videos/',
						'allowed_types' => "wmv|mp4|avi|mov|webm|3gp",
						'overwrite' => TRUE,
						'max_size' => "100000",
					);
					$this->load->library('upload');
					$this->upload->initialize($config3);
					$this->upload->do_upload('movie_video');
					$video = $this->upload->data();

					$cover_image = $this->input->post('cover_image_old');
					$data = array(
						'name' => $this->input->post('name'),
						'cover_image' => $cover_image,
						'image_appearance' => $this->input->post('image_appearance'),
						'category_id' => $this->input->post('category_id'),
						'subcategory_id' => $this->input->post('subcategory_id'),
						'description' => $this->input->post('description'),
						'hashtags'  => $this->input->post('hashtags'),
						'duration' => $this->input->post('duration'),
						'movie_video' => base_url()."uploads/videos/".$video_name,
						'created_at' => date('Y-m-d h:m:s'),
						'updated_at' => date('Y-m-d h:m:s')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->movie_model->edit_movie($data, $id);
					if($result)
					{
						$this->session->set_flashdata('msg', 'Movie has been updated sucessfully!');
						redirect(base_url('admin/movies'));
					}
				}else{
					$config1 = array
					(
						'upload_path'   => "./uploads/images/",
						'allowed_types' => 'jpg|jpeg|bmp|png',
						'overwrite' => TRUE,
						'max_size' => "30720"
					);
					$this->load->library('upload');
					$this->upload->initialize($config1);
					$this->upload->do_upload('cover_image');
					$cover_upload = $this->upload->data();

					$new_name = time();
					$file_ext = pathinfo($_FILES["movie_video"]['name'],PATHINFO_EXTENSION);
					$video_name = $new_name.".".$file_ext;
					$config3 = array
					(
						'file_name' => $video_name,
						'upload_path' => './uploads/videos/',
						'allowed_types' => "wmv|mp4|avi|mov|webm|3gp",
						'overwrite' => TRUE,
						'max_size' => "100000",
					);
					$this->upload->initialize($config3);
					$this->upload->do_upload('movie_video');
					$video = $this->upload->data();

					$cover_image = $_FILES["cover_image"]['name'];
					$data = array(
						'name' => $this->input->post('name'),
						'cover_image' => base_url().'/uploads/images/'.$cover_image,
						'image_appearance' => $this->input->post('image_appearance'),
						'description' => $this->input->post('description'),
						'hashtags'  => $this->input->post('hashtags'),
						'category_id' => $this->input->post('category_id'),
						'subcategory_id' => $this->input->post('subcategory_id'),
						'duration' => $this->input->post('duration'),
						'movie_video' => base_url()."uploads/videos/".$video_name,
						'created_at' => date('Y-m-d h:m:s'),
						'updated_at' => date('Y-m-d h:m:s')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->movie_model->edit_movie($data, $id);
					if($result)
					{
						$this->session->set_flashdata('msg', 'Movie has been updated sucessfully!');
						redirect(base_url('admin/movies'));
					}
				}
			}
		}
		else{
			$data['video'] = $this->movie_model->get_movies_by_id($id);
			$data['category'] = $this->video_model->get_all_category();
			$data['subcategory'] = $this->subcategory_model->get_subcategory_by_id($id);
			$data['category_groups'] = $this->category_model->get_all_category();
			$data['title'] = 'Edit Movie';
			$data['view'] = 'admin/movies/movie_edit';			
			$this->load->view('admin/layout', $data);
		}
	}

	//Delete
	public function del($id=0){
		$this->db->select('*');
		$this->db->from('ci_movies');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->row_array();
        $spilt_str = explode('/', $result['movie_video']);
        $video = $spilt_str[count($spilt_str)-1];
		$this->db->delete('ci_movies', array('id' => $id));
        unlink('uploads/videos/'.$video);
		$this->session->set_flashdata('msg', 'Movie has been Deleted Successfully!');
		redirect(base_url('admin/movies'));

	}


	//File type validation
	public function file_check2($str){
		$allowed_mime_type_arr = array('image/jpg','image/jpeg','image/png','image/pjpeg','image/bmp');
		//$file_size_max = 100000;
        $mime = get_mime_by_extension($_FILES['cover_image']['name']);
        if(isset($_FILES['cover_image']['name']) && $_FILES['cover_image']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            } else{
                $this->form_validation->set_message('file_check', 'Please select only jpg,png and jpeg formats.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    }

		//File type validation
	public function file_check3($str){
		$allowed_mime_type_arr = array('video/mp4','video/3gp','video/mp3','video/mov','video/wmv','video/webm');
		//$file_size_max = 100000;
        $mime = get_mime_by_extension($_FILES['movie_video']['name']);
        if(isset($_FILES['movie_video']['name']) && $_FILES['movie_video']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            } else{
                $this->form_validation->set_message('file_check', 'Please select only mp4/3gp/mp3/mov/wmv/webm formats.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    }
	
	public function user_history(){
		if(isset($_POST['user_id']) && $_POST['user_id'] != ""){
			$json = array(
				'user_id '=>$this->input->post('user_id'),
				'movie_id' => $this->input->post('movie_id'),
				'video' => $this->input->post('video'),
			);
			$video = $json['video'];
			$data = array(
				'movie_id' => $this->input->post('movie_id'),
				'user_id '=>$this->input->post('user_id'),
				'created_at' => date('Y-m-d h:m:s'),
				'updated_at' => date('Y-m-d h:m:s')
			);
			$result = $this->db->insert('ci_user_watch_history', $data);
			$arr = array('movie_id' => $data['movie_id'],
				'user_id '=>$this->input->post('user_id'),
				'video' => $video
			);
			echo json_encode($arr);
		}
	}
	
		public function mark_as_favourite(){
		if(isset($_POST['mark_fav'])){
			$json = array(
				'mark_fav' => $this->input->post('mark_fav'),
				'user_id '=>$this->input->post('user_id'),
				'id1' => $this->input->post('id1'),
				'title' => $this->input->post('title'),
				'message' => $this->input->post('message'),
				'old_movie_id' => $this->input->post('old_movie_id'),
				'old_user_id' => $this->input->post('old_user_id')
			);
			$data1 = json_encode($json);
			$id = $json['id1'];
			$userid = $json['old_user_id'];
			$movie_id = $json['old_movie_id'];
			$data = array(
				'mark_fav' =>$this->input->post('mark_fav'),
				'user_id' => $this->input->post('user_id'),
				'movie_id' => $id,
				'date' => date('Y-m-d h:m:s')
			);
			if($data['mark_fav'] == "yes"){
				$return = $this->db->insert('ci_favourite', $data);
			}else if($data['mark_fav'] == "no"){
				$array = array('user_id' => $data['user_id'], 'movie_id' => $data['movie_id']);
				$this->db->where($array);
				$return = $this->db->update('ci_favourite', $data);
			}else{
				
			}
			$arr = array('msg' => true,
			'markfav' => $data['mark_fav'],
			'userid' => $data['user_id'],
			'id' => $id
			);
			echo json_encode($arr);
		}
	}

}

?>
