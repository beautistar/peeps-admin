<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class User_category extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/user_category_model', 'user_category_model');
		}

		public function index(){
			$data['all_user_category'] =  $this->user_category_model->get_all_user_category();
			$data['title'] = 'User Category List';
			$data['view'] = 'admin/user_category/user_category_list';
			$this->load->view('admin/layout', $data);
		}
		
		//---------------------------------------------------------------
		//  Add Category
		public function add(){
			if($this->input->post('submit')){
				$this->form_validation->set_rules('user_category_name', 'User Category Name', 'trim|min_length[3]|required');
				$this->form_validation->set_rules('user_category_check', 'Select Yes or No', 'trim|required');
				$this->form_validation->set_rules('user_discount', 'Discount', 'trim|required');
				if ($this->form_validation->run() == FALSE) {
					$data['title'] = 'Add User Category';
					//$data['user_groups'] = $this->user_model->get_user_groups();
					$data['view'] = 'admin/user_category/user_category_add';
					$this->load->view('admin/layout', $data);
				}
				else{
					$data = array(
						'user_category_name' => $this->input->post('user_category_name'),
						'user_category_check' => $this->input->post('user_category_check'),
						'user_discount' => $this->input->post('user_discount'),
						//'firstname' => $this->input->post('firstname'),
						//'lastname' => $this->input->post('lastname'),
						//'email' => $this->input->post('email'),
						//'mobile_no' => $this->input->post('mobile_no'),
						//'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT),
						//'role' => $this->input->post('group'),
						//'created_at' => date('Y-m-d : h:m:s'),
						//'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->user_category_model->add_user_category($data);
					if($result){
						$this->session->set_flashdata('msg', 'Category has been Added Successfully!');
						redirect(base_url('admin/user_category'));
					}
				}
			}
			else{
				//$data['user_groups'] = $this->user_model->get_user_groups();
				$data['title'] = 'Add user Category';
				$data['view'] = 'admin/user_category/user_category_add';
				$this->load->view('admin/layout', $data);
			}
			
		}

		//---------------------------------------------------------------
		//  Edit Category
		public function edit($id = 0){
			if($this->input->post('submit')){
				$this->form_validation->set_rules('user_category_name', 'User Category_name', 'trim|required');
				$this->form_validation->set_rules('user_category_check');
				$this->form_validation->set_rules('user_discount', 'User discount');
				//$this->form_validation->set_rules('firstname', 'Username', 'trim|required');
				//$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
				//$this->form_validation->set_rules('email', 'Email', 'trim|required');
				//$this->form_validation->set_rules('mobile_no', 'Number', 'trim|required');
				//$this->form_validation->set_rules('status', 'Status', 'trim|required');
				//$this->form_validation->set_rules('group', 'Group', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['user_category'] = $this->user_category_model->get_user_category_by_id($id);
					//$data['user_groups'] = $this->user_model->get_user_groups();
					$data['title'] = 'Edit User Category';
					$data['view'] = 'admin/user_category/user_category_edit';
					$this->load->view('admin/layout', $data);
				}
				else{
					$data = array(
						'user_category_name' => $this->input->post('user_category_name'),
						'user_category_check' => $this->input->post('user_category_check'),
						'user_discount' => $this->input->post('user_discount'),
						//'firstname' => $this->input->post('firstname'),
						//'lastname' => $this->input->post('lastname'),
						//'email' => $this->input->post('email'),
						//'mobile_no' => $this->input->post('mobile_no'),
						//'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT),
						//'role' => $this->input->post('group'),
						//'is_active' => $this->input->post('status'),
						//'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->user_category_model->edit_user_category($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'User Category has been Updated Successfully!');
						redirect(base_url('admin/user_category'));
					}
				}
			}
			else{
				$data['user_category'] = $this->user_category_model->get_user_category_by_id($id);
				//$data['user_groups'] = $this->user_model->get_user_groups();
				$data['title'] = 'Edit User Category';
				$data['view'] = 'admin/user_category/user_category_edit';
				$this->load->view('admin/layout', $data);
			}
		}

		//---------------------------------------------------------------
		//  Delete Users
		public function del($id = 0){
			$this->db->delete('ci_user_category', array('id' => $id));
			$this->session->set_flashdata('msg', 'User Category has been Deleted Successfully!');
			redirect(base_url('admin/user_category'));
		}
		
		//  User Caegory Access
		public function user_categor_access(){
			$data['all_users'] = $this->user_model->get_all_users();
		}

		//---------------------------------------------------------------
		//  Export Users PDF 
		/*public function create_users_pdf(){
			$this->load->helper('pdf_helper'); // loaded pdf helper
			$data['all_users'] = $this->user_model->get_all_users();
			$this->load->view('admin/users/users_pdf', $data);
		}*/

		//---------------------------------------------------------------	
		// Export data in CSV format 
		/* public function export_csv(){
		   // file name 
		   $filename = 'users_'.date('Y-m-d').'.csv'; 
		   header("Content-Description: File Transfer"); 
		   header("Content-Disposition: attachment; filename=$filename"); 
		   header("Content-Type: application/csv; ");
		   
		   // get data 
		   $user_data = $this->user_model->get_all_users_for_csv();

		   // file creation 
		   $file = fopen('php://output', 'w');
		 
		   $header = array("ID", "Username", "First Name", "Last Name", "Email", "Mobile_no", "Created Date"); 
		   fputcsv($file, $header);
		   foreach ($user_data as $key=>$line){ 
		     fputcsv($file,$line); 
		   }
		   fclose($file); 
		   exit; 
		  } */
	}


?>