 <!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h4><i class="fa fa-list"></i> &nbsp; Episode List</h4>
        </div>
		<div class="col-md-6 text-right">
          <?php if (isset($season_id)) { ?>
          <a href="<?= base_url('admin/episodes/add/'.$season_id); ?>" class="btn btn-success">Add a Episode</a>
          <?php }?>
		  <a href="<?= base_url('admin/tvshows_videos'); ?>" class="btn btn-success">Back</a>
		</div>
        <?php /*<div class="col-md-6 text-right">
          <div class="btn-group margin-bottom-20" style="display:none;">
            <a href="<?= base_url('admin/videos/create_users_pdf'); ?>" class="btn btn-success">Export as PDF</a>
            <a href="<?= base_url('admin/videos/export_csv'); ?>" class="btn btn-success">Export as CSV</a>
          </div>
        </div> */?>

      </div>
    </div>
  </div>
   <div class="box border-top-solid">
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
            <th>Episode ID</th>
			<th>Name</th>
			<th>Description</th>
			<th>Hashtags</th>
			<th>Season ID</th>
			<th>Episode Number</th>
			<th>Episode Video</th>
			<th>Duration</th>
			<th class="text-right">Option</th>
        </tr>
        </thead>
        <tbody>
          <?php foreach($all_episodes as $row):
			//echo "<pre>"; print_r($row); echo "</pre>";
		  ?>
          <tr>
            <td><?= $row['id']; ?></td>
            <td><?= $row['name']; ?></td>
            <td><?= $row['description']; ?></td>
			<td><?= $row['hashtags']; ?></td>
            <td><?= $row['season_id']; ?></td>
            <td><?= $row['episode_number']; ?></td>
            <td>
				<form method="post" action="" class="episode_form-<?php echo $row['id']; ?>">
					<input type="hidden" name="user_id" id="user_id" value="<?php $id = $this->session->userdata('admin_id');  echo $id;?>">
					<input type="hidden" name="episode_id" id="episode_id" value="<?php echo $row['id']; ?>">
					<input type="hidden" name="video" id="video" value="<?php echo $row['episode_video']; ?>">
					<a onClick="sendajax('<?= $row['id']; ?>')"  href="#"><?= $row['episode_video']; ?></a>
				</form>
            </td>
			<td><?= $row['duration']; ?></td>
            <td>
              <a href="<?= base_url('admin/episodes/edit/'.$row['id']); ?>" class="btn btn-info btn-flat btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>
              <a data-href="<?= base_url('admin/episodes/del/'.$row['id']); ?>" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
            </td>
		  </tr>
          <?php endforeach; ?>
        </tbody>

      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>


<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>As you sure you want to delete.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
  <script type="text/javascript">
      $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
  </script>

<script>
$("#view_users").addClass('active');
</script>

<script>
function sendajax(id){ 
	jQuery('.episode_form-'+id )
	var episode_id = $('.episode_form-'+id + ' input#episode_id').val();
	var user_id = $('.episode_form-'+id + ' input#user_id').val();
	var video = $('.episode_form-'+id + ' input#video').val();
	jQuery.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>" + "admin/episodes/user_history",
		dataType: 'json',
		data: {user_id: user_id, episode_id:episode_id, video:video},
		success: function(res) {
			if (res.video != '')
			{
				window.open(res.video, '_blank');
			}
		}
	});
}
</script>
