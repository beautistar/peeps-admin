<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-5">
          <h4><i class="fa fa-pencil"></i> &nbsp; Edit Episode</h4>
        </div>
        
        <div class="col-md-6 text-right">
          <div class="btn-group margin-bottom-20">
            <a href="<?= base_url('admin/episodes'); ?>" class="btn btn-success">Back</a>
            <a href="<?= base_url('admin/tvshows_videos'); ?>" class="btn btn-success" style="margin-left: 5px;"><i class="fa fa-list"></i>Tv series list</a>
          </div>
        </div> 
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           <?php foreach($video as $episode){?>
            <?php echo form_open_multipart(base_url('admin/episodes/edit/'.$episode['id']), 'class="form-horizontal"' )?> 
				<div class="form-group">
					<label for="name" class="col-sm-3 control-label">Name</label>
					<div class="col-sm-9">
					  <input type="text" name="name" value="<?= $episode['name']; ?>" class="form-control" id="name" placeholder="">
					</div>
				</div>
				<div class="form-group">
					<label for="description" class="col-sm-3 control-label">Description</label>
					<div class="col-sm-9">
					  <textarea type="text" id="description" name="description" class="form-control" rows="3" placeholder=""><?php echo $episode['description']; ?></textarea>
					</div>
				</div> 
				<div class="form-group">
					<label for="hashtags" class="col-sm-3 control-label">Hashtags</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="hashtags" value="<?php echo $episode['hashtags']; ?>">
					</div>
				</div>
				
				<div class="form-group">
					<label for="episode_number" class="col-sm-3 control-label">Episode no.</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="episode_number" value="<?php echo $episode['episode_number']; ?>">
					</div>	
				</div>
				<div class="form-group">
					<label for="current" class="col-sm-3 control-label">Current Video</label>
					<div class="col-sm-9">
						<a href="<?php echo $episode['episode_video']; ?>"><span><?php echo $episode['episode_video']; ?></span></a>
						<input type="hidden" name="video_old" value="<?php echo $episode['episode_video']; ?>">
					</div>
				</div>	
				<div class="form-group">
					<label for="episode_video" class="col-sm-3 control-label">Add a video</label>
					<div class="col-sm-9">
						<input type="file" class="form-control" name="episode_video"  id="episode_video">
					</div>
				</div>	
				<div class="form-group">
					<label for="duration" class="col-sm-3 control-label">Duration</label>
					<div class="col-sm-9">
					  <input type="text" class="form-control" name="duration"  id="duration" value="<?php echo $episode['duration']; ?>">
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-11">
					  <input type="submit" name="submit" value="Update" class="btn btn-info pull-right">
					</div>
				</div>
            <?php echo form_close(); ?>
			<?php }?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  
</section> 
<script>
/* JQuery to bind Subcategory according to Category selection */
$(document).ready(function() {
  //$('#subcategory_id_div').hide();
	$('#category-list').on('change', function() {
		var category_id = $(this).val();
		if(category_id) {
			$.ajax({
				url:"<?php echo base_url(); ?>admin/videos/subcategory/"+category_id,
				type: "GET",
				dataType: "json",
				success:function(response)
        {
          //('#subcategory-list').empty();
          $('#subcategory_id_div').show();
          $('#subcategory_id').empty();
          $.each(response, function(index,value)
          {
            //alert(data.subcategory_name);
            $('#subcategory_id').append('<option value="'+ value.id +'">'+ value.subcategory_name +'</option>');
          });
				}
			});
		}else{
      //$('#subcategory_id_div').hide();
			$('#subcategory_id').empty();
		}
	});
});
</script>
<script>
$(document).ready(function(){
/* 	if ($('#file').attr("value") == "file") {
		//$('#filetype').val('File');
		$("#video_url").hide();
		$("#video_file").show();
	}else if($('#url').attr("value")== "url"){
		//$('#filetype').val('URL');
		$("#video_file").hide();
		$("#video_url").show();
	} */
	$('#url').on('click',function(){
		$('#video_file').hide();
		$('#video_url').show();
	});
	$('#file').on('click',function(){
		$('#video_url').hide();
		$('#video_file').show();
	});
});
</script>
