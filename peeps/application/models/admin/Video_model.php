<?php
class Video_model extends CI_Model{
	public function add_video($data){	
		$this->db->insert('ci_video', $data);
		return true;
	}
	

	/* public function get_all_videos(){
		$query = $this->db->get('ci_video');
		return $result = $query->result_array();
	} */
	
	public function get_all_videos(){
		$this->db->select ('ci_subcategory.subcategory_name,ci_video.*,ci_category.category_name,ci_users.firstname');
		$this->db->from('ci_video');
		$this->db->join('ci_category', 'ci_category.id = ci_video.category_id',"LEFT");
		$this->db->join('ci_subcategory', 'ci_subcategory.id = ci_video.subcategory_id',"RIGHT");
		$this->db->join('ci_users', 'ci_users.id = ci_video.user_id');
		$this->db->where(array('ci_video.id !=' => NULL));
		$query = $this->db->get();  
		$last = $this->db->last_query();
		$result = $query->result_array();	
		return $result;
	}	
	
/* 	public function get_videos(){
		$this->db->select ('*');
		$this->db->from('ci_video');
 		$this->db->join('ci_category', 'ci_category.id = ci_video.category_id',"LEFT"); 
		$this->db->where(array('type =' => 'tvshows'));
		$query = $this->db->get();  
		$last = $this->db->last_query();
		$result = $query->result_array();	
		return $result;
	}
	
	public function get_movies_videos(){
		$this->db->select ('*');
		$this->db->from('ci_video');
		$this->db->join('ci_category', 'ci_category.id = ci_video.category_id',"LEFT");
		$this->db->where(array('type =' => 'movies'));
		$query = $this->db->get();  
		$last = $this->db->last_query();
		$result = $query->result_array();	
		return $result;
	} */
	
	public function get_videos_by_id($id){
		$this->db->select ('ci_subcategory.subcategory_name,ci_video.*,ci_category.category_name');
		$this->db->from('ci_video');
		$this->db->join('ci_category', 'ci_category.id = ci_video.category_id',"LEFT");
		$this->db->join('ci_subcategory', 'ci_subcategory.id = ci_video.subcategory_id',"RIGHT");
		$this->db->where_in('ci_video.id', array('id' => $id));
		$query = $this->db->get();
		$last = $this->db->last_query();
		return $result = $query->row_array();
	}

	public function edit_video($data, $id){
		$this->db->select('video_upload');
		$this->db->from('ci_video');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->row_array();
		$image = $result['video_upload'];
		if($result>0){
			unlink('uploads/videos/'.$image);
			$this->db->where('id', $id);
			$this->db->update('ci_video', $data);
			return true;
		}else{
			$this->db->where('id', $id);
			$this->db->update('ci_video', $data);
			return true;
		}
	}
	
	public function get_all_category(){
		$query = $this->db->get('ci_category');
		return $result = $query->result_array();	
	}
	
	
	public function get_user_by_id($id){
		$query = $this->db->get_where('ci_users', array('id' => $id));
		$result = $query->row_array();
		return $result;
	}
	
	public function get_videos_by_rowid($id){
		$this->db->select ('*');
		$this->db->from('ci_video');
		$this->db->where_in('id', array('id' => $id));
		$query = $this->db->get();
		$last = $this->db->last_query();
		return $result = $query->row_array();
	}
	
	public function edit_playlist($data, $id){
		$this->db->where('playlist_id', $id);
		$this->db->update('ci_video', $data);
		return true;
	}
}



?>