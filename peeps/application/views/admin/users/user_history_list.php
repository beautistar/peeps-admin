 <!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h4><i class="fa fa-list"></i> &nbsp; User Watch History List</h4>
        </div> 
		<div class="col-md-6 text-right">
			 <a href="<?= base_url('admin/users'); ?>" class="btn btn-success">Back</a>
		</div>
      </div>
    </div>
  </div>
   <div class="box border-top-solid">
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
			<th>User Name</th>
			<th>Series Name</th>
			<th>Season Name</th>
			<th>Episode Name</th>
			<th style="width:100px">Watched At</th>
        </tr>
        </thead>
        <tbody>
 
          <?php foreach($history as $row): 
			//echo "<pre>"; print_r($row); echo "</pre>";
		  ?>
          <tr>
            <td><?= $row['firstname']; ?> <?= $row['lastname']; ?></td>
            <td><?= $row['series_name']; ?></td>
            <td><?= $row['season_name']; ?></td>
            <td><?= $row['name']; ?></td>
            <td><?= $row['created_at']; ?></td>
		  </tr>
          <?php endforeach; ?>
		  
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>


<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>As you sure you want to delete.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
  <script type="text/javascript">
      $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
  </script>

<script>
$("#view_history").addClass('active');
</script>
