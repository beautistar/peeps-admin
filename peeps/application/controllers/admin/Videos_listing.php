<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Videos_listing extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('admin/video_listing_model', 'video_listing_model');
		$this->load->model('admin/video_model', 'video_model');
		$this->load->model('admin/category_model', 'category_model');
		$this->load->model('admin/subcategory_model', 'subcategory_model');

	}

	public function index()
	{
		$data['user_playlist'] =  $this->video_listing_model->get_all_videoslist_by_user($id);
		$data['all_lists'] =  $this->video_listing_model->get_videos_by_id($id);
		$data['title'] = 'Videos List';
		$data['view'] = 'admin/videos_listing/video_list';
		$this->load->view('admin/layout', $data);
	}
	public function playlists()
	{
		$data['all_playlists'] = $this->video_listing_model->get_videos();
		$data['title'] = 'Videos List';
		$data['view'] = 'admin/videos_listing/video_playlist_listing';
		$this->load->view('admin/layout', $data);
	}

	/* public function add()
	{
		if($this->input->post('submit'))
		{
			$this->form_validation->set_rules('title', 'Video Title', 'trim|required');
			$this->form_validation->set_rules('video_list', 'Video Selection', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data['title'] = 'Add Videos List';
				//$data['category'] = $this->video_listing_model->get_all_category();
				$data['view'] = 'admin/videos_listing/video_list_add';
				$this->load->view('admin/layout', $data);
			}
			else
			{
				$data = array(
					'video_title' => $this->input->post('title'),
					'video_list' => $this->input->post('video_list'),
					'created_at' => date('Y-m-d : h:m:s'),
					'updated_at' => date('Y-m-d : h:m:s')
				);
				$data = $this->security->xss_clean($data);
				$result = $this->video_listing_model->add_video_list($data);
				if($result)
				{
					$this->session->set_flashdata('msg', 'Video has been Added Successfully!');
					redirect(base_url('admin/videos_listing'));
				}
			}
		}
		else
		{
			$data['videos_list'] = $this->video_listing_model->get_videos();
			$data['title'] = 'Add Playlist';
			$data['view'] = 'admin/videos_listing/video_list_add';
			$this->load->view('admin/layout', $data);
		}
	} */

	//  Edit User's Playlist
	public function edit($id = 0){
		$data['all_videos'] =  $this->video_model->get_videos_by_rowid($id);
		if($this->input->post('submit')){
			$videourl = $this->input->post('video_update');	
			$this->form_validation->set_rules('video_title_update', 'Video Title', 'trim|required');
			$this->form_validation->set_rules('video_description_update', 'Video Description', 'trim|required');
			$this->form_validation->set_rules('category_id_updated', 'Category', 'trim|required');
			$video_current = $this->input->post('video_update');
			/* if(!empty($video_current)){
				$this->form_validation->set_rules('video_update', '', 'callback_file_check_update');
			} */
			if ($this->form_validation->run() == FALSE) {
				$data['video'] = $this->video_model->get_videos_by_id($id);
				$data['title'] = 'Edit Video';
				$data['view'] = 'admin/videos_listing/video_list_edit';
				$this->load->view('admin/layout', $data);
			}
			else{
				if(($_FILES['video_update']['name'] != "" ) &&($viedeourl == "")){
					$new_name = time();
					$file_ext = pathinfo($_FILES["video_update"]['name'],PATHINFO_EXTENSION);
					$video_name = $new_name.".".$file_ext;
					$config = array(
						'file_name' => $video_name,
						'upload_path' => './uploads/videos/',
						'allowed_types' => "wmv|mp4|avi|mov|webm|3gp",
						'overwrite' => TRUE,
						'max_size' => "100000",
					);
					$this->load->library('upload', $config);
					if(!$this->upload->do_upload('video_update')){
						$error = array('error'=>$this->upload->display_errors());
						$this->load->view('video_model',$error);
					}else{
						$data = array(
							'file_type' => 'File',
							'user_id' => $this->input->post('user_id'),
							'playlist_id' => $this->input->post('playlist_id'),
							'video_title' => $this->input->post('video_title_update'),
							'video_description' => $this->input->post('video_description_update'),
							'category_id' => $this->input->post('category_id_updated'),
							'video_upload' => base_url()."uploads/videos/".$video_name,
							'updated_at' => date('Y-m-d : h:m:s')
						);
						$data = $this->security->xss_clean($data);
						$result = $this->video__listing_model->edit_video($data, $id);
						if($result){
							$this->session->set_flashdata('msg', 'Fields have been Updated Successfully!');
							redirect(base_url('admin/videos'));
						}
					}
				}else if(($_FILES['video_update']['name'] == "") && ($videourl != "")){
					$data = array(
						'file_type' => 'URL',
						'user_id' => $this->input->post('user_id'),
						'playlist_id' => $this->input->post('playlist_id'),
						'video_title' => $this->input->post('video_title_update'),
						'video_description' => $this->input->post('video_description_update'),
						'category_id' => $this->input->post('category_id_updated'),
						'video_upload' => $videourl,
						'updated_at' => date('Y-m-d : h:m:s')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->video_listing_model->edit_video($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'Fields have been Updated Successfully!');
						redirect(base_url('admin/videos'));
					}
				}
				else if(($_FILES['video_update']['name'] == "") && ($videourl == "")){
					$video_name = $this->input->post('old');
					$filetype = $this->input->post('old_filetype');
					$data = array(
						'file_type' => $filetype,
						'user_id' => $this->input->post('user_id'),
						'playlist_id' => $this->input->post('playlist_id'),
						'video_title' => $this->input->post('video_title_update'),
						'video_description' => $this->input->post('video_description_update'),
						'category_id' => $this->input->post('category_id_updated'),
						'video_upload' => $video_name,
						'updated_at' => date('Y-m-d : h:m:s')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->video_listing_model->edit_video($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'Fields have been Updated Successfully!');
						redirect(base_url('admin/videos'));
					}
				}
			}
		}
		else{
			$data['video'] = $this->video_listing_model->get_videos_by_playlistid($id);
			$data['category'] = $this->video_model->get_all_category();
			$data['subcategory'] = $this->subcategory_model->get_subcategory_by_id($id);
			$data['category_groups'] = $this->category_model->get_all_category();
			$data['title'] = 'Edit Video';
			$data['view'] = 'admin/videos_listing/video_list_edit';
			$this->load->view('admin/layout', $data);
		}
	}
	
	
	public function edit_playlist($id = 0){
		if($this->input->post('submit')){
			$userid = $this->input->post('user_id');
			$this->form_validation->set_rules('video_title', 'Playlist Title', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				$data['video'] = $this->video_model->get_videos_by_id($id);
				$data['title'] = 'Edit Video';
				$data['view'] = 'admin/videos_listing/video_list_edit';
				$this->load->view('admin/layout', $data);
			}
			else{
				$data = array(
					'video_title' => $this->input->post('video_title'),
					'videos' => $this->input->post('videos'),
					'user_id' => $this->input->post('user_id'),
					'updated_at' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$this->db->where('id',$id);
				$result = $this->db->update('ci_video_list', $data);
				if($_FILES['video_update']['name'] != "" && $_POST['video_url'] == ""){
					$new_name = time();
					$file_ext = pathinfo($_FILES["video_update"]['name'],PATHINFO_EXTENSION);
					$video_name = $new_name.".".$file_ext;
					$config = array(
						'file_name' => $video_name,
						'upload_path' => './uploads/videos/',
						'allowed_types' => "wmv|mp4|avi|mov|webm|3gp",
						'overwrite' => TRUE,
						'max_size' => "100000",
					);
					$this->load->library('upload', $config);
					if(!$this->upload->do_upload('video_update')){
						$error = array('error'=>$this->upload->display_errors());
						$this->load->view('video_model',$error);
					}else{
						$data2 = array(
							'file_type' => 'File',
							'video_upload' => base_url()."uploads/videos/".$video_name,
							'updated_at' => date('Y-m-d : h:m:s')
						);
						$data2 = $this->security->xss_clean($data2);
						$this->db->where('ci_video.user_id',$userid);
						$result2 = $this->db->update('ci_video', $data2);
					}
				}else if($_FILES['video_update']['name'] == "" && $_POST['video_url'] != ""){
					$data2 = array(
						'file_type' => 'URL',
						'video_upload' => $this->input->post('video_url'),
						'updated_at' => date('Y-m-d : h:m:s')
					);
					$data2 = $this->security->xss_clean($data2);
					$this->db->where('ci_video.user_id',$userid);
					$result2 = $this->db->update('ci_video', $data2);
				}else{
					$data2 = array(
						'file_type' => $this->input->post('old_filetype'),
						'video_upload' => $this->input->post('old_video'),
						'updated_at' => date('Y-m-d : h:m:s')
					);
					$data2 = $this->security->xss_clean($data2);
					$this->db->where('ci_video.user_id',$userid);
					$result2 = $this->db->update('ci_video', $data2);
				}
				if($result){
					$this->session->set_flashdata('msg', 'Record has been Updated Successfully!');
					redirect(base_url('admin/videos_listing/playlists'));
				}else if($result2){
					$this->session->set_flashdata('msg', 'Record has been Updated Successfully!');
					redirect(base_url('admin/videos_listing/playlists'));
				}
			}
		}
		else{
			$data['playlists'] = $this->video_listing_model->get_listing_by_id($id);
			$data['video'] = $this->video_model->get_videos_by_id($id);
			$data['title'] = 'Edit Playlist';
			$data['view'] = 'admin/videos_listing/video_playlist_edit';
			$this->load->view('admin/layout', $data);
		}
	}

	
	//  Delete Video 
	public function del($id = 0){
		$this->db->select('*');
		$this->db->from('ci_video');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->row_array();
		$image = $result['video_upload'];
		$this->db->delete('ci_video', array('id' => $id));
		unlink('uploads/videos/'.$image);
		$this->session->set_flashdata('msg', 'Record has been Deleted Successfully!');
		redirect(base_url('admin/videos'));
	}
	
	//Delete Playlist
	public function del_list($id = 0){
		$this->db->select('*');
		$this->db->from('ci_video_list');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->row_array();
		$this->db->delete('ci_video_list', array('id' => $id));
		$this->session->set_flashdata('msg', 'Record has been Deleted Successfully!');
		redirect(base_url('admin/videos_listing/playlists/')); 
	}

	
	//View all playlists
	public function view_all($id= null){
			$data['all_lists'] = $this->video_listing_model->listing_by_user_id($id);
			//$data['playlist'] =  $this->video_listing_model->get_all_videoslist_by_user($id);
			$data['playlist'] =  $this->video_listing_model->get_user_id($id);
			$data['title'] = 'Your Playlist';
			$data['view'] = 'admin/videos_listing/video_play_list';
			$this->load->view('admin/layout', $data);
		}
	}
	
?>
