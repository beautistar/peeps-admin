 <!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h4><i class="fa fa-list"></i> &nbsp; Seasons</h4>
        </div>
        <?php $admin = $this->session->userdata('name');
        if($admin == "Admin"){?>                 
          <div class="col-md-6 text-right">
            <div class="btn-group margin-bottom-20" >
              <a href="<?= base_url('admin/seasons/add/'.$series_id); ?>" class="btn btn-success">Add a New Season</a>
              <a href="<?= base_url('admin/tvshows_videos/'); ?>" class="btn btn-success" style="margin-left: 5px;">Back</a>
            </div>
          </div>  
            
        <?php }else{?>
            <div class="col-md-6 text-right">
                <a href="<?= base_url('admin/seasons/'.$series_id); ?>" class="btn btn-success">Back</a>
            </div>    
        <?php }
        ?>        

      </div>
    </div>
  </div>
   <div class="box border-top-solid">
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
            <th>Season No</th>
            <th>Season Name</th>
            <th>Description</th>
            <?php $admin = $this->session->userdata('name');
            if($admin == 'Admin'){?>                
                <th class="text-center">Option</th>
            <?php }?>
        </tr>
        </thead>
        <tbody>
          <?php  
          ?>    
          <?php foreach($all_seasons as $row):
          ?>
          <tr>
            <td><?= $row['id']?></td>
            <td><?= $row['name']; ?></td>
            <td><?= $row['description']; ?></td>
			<td class="text-center">
			  <?php /*<a href="<?= base_url('admin/tvshows_videos/all_episodes/'.$row['season_id']); ?>" class="btn btn-info btn-flat btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Episodes</a>*/?>
			  <a href="<?php echo base_url('admin/episodes/episode_by_season_id/'.$row['id']); ?>" class="btn btn-warning btn-flat btn-xs">View Episodes</a>
			  <a href="<?= base_url('admin/seasons/edit/'.$row['id']); ?>" class="btn btn-info btn-flat btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			  <a data-href="<?= base_url('admin/seasons/del/'.$row['id']); ?>" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
			</td>
          </tr>
          <?php endforeach; ?>
        </tbody>

      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>

<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>



<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
  <script type="text/javascript">
      $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
  </script> 




