 <!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-5">
          <h4><i class="fa fa-list"></i> &nbsp; User List</h4>
        </div>
      </div>
    </div>
  </div>
   <div class="box border-top-solid">
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
			<th>Name</th>
			<th>Email</th>
			<th>MobileNo.</th>
			<th>Country</th>
			<th>Address</th>
			<th>Gender</th>
			<th>UserCategory</th>
			<th>ProfilePhoto</th>
			<th>SubscriptionType</th>
			<th>Watched TV Shows & Movies</th>
			<?php /*<th>Group/Role</th> */?>
			<th>Playlist</th>
			<th style="width: 100px;" class="text-center">Option</th>
        </tr>
        </thead>
        <tbody>
 
		<?php 
           $name = $this->session->userdata('admin_id');
		  // print_r($name);
		     //is_active();
			  //$user_id = $this->session->userdata('admin_id');
			 //$name = $this->session->userdata('admin_id');
	
			 
			  // $new_user = $user_id->is_active;
			   
			  // echo $new_user;
			   /* if($user_id == 1){
				   echo "Online";
			   }else{
				   echo "Disabled";
			   } */
			   
			?>
          <?php foreach($all_users as $row): 
			//echo "<pre>"; print_r($row); echo "</pre>";
		  ?>
          <tr>
            <td><?= $row['firstname']; ?> <?= $row['lastname']; ?></td>
            <td><?= $row['email']; ?></td>
            <td><?= $row['mobile_no']; ?></td>
            <td><?= $row['country']; ?></td>
            <td><?= $row['address']; ?></td>
            <td><?= $row['gender']; ?></td>
            <td><?= $row['user_category_name']; ?></td>
            <td><?php if($row['photo_url'] != ""){?>
				<img src="<?= $row['photo_url']; ?>" height="100px" width="100px">
			<?php }else{ ?>
				Not uploaded
			<?php } ?></td>
			<td><?= $row['subscription_type']; ?></td>
			<td>
				<span><a href="<?= base_url('admin/users/user_watch_history/'.$row['id']); ?>" class="btn btn-info btn-flat btn-xs">TvShows</a></span>
				<span><a href="<?= base_url('admin/users/user_watch_movie_history/'.$row['id']); ?>" class="btn btn-info btn-flat btn-xs">Movies</a><span>
			</td>
            <?php /*<td><span class="btn btn-primary btn-flat btn-xs bg-green"><?= getGroupyName($row['role']) ?><span></td>*/?>
			<td><span><a href="<?= base_url('admin/users/view_playlist/'.$row['id']); ?>" class="btn btn-info btn-flat btn-xs">View Playlist</a></span></td>
            <td class="text-center">
				<span><a href="<?= base_url('admin/users/edit/'.$row['id']); ?>" class="btn btn-info btn-flat btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a></span>
				<span><a data-href="<?= base_url('admin/users/del/'.$row['id']); ?>" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a></span>
			</td>
		  </tr>
          <?php endforeach; ?>
		  
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>


<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>As you sure you want to delete.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>

<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
  <script type="text/javascript">
      $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
  </script>

<script>
//$("#view_users").addClass('active');
</script>
