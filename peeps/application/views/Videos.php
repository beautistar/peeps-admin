<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('admin/video_model', 'video_model');
		$this->load->model('admin/user_model', 'user_model');
		
	}
	
	public function index()
	{
		$data['all_videos'] =  $this->video_model->get_all_videos();
		$data['subcategory'] = $this->video_model->get_all_subcategory();
		$data['title'] = 'Video List';
		$data['view'] = 'admin/videos/video_list';
		$this->load->view('admin/layout', $data);
		//$this->load->view('video_add', array('error'=>''));
	}	
	
	public function add($id)
	{
		//print_r($_GET);
		die($id);
		
		if($this->input->post('submit'))
		{
			$new_name = time();
			$file_ext = pathinfo($_FILES["video_upload"]['name'],PATHINFO_EXTENSION);
			$video_name = $new_name.".".$file_ext;
			$config['file_name'] = $video_name;
		
			$this->form_validation->set_rules('video_title', 'Video Title', 'trim|required');
			$this->form_validation->set_rules('video_description', 'Video Description', 'trim|required');
			$this->form_validation->set_rules('category_id', 'Category', 'trim|required');
			$this->form_validation->set_rules('subcategory_id', 'Subcategory', 'trim|required');
			$this->form_validation->set_rules('video_upload', '', 'callback_file_check');
			
			
			if ($this->form_validation->run() == FALSE) 
			{
				$data['title'] = 'Add Video';
				$data['subcategory'] = $this->video_model->get_all_childcategory();
				$data['category'] = $this->video_model->get_all_category();
				$data['view'] = 'admin/videos/video_add';
				$this->load->view('admin/layout', $data);
			}
			else
			{
				//$data['all_users'] = $this->video_model->get_user_by_id($id);	
				$config = array
				(
					'file_name'=> $video_name,
					'upload_path'   => "./uploads/videos/",
					'allowed_types' => "wmv|mp4|avi|mov|webm|3gp",
					'overwrite' => TRUE,
					'max_size' => "100000",
				);
			
				$this->load->library('upload', $config);
				if(!$this->upload->do_upload('video_upload'))
				{
					$error = array('error'=>$this->upload->display_errors());
					$this->load->view('video_model',$error);
				}
				else
				{
					$data = array(
							'video_title' => $this->input->post('video_title'),
							'video_description' => $this->input->post('video_description'),
							'category_id' => $this->input->post('category_id'),
							'subcategory_id' => $this->input->post('subcategory_id'),
							'video_upload' => $video_name,
							'create_date' => date('Y-m-d : h:m:s')
						);
					$data = $this->security->xss_clean($data);
					$result = $this->video_model->add_video_by_id($id,$data);
					if($result)
					{
						$this->session->set_flashdata('msg', 'Video has been Added Successfully!');
						redirect(base_url('admin/videos'));
					}
				}
			}
		}
		else
		{
			echo "+++++++".$id;
			$data['subcategory'] = $this->video_model->get_all_childcategory();
			$data['category'] = $this->video_model->get_all_category();
			//$data['all_users'] = $this->video_model->get_user_by_id($id);
			$data['title'] = 'Add Videos';
			$data['view'] = 'admin/videos/video_add';
			$this->load->view('admin/layout', $data);
		}
	}
	
	
	//Edit
	public function edit($id = 0){
		if($this->input->post('submit')){
			$this->form_validation->set_rules('video_title_update', 'Video Title', 'trim|required');
			$this->form_validation->set_rules('video_description_update', 'Video Description', 'trim|required');
			$this->form_validation->set_rules('category_id_updated', 'Category', 'trim|required');
			$video_current = $this->input->post('video_update');
			if(!empty($video_current)){
				$this->form_validation->set_rules('video_update', '', 'callback_file_check_update');
			}
			if ($this->form_validation->run() == FALSE) {
				$data['video'] = $this->video_model->get_videos_by_id($id);
				$data['title'] = 'Edit Video';
				$data['view'] = 'admin/videos/video_edit';
				$this->load->view('admin/layout', $data);
			}
			else{
				if($_FILES['video_update']['name'] != ""){
					$new_name = time();
					$file_ext = pathinfo($_FILES["video_update"]['name'],PATHINFO_EXTENSION);
					$video_name = $new_name.".".$file_ext;
					$config = array(
						'file_name' => $video_name,
						'upload_path' => './uploads/videos/',
						'allowed_types' => "wmv|mp4|avi|mov|webm|3gp",
						'overwrite' => TRUE,
						'max_size' => "100000",
					);
					$this->load->library('upload', $config);
					if(!$this->upload->do_upload('video_update')){
						$error = array('error'=>$this->upload->display_errors());
						$this->load->view('video_model',$error);
					}else{
						$data = array(
							'video_title' => $this->input->post('video_title_update'),
							'video_description' => $this->input->post('video_description_update'),
							'category_id' => $this->input->post('category_id_updated'),
							'video_upload' => $video_name,
							'update_date' => date('Y-m-d : h:m:s')
						);
						$data = $this->security->xss_clean($data);
						$result = $this->video_model->edit_video($data, $id);
						if($result){
							$this->session->set_flashdata('msg', 'Fields have been Updated Successfully!');
							redirect(base_url('admin/videos'));
						}
					}
				}else{
					$video_name = $this->input->post('old');
					$data = array(
						'video_title' => $this->input->post('video_title_update'),
						'video_description' => $this->input->post('video_description_update'),
						'category_id' => $this->input->post('category_id_updated'),
						'video_upload' => $video_name,
						'update_date' => date('Y-m-d : h:m:s')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->video_model->edit_video($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'Fields have been Updated Successfully!');
						redirect(base_url('admin/videos'));
					}
				}
			}
		}
		else{
			$data['video'] = $this->video_model->get_videos_by_id($id);
			$data['category'] = $this->video_model->get_all_category();
			$data['subcategory'] = $this->video_model->get_all_subcategory();
			//$data['user_groups'] = $this->user_model->get_user_groups();
			$data['title'] = 'Edit Video';
			$data['view'] = 'admin/videos/video_edit';
			$this->load->view('admin/layout', $data);
		}
	}
		
	//  Delete Video
	public function del($id = 0){
		$this->db->select('video_upload');
		$this->db->from('ci_video');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->row_array();
		$image = $result['video_upload'];	
		$this->db->delete('ci_video', array('id' => $id));
		unlink('uploads/videos/'.$image);
		$this->session->set_flashdata('msg', 'Video has been Deleted Successfully!');
		redirect(base_url('admin/videos'));
	}
		
		
	//File type validation	
	public function file_check($str){
		$allowed_mime_type_arr = array('video/mp4','video/3gp','video/mp3','video/mov','video/wmv','video/webm');
		//$file_size_max = 100000;
        $mime = get_mime_by_extension($_FILES['video_upload']['name']);
        if(isset($_FILES['video_upload']['name']) && $_FILES['video_upload']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            } else{
                $this->form_validation->set_message('file_check', 'Please select only mp4/3gp/mp3/mov/wmv/webm formats.');
                return false;
            } 
        }else{
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    }	
	
	//File type validation	
	public function file_check_update($str){
		$allowed_mime_type_arr = array('video/mp4','video/3gp','video/mp3','video/mov','video/wmv','video/webm');
        $mime = get_mime_by_extension($_FILES['video_update']['name']);
        if(isset($_FILES['video_update']['name']) && $_FILES['video_update']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            } else{
                $this->form_validation->set_message('file_check_update', 'Please select only mp4/3gp/mp3/mov/wmv/webm formats.');
                return false;
            } 
        }else{
            $this->form_validation->set_message('file_check_update', 'Please choose a file to upload.');
            return false;
        }
    }
}

?>