<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Users extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/user_model', 'user_model');
			$this->load->model('admin/video_listing_model', 'video_listing_model');
			$this->load->model('admin/category_model', 'category_model');
			$this->load->model('admin/video_listing_model', 'video_listing_model');
			$this->load->model('admin/user_category_model', 'user_category_model');
		}

		public function index(){
			$data['all_playlists'] = $this->video_listing_model->get_videos();	
			//$data['userplaylist'] = $this->user_model->playlist_by_user();	
			$data['playlists'] = $this->user_model->get_playlists();
			$data['all_users'] =  $this->user_model->get_all_users();
			//$data['user_history'] = $this->user_model->get_user_history();
			$data['title'] = 'User List';
			$data['view'] = 'admin/users/user_list';
			$this->load->view('admin/layout', $data);
		}
		
		//---------------------------------------------------------------
		//  Add User
		public function add(){
			///$this->load->library('session');
			if($this->input->post('submit')){
				$new_name = time();
				$file_ext = pathinfo($_FILES["photo_url"]['name'],PATHINFO_EXTENSION);
				$filename = $new_name.".".$file_ext;
				$config['file_name'] = $filename;
				$this->form_validation->set_rules('username', 'Username', 'trim|min_length[5]|required');
				$this->form_validation->set_rules('firstname', 'Firstname', 'trim|required');
				$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
				$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|is_unique[ci_users.email]|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required');
				$this->form_validation->set_rules('country', 'Country', 'trim|required');
				$this->form_validation->set_rules('address', 'Address', 'trim|required');
				$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
				//$this->form_validation->set_rules('user_category', 'Category', 'trim|required');
				$this->form_validation->set_rules('photo_url', '', 'callback_file_check');
				//$this->form_validation->set_rules('group', 'Group', 'trim|required'); 

				if ($this->form_validation->run() == FALSE) {
					
					$data['title'] = 'Add User';
					$data['user_groups'] = $this->user_model->get_user_groups();
					$data['user_category'] = $this->user_category_model->get_all_user_category();
					$data['view'] = 'admin/users/user_add';
					$this->load->view('admin/layout', $data);
					
					
				}else{
					$config = array
					(
						'file_name'=> $filename,
						'upload_path'   => "./uploads/profile-pictures/",
						'allowed_types' => 'jpg|png|jpeg',
						'overwrite' => TRUE,
						'max_size' => "100000"
					);
					$this->load->library('upload', $config);
					if(!$this->upload->do_upload('photo_url'))
					{
						$error = array('error'=>$this->upload->display_errors());
						$this->load->view('user_model',$error);
					}
					else
					{
						$data = array(
							'photo_url' =>base_url()."uploads/profile-pictures/".$filename,
							'username' => $this->input->post('username'),
							'firstname' => $this->input->post('firstname'),
							'lastname' => $this->input->post('lastname'),
							'email' => $this->input->post('email'),
							'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT),
							'mobile_no' => $this->input->post('mobile_no'),
							'country' => $this->input->post('country'),
							'address' => $this->input->post('address'),
							'gender' => $this->input->post('gender'),
							//'user_category' => 'default',
							'subscription_type' =>$this->input->post('subscription_type'),
							'begin_date' => $this->input->post('begin_date'),
							'end_date' => $this->input->post('end_date'),
							'role' => $this->input->post('group'), 
							'created_at' => date('Y-m-d : h:m:s'),
							'updated_at' => date('Y-m-d : h:m:s'),
						);
						$data = $this->security->xss_clean($data);
						$result = $this->user_model->add_user($data);
						if($result){
							$this->session->set_flashdata('msg', 'User has been Added Successfully!');
							redirect(base_url('admin/users'));
						}
					}
				}
			}
			else{
				$data['playlists'] = $this->user_model->get_playlists();	
				$data['user_groups'] = $this->user_model->get_user_groups();
				$data['user_category'] = $this->user_category_model->get_all_user_category();	
				$data['category_groups'] = $this->category_model->get_all_category();
				$data['title'] = 'Add User';
				$data['view'] = 'admin/users/user_add';
				$this->load->view('admin/layout', $data);
			}
			
		}

		//---------------------------------------------------------------
		//  Edit User
		public function edit($id = 0){	
			$old_image = $this->input->post('old_photo');	
			if($this->input->post('submit')){
				$this->form_validation->set_rules('username', 'Username', 'trim|required');
				$this->form_validation->set_rules('firstname', 'Username', 'trim|required');
				$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
				$this->form_validation->set_rules('email', 'Email', 'trim|required');
			 	$this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required');
				$this->form_validation->set_rules('country', 'Country', 'trim|required');
				$this->form_validation->set_rules('address', 'Address', 'trim|required');
				$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
				//$this->form_validation->set_rules('user_category', 'Category', 'trim|required');
				if(!empty($_FILES['photo_url']['name'])){
					$this->form_validation->set_rules('photo_url', '', 'callback_file_check');
				}
				//$this->form_validation->set_rules('status', 'Status', 'trim|required');
				$this->form_validation->set_rules('group', 'Group', 'trim|required'); 

				if ($this->form_validation->run() == FALSE) {
					$data['user'] = $this->user_model->get_user_by_id($id);
					$data['user_groups'] = $this->user_model->get_user_groups();
					$data['title'] = 'Edit User';
					$data['view'] = 'admin/users/user_edit';
					$this->load->view('admin/layout', $data);
				}
				else{
					if(($_FILES['photo_url']['name'] != "")){
						$new_name = time();
						$file_ext = pathinfo($_FILES["photo_url"]['name'],PATHINFO_EXTENSION);
						$filename = $new_name.".".$file_ext;
						$config = array(
							'file_name' => $filename,
							'upload_path' => './uploads/profile-pictures/',
							'allowed_types' => 'jpg|png|jpeg',
							'overwrite' => TRUE,
							'max_size' => "100000",
						);
						$this->load->library('upload', $config);
						if(!$this->upload->do_upload('photo_url')){
							$error = array('error'=>$this->upload->display_errors());
							$this->load->view('user_model',$error);
						}else{
							$data = array(
								'photo_url' => base_url()."uploads/profile-pictures/".$filename,
								'username' => $this->input->post('username'),
								'firstname' => $this->input->post('firstname'),
								'lastname' => $this->input->post('lastname'),
								'email' => $this->input->post('email'),
								'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT),
								'mobile_no' => $this->input->post('mobile_no'),
								'country' => $this->input->post('country'),
								'address' => $this->input->post('address'),
								'gender' => $this->input->post('gender'),
								//'user_category' => $this->input->post('user_category'),
								'subscription_type' =>$this->input->post('subscription_type'),
								'begin_date' => $this->input->post('old_begin_date'),
								'end_date' => $this->input->post('old_end_date'),
								'role' => $this->input->post('group'),
								//'is_active' => $this->input->post('status'), 
								'updated_at' => date('Y-m-d : h:m:s'),
							);
							$data = $this->security->xss_clean($data);
							$result = $this->user_model->edit_user($data, $id);
							if($result){
								$this->session->set_flashdata('msg', 'User has been Updated Successfully!');
								redirect(base_url('admin/users'));
							}
						}
					}else{
						$data = array(
							'photo_url' => $old_image,	
							'username' => $this->input->post('username'),
							'firstname' => $this->input->post('firstname'),
							'lastname' => $this->input->post('lastname'),
							'email' => $this->input->post('email'),
							'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT),
							'mobile_no' => $this->input->post('mobile_no'),
							'country' => $this->input->post('country'),
							'address' => $this->input->post('address'),
							'gender' => $this->input->post('gender'),
							//'user_category' => $this->input->post('user_category'),
							'subscription_type' =>$this->input->post('subscription_type'),
							'begin_date' => $this->input->post('old_begin_date'),
							'end_date' => $this->input->post('old_end_date'),
							'role' => $this->input->post('group'),
							//'is_active' => $this->input->post('status'), 
							'updated_at' => date('Y-m-d : h:m:s'),
						);
						$data = $this->security->xss_clean($data);
						$result = $this->user_model->edit_user($data, $id);
						if($result){
							$this->session->set_flashdata('msg', 'User has been Updated Successfully!');
							redirect(base_url('admin/users'));
						}
					}
				}
			}
			else{
				$data['user'] = $this->user_model->get_user_by_id($id);
				$data['playlists'] = $this->user_model->get_playlists();
				$data['user_groups'] = $this->user_model->get_user_groups();
				$data['user_category'] = $this->user_category_model->get_all_user_category();
				$data['title'] = 'Edit User';
				$data['view'] = 'admin/users/user_edit';
				$this->load->view('admin/layout', $data);
			}
		}
        
		//---------------------------------------------------------------
		//  Delete Users
		public function del($id = 0){
			$this->db->delete('ci_users', array('id' => $id));
			$this->session->set_flashdata('msg', 'User has been Deleted Successfully!');
			redirect(base_url('admin/users'));
		}

		//Redirect to Playlist
		public function view_playlist($id = null){
			$query = $this->db->get_where('ci_video_list', array('user_id' => $id));
			$result = $query->row_array();
			$data['all_lists'] =  $this->video_listing_model->get_all_videoslist_by_user($id);
			$data['title'] = 'Videos List';
			$data['view'] = 'admin/videos_listing/video_list';
			$this->load->view('admin/layout', $data); 
		}
		
		
		//---------------------------------------------------------------
		//  Export Users PDF 
		public function create_users_pdf(){
			$this->load->helper('pdf_helper'); // loaded pdf helper
			$data['all_users'] = $this->user_model->get_all_users();
			$this->load->view('admin/users/users_pdf', $data);
		}

		//---------------------------------------------------------------	
		// Export data in CSV format 
		public function export_csv(){ 
		   // file name 
		   $filename = 'users_'.date('Y-m-d').'.csv'; 
		   header("Content-Description: File Transfer"); 
		   header("Content-Disposition: attachment; filename=$filename"); 
		   header("Content-Type: application/csv; ");
		   
		   // get data 
		   $user_data = $this->user_model->get_all_users_for_csv();

		   // file creation 
		   $file = fopen('php://output', 'w');
		 
		   $header = array("ID", "Username", "First Name", "Last Name", "Email", "Mobile_no", "Created Date"); 
		   fputcsv($file, $header);
		   foreach ($user_data as $key=>$line){ 
		     fputcsv($file,$line); 
		   }
		   fclose($file); 
		   exit; 
		  }
		  
		//File Type validation  
		public function file_check($str){
			$allowed_mime_type_arr = array('image/jpeg','image/pjpeg','image/png','image/x-png','image/jpg');
			$mime = get_mime_by_extension($_FILES['photo_url']['name']);
			if(isset($_FILES['photo_url']['name']) && $_FILES['photo_url']['name']!=""){
				if(in_array($mime, $allowed_mime_type_arr)){
					return true;
				}else{
					$this->form_validation->set_message('file_check', 'Please select only pdf/gif/jpg/png file.');
					return false;
				}
			}else{
				$this->form_validation->set_message('file_check', 'Please choose a file to upload.');
				return false;
			}
		}
		
		public function user_watch_history($id=0){
			$data['history'] =  $this->user_model->user_history($id);
			//$data['user_history'] = $this->user_model->get_user_history();
			$data['title'] = 'User history list';
			$data['view'] = 'admin/users/user_history_list';
			$this->load->view('admin/layout', $data);
		}
		
		public function user_watch_movie_history($id=0){
			$data['history'] =  $this->user_model->user_movie_history($id);
			//$data['user_history'] = $this->user_model->get_user_history();
			$data['title'] = 'User history list';
			$data['view'] = 'admin/users/user_movie_history_list';
			$this->load->view('admin/layout', $data);
		}
	}

?>