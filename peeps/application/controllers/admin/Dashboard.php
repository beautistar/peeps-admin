<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Dashboard extends MY_Controller {
		public function __construct(){
			parent::__construct();
			//$this->load->helper('form');
			$this->load->model('admin/dashboard_model', 'dashboard_model');

		}

		public function index(){
			$data['all_users'] = $this->dashboard_model->total_users();
			$data['title'] = 'Dashboard 1';
			$data['view'] = 'admin/dashboard/index';
			$this->load->view('admin/layout', $data);
		}

		public function index2(){
			$data['title'] = 'Dashboard 2';
			$data['view'] = 'admin/dashboard/index2';
			$this->load->view('admin/layout', $data);
		}
	}

?>	