<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body with-border">
        <div class="col-md-6">
          <h4><i class="fa fa-plus"></i> &nbsp; Edit Season</h4>
        </div>	
		<div class="col-md-6 text-right">
			<a href="<?= base_url('admin/seasons/seasons_by_series_id/'.$season['series_id']); ?>" class="btn btn-success"> Back</a>
		</div>
		<?php /*<div class="col-md-3 text-left">
			<a href="<?= base_url('admin/episodes/add'); ?>" class="btn btn-success"> Add Episode</a>
		</div> */?>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
            <?php echo form_open(base_url('admin/seasons/edit/'.$season['id']), 'class="form-horizontal"');  ?>
				
                <input type="hidden" name="series_id" value="<?= $season['series_id'];?>">
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label">Season Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="name" id="name" value="<?= $season['name']; ?>">
					</div>
				</div>
				
				
				<div class="form-group">
					<label for="description" class="col-sm-2 control-label">Description</label>
					<div class="col-sm-9">
						<textarea type="text" id="description" name="description" class="form-control" rows="3"><?= $season['description']; ?></textarea>
					</div>
				</div>	

				<div class="form-group">
					<div class="row">
						<div class="col-md-11">
						  <input type="submit" name="submit" value="Update season" class="btn btn-info pull-right">
						</div>
					</div>
				</div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>


<script>
$(document).ready(function(){
	$('#url').on('click',function(){
		$('#video_file').hide();
		$('#video_url').show();
	});
	$('#file').on('click',function(){
		$('#video_url').hide();
		$('#video_file').show();
	});
});
</script>
