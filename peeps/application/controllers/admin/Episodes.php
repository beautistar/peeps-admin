<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Episodes extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('admin/video_model', 'video_model');
		$this->load->model('admin/video_listing_model', 'video_listing_model');
		$this->load->model('admin/subcategory_model', 'subcategory_model');
		$this->load->model('admin/category_model', 'category_model');
		$this->load->model('admin/tvshow_video_model', 'tvshow_video_model');
		$this->load->model('admin/episode_model', 'episode_model');
		$this->load->model('admin/notification_model', 'notification_model');

	}
    
    function send_notifications($tokens, $title, $body) {
    
        // send FCM push notification
        
        $url = "https://fcm.googleapis.com/fcm/send";
        $api_key = "AAAArlvJd04:APA91bGB6h4Rf5B1imiYnzYuquziITAuXqrsGU6cBpooVTxXBVi4Vc_zt4RNNapK4INHeSixFXgzXasPpGKz-KX6ik-wff8Y0QeTPeFjAjLjyiagrT2gimmytvkFBD3zXMSLHg3YyOzp";
        
        if (count($tokens) == 0 ) {

            return;
        }        
        
        $msg = array
                (
                    'body'          => $body,
                    'title'         => $title,   
                    'badge'         => 1,             
                    'sound'         => 'default'/*Default sound*/
                );
                
        $data = array('msgType'     => "notification",
                      'content'     => $body);
                      
        $fields = array
            (
                'registration_ids'    => $tokens,                
                'notification'      => $msg,
                'priority'          => 'high',
                'data'              => $data
            );

        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        
        $result['result'] = curl_exec($ch); 

        curl_close($ch); 
        
        return $result;    
    }
    
    public function index()
	{
		
        //$data['all_series'] =  $this->episode_model->get_all_series();
		$data['all_episodes'] = $this->episode_model->get_all_episodes();
		$data['title'] = 'Episode List';
		$data['view'] = 'admin/episodes/episode_list';
		$this->load->view('admin/layout', $data);
	}
    
    public function episode_by_season_id($id = 0) {
        
        $data['all_episodes'] =  $this->episode_model->get_episode_by_season_id($id);
        $data['season_id'] = $id;
        $data['title'] = 'Episode List';
        $data['view'] = 'admin/episodes/episode_list';
        $this->load->view('admin/layout', $data);
    }

    // $id is season id
	public function add($id=0){
		
        //$data['all_episodes'] =  $this->episode_model->get_episode_by_season_id($id);
		$data['season_id'] = $id;
        $series_id = $this->episode_model->get_series_id_by_season($id);
        print_r($data);
		if($this->input->post('submit')){
			$new_name = time();
			$file_ext = pathinfo($_FILES["episode_video"]['name'],PATHINFO_EXTENSION);
			$video_name = $new_name.".".$file_ext;
			$config['file_name'] = $video_name;
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('description', 'Description', 'trim|required');
			$this->form_validation->set_rules('duration', 'Duration', 'trim|required');
			$this->form_validation->set_rules('episode_video', '', 'callback_file_check');
			if ($this->form_validation->run() == FALSE)
			{
				$data['title'] = 'Add Video';
				$data['category'] = $this->video_model->get_all_category();
				$data['view'] = 'admin/episodes';
				$this->load->view('admin/layout', $data);
			}else{
				$config = array
				(
					'file_name'=> $video_name,
					'upload_path'   => "./uploads/videos/",
					'allowed_types' => "wmv|mp4|avi|mov|webm|3gp|m3u",
					'overwrite' => TRUE,
					'max_size' => "100000"
				);
				$this->load->library('upload', $config);
				if(!$this->upload->do_upload('episode_video'))
				{
					//$data['playlistData']=$this->video_listing_model->get_listing_by_id($id);
                    //$data['category'] = $this->video_model->get_all_category();
                    //$data['all_seasons'] = $this->episode_model->get_all_seasons_in_series($data['series_id']);
                    $data['title'] = 'Add Episode';                      
                    $data['error'] = $this->upload->display_errors();
					$data['view'] = 'admin/episodes/episode_add';
                    $this->load->view('admin/layout',$data);
				}
				else
				{
					$data = array(
						'name' => $this->input->post('name'),
						'description' => $this->input->post('description'),
						'hashtags' => $this->input->post('hashtags'),
						'episode_video' => base_url()."uploads/videos/".$video_name,
						'episode_number' => $this->input->post('episode_number'),
						'duration' => $this->input->post('duration'),
						//'display_order' => $this->input->post('display_order'),
						'season_id' => $this->input->post('season_id'),
						'series_id' => $series_id,
						'created_at' => date('Y-m-d h:m:s'),
						'updated_at' => date('Y-m-d h:m:s')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->episode_model->add($data);
					if($result)
					{
						$favdata = $this->notification_model->get_favourite_data($series_id);
						foreach($favdata as $fav){							
							if($fav['mark_fav'] == "yes"){
								
								$registrationIds = array($fav['token']);
							}
                        }
                            
                        $title = 'Peeps';
                        $msg = 'New Episode has been added to the TV series you favorite.';	
						$this->send_notifications($registrationIds, $title, $msg);
						$this->session->set_flashdata('msg', 'Episode has been added sucessfully'); 
						redirect(base_url('admin/episodes/episode_by_season_id/'.$id));
						
					}
				}
			}
		}else{
			//$data['playlistData']=$this->video_listing_model->get_listing_by_id($id);
			//$data['category'] = $this->video_model->get_all_category();
			//$data['all_seasons'] = $this->episode_model->get_all_seasons_in_series($data['series_id']);
			$data['title'] = 'Add Episode';
			$data['view'] = 'admin/episodes/episode_add';
			$this->load->view('admin/layout', $data);
		}
	}
	
	
	//Edit
	public function edit($id = 0){
		if($this->input->post('submit')){
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('description', 'Description', 'trim|required');
			if(!empty($_FILES['episode_video']['name'])){
				$this->form_validation->set_rules('episode_video', '', 'callback_file_check');
			}
			if ($this->form_validation->run() == FALSE) {
				$data['video'] = $this->episode_model->get_episodes_by_id($id);
				$data['title'] = 'Edit Episode';
				$data['view'] = 'admin/episodes/episode_edit';
				$this->load->view('admin/layout', $data);
			}
			else{
				if(($_FILES['episode_video']['name'] != "")){
					$new_name = time();
					$file_ext = pathinfo($_FILES["episode_video"]['name'],PATHINFO_EXTENSION);
					$video_name = $new_name.".".$file_ext;
					$config = array(
						'file_name' => $video_name,
						'upload_path' => './uploads/videos/',
						'allowed_types' => "wmv|mp4|avi|mov|webm|3gp",
						'overwrite' => TRUE,
						'max_size' => "100000",
					);
					$this->load->library('upload', $config);
					if(!$this->upload->do_upload('episode_video')){
                        $data['title'] = 'Add Episode';                      
                        $data['error'] = $this->upload->display_errors();
                        $data['view'] = 'admin/episodes/episode_edit';
                        $this->load->view('admin/layout',$data);
						
					} else {
						$data = array(
							'name' => $this->input->post('name'),
							'description' => $this->input->post('description'),
							'hashtags' => $this->input->post('hashtags'),
							'episode_video' => base_url()."uploads/videos/".$video_name,
							'episode_number' => $this->input->post('episode_number'),
							'duration' => $this->input->post('duration'),							
							'updated_at' => date('Y-m-d h:m:s')
						);
						$data = $this->security->xss_clean($data);
						$result = $this->episode_model->edit_episode($data, $id);
						if($result){
							$this->session->set_flashdata('msg', 'Episode has been Updated Successfully!');
							redirect(base_url('admin/episodes'));
						}
					}
				}else{
					$video_name = $this->input->post('video_old');
					$data = array(
						'name' => $this->input->post('name'),
						'description' => $this->input->post('description'),
						'hashtags' => $this->input->post('hashtags'),
						'episode_video' => $video_name,
						'episode_number' => $this->input->post('episode_number'),
						'duration' => $this->input->post('duration'),						
						'updated_at' => date('Y-m-d h:m:s')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->episode_model->edit_episode($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'Episode has been Updated Successfully!');
						redirect(base_url('admin/episodes'));
					}
				}
			}
		}
		else{
			$data['video'] = $this->episode_model->get_episodes_by_id($id);
			$data['title'] = 'Edit Episode';
			$data['view'] = 'admin/episodes/episode_edit';
			$this->load->view('admin/layout', $data);
		}
	}
	
	//Delete
	public function del($id=0){
		$this->db->select('*');
		$this->db->from('ci_episode');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->row_array();
		$image = $result['episode_video'];
		$this->db->delete('ci_episode', array('id' => $id));
		$spilt_str = explode('/', $image);
        $video = $spilt_str[count($spilt_str)-1];
        unlink('uploads/videos/'.$video);
        $series_id = $this->episode_model->get_series_id_by_season($id);
        $favdata = $this->notification_model->get_favourite_data($series_id);
        foreach($favdata as $fav){                            
            if($fav['mark_fav'] == "yes"){
                
                $registrationIds = array($fav['token']);
            }
        }
            
        $title = 'Peeps';
        $msg = 'Episode has been Deleted from the TV series you favorite.';    
        $this->send_notifications($registrationIds, $title, $msg);
        $this->session->set_flashdata('msg', 'Episode has been Deleted Successfully!'); 
		redirect(base_url('admin/episodes'));
	}
    
    //File type validation
    public function file_check($str){
        $allowed_mime_type_arr = array('video/mp4','video/3gp','video/mp3','video/mov','video/wmv','video/webm');
        //$file_size_max = 100000;
        $mime = get_mime_by_extension($_FILES['episode_video']['name']);
        if(isset($_FILES['episode_video']['name']) && $_FILES['episode_video']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            } else{
                $this->form_validation->set_message('file_check', 'Please select only mp4/3gp/mp3/mov/wmv/webm formats.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    }

    //File type validation
    public function file_check_update($str){
        $allowed_mime_type_arr = array('video/mp4','video/3gp','video/mp3','video/mov','video/wmv','video/webm');
        $mime = get_mime_by_extension($_FILES['episode_video']['name']);
        if(isset($_FILES['episode_video']['name']) && $_FILES['episode_video']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            } else{
                $this->form_validation->set_message('file_check_update', 'Please select only mp4/3gp/mp3/mov/wmv/webm formats.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check_update', 'Please choose a file to upload.');
            return false;
        }
    }
	
	public function user_history(){
		if(isset($_POST['user_id']) && $_POST['user_id'] != ""){
			$json = array(
				'episode_id' => $this->input->post('id'),
				'user_id '=>$this->input->post('user_id'),
				'video' => $this->input->post('video'),
			);
			$video = $json['video'];
			$data = array(
				'episode_id' => $this->input->post('episode_id'),
				'user_id '=>$this->input->post('user_id'),
				'created_at' => date('Y-m-d h:m:s'),
				'updated_at' => date('Y-m-d h:m:s')
			);
			$result = $this->db->insert('ci_user_watch_history', $data);
			$arr = array('episode_id' => $data['episode_id'],
				'user_id '=>$this->input->post('user_id'),
				'video' => $video
			);
			echo json_encode($arr);
		}
	}
	
	

}

?>
