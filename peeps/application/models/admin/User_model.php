<?php
	class User_model extends CI_Model{

		public function add_user($data){
			$this->db->insert('ci_users', $data);
			return true;
		}

		public function get_all_users(){
			//$array = array('is_admin' => 0,'status' => 1);
			$this->db->select('ci_users.*, ci_user_category.user_category_name');
			$this->db->from('ci_users');
			$this->db->join('ci_user_category','ci_user_category.id = ci_users.role',"LEFT");
			$this->db->where('ci_users.is_admin', 0);
			$query = $this->db->get();
			$last = $this->db->last_query();
			return $result = $query->result_array();
			//echo "<pre>"; print_r($result); echo "</pre>";
		}
		public function get_all_users_for_csv(){
			$this->db->where('is_admin', 0);
			$this->db->select('id, username, firstname, lastname, email, mobile_no, created_at');
			$this->db->from('ci_users');
			$query = $this->db->get();
			return $result = $query->result_array();
		}
		

		public function get_user_by_id($id){
			$query = $this->db->get_where('ci_users', array('id' => $id));
			return $result = $query->row_array();
		}

		public function edit_user($data, $id){
			$this->db->where('id', $id);
			$this->db->update('ci_users', $data);
			return true;
		}
		
		public function get_user_groups(){
			$query = $this->db->get('ci_user_groups');
			return $result = $query->result_array();
		}
		public function get_playlists(){
			$query = $this->db->get('ci_video_list');
 			return $result = $query->result_array();
		}
		
		public function activate($user_id){
		$this->db->query("UPDATE users SET is_active = 1 
                  WHERE id =?", array($user_id));
		
         print_r($user_id);		
				  
		}
	/* 	
		public function playlist_by_user(){
			$this->db->select('*');
			$this->db->from('ci_video_list');
			$this->db->join('ci_users', 'ci_users.playlist_id' = 'ci_video_list.id');
			$this->db->where_in('ci_video_list.id', 'ci_user.playlist_id');
			$query = $this->db->get();
			$last = $this->db->last_query();
			echo $last;
			die;
			return $result = $query->row_array();
		} */
		
		
		public function user_history($id){
			$this->db->select('ci_user_watch_history.*, ci_episode.*,ci_seasons.name as season_name, ci_tv_series.series_name, ci_users.firstname, ci_users.lastname'); 
			$this->db->from('ci_user_watch_history');
			$this->db->join('ci_users','ci_users.id = ci_user_watch_history.user_id');
			$this->db->join('ci_episode', 'ci_episode.id = ci_user_watch_history.episode_id'); 
			$this->db->join('ci_seasons', 'ci_seasons.id = ci_episode.season_id'); 
			$this->db->join('ci_tv_series', 'ci_tv_series.id = ci_episode.series_id'); 
			$this->db->where('ci_users.id', $id);
			$query = $this->db->get();
			$last = $this->db->last_query();
			return $result = $query->result_array(); 
			//echo "<pre>"; print_r($result); echo "</pre>";
		}
		
		public function user_movie_history($id){
			$this->db->select('*');
			$this->db->from('ci_user_watch_history');
			$this->db->join('ci_users', 'ci_users.id = ci_user_watch_history.user_id');
			$this->db->join('ci_movies', 'ci_movies.id = ci_user_watch_history.movie_id');
			$this->db->join('ci_category', 'ci_category.id = ci_movies.category_id');
			$this->db->join('ci_subcategory', 'ci_subcategory.id = ci_movies.subcategory_id');
			$this->db->where('ci_users.id', $id);
			$query = $this->db->get();
			$last = $this->db->last_query();
			return $result = $query->result_array(); 
			//echo "<pre>"; print_r($result); echo "</pre>";
		}
		
		public function getToken($user_id) {      
		
			return $this->db->where('id', $user_id)->get('ci_users')->row_array()['token'];
       
	  }
	}

?>