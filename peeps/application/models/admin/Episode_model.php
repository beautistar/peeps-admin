<?php
class Episode_model extends CI_Model{
    
    public function get_episode_by_season_id($id) {
        
        $this->db->select('ci_episode.*,ci_seasons.name as season_name');
        $this->db->from('ci_episode');
        $this->db->where('ci_episode.season_id', $id);
        $this->db->join('ci_seasons','ci_seasons.id = ci_episode.season_id',"LEFT");        
        $query = $this->db->get();
        $last = $this->db->last_query();
        return $result = $query->result_array();
        
        
     /*    $query = $this->db->get('ci_episode');
        return $query->result_array(); */
    }
	
	public function add($data){	
		$this->db->insert('ci_episode', $data);
		return true;
	}
    
    public function get_all_seasons(){
        $query = $this->db->get('ci_seasons');
        return $result = $query->result_array();
    }
	

	public function get_all_series(){
		$query = $this->db->get('ci_tv_series');
		return $result = $query->result_array();
	}
	
	public function get_all_episodes(){
		$this->db->select('ci_episode.*,ci_tv_series.series_name');
		$this->db->from('ci_episode');
		$this->db->join('ci_tv_series','ci_tv_series.id = ci_episode.season_id',"LEFT");
		$this->db->order_by("ci_episode.name", "desc");
		$query = $this->db->get();
		$last = $this->db->last_query();
		return $result = $query->result_array();
	}
	
	public function get_seasons_by_id($id){
		$this->db->select("*");
		$this->db->from('ci_tv_series');
		$this->db->where('ci_tv_series.id',$id);
		$query = $this->db->get();
		$last = $this->db->last_query();
		return $result = $query->result_array();
		
	}
	
	public function get_episodes_by_id($id){
		$this->db->select('*');
		$this->db->from('ci_episode');
		$this->db->where('ci_episode.id', $id);
		$query = $this->db->get();
		return $result = $query->result_array();
	}
	
	public function edit_episode($data, $id){
		$this->db->select('*');
		$this->db->from('ci_episode');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->row_array();
		$image = $result['episode_video'];
		if($result>0){
			unlink('uploads/videos/'.$image);
			$this->db->where('id', $id);
			$this->db->update('ci_episode', $data);
			return true;
		}else{
			$this->db->where('id', $id);
			$this->db->update('ci_episode', $data);
			return true;
		}
	}
    
    public function get_series_id_by_season($id) {
        
        $this->db->where('id', $id);
        $query = $this->db->get('ci_seasons');
        return $series_id = $query->row_array()['series_id'];
    }
    
    public function get_all_seasons_in_series($id) {
        
        $this->db->where('series_id', $id);
        $query = $this->db->get('ci_seasons');
        return $result = $query->result_array();
    }
}



?>