 <!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h4><i class="fa fa-list"></i> &nbsp; Video List</h4>
        </div>
		<?php foreach($all_videos as $videos){
			$id = $videos['id'];
		} ?>
		<div class="col-md-6 text-right">
			 <a href="<?= base_url('admin/users/'); ?>" class="btn btn-success">Back</a>
		</div>
        <?php /*<div class="col-md-6 text-right">
          <div class="btn-group margin-bottom-20" style="display:none;">
            <a href="<?= base_url('admin/videos/create_users_pdf'); ?>" class="btn btn-success">Export as PDF</a>
            <a href="<?= base_url('admin/videos/export_csv'); ?>" class="btn btn-success">Export as CSV</a>
          </div>
        </div> */?>

      </div>
    </div>
  </div>
   <div class="box border-top-solid">
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
			<th>Name</th>
			<th>Description</th>
			<th>Category</th>
			<th>Subcategory</th>
			<th>Uploaded Video</th>
			<th class="text-right">Option</th>
        </tr>
        </thead>
        <tbody>
          <?php foreach($all_videos as $row):
		  ?>
          <tr>
            <td><?= $row['video_title']; ?></td>
            <td><?= $row['video_description']; ?></td>
            <td><?= $row['category_name']; ?></td>
            <td><?= $row['subcategory_name']; ?></td>
            <td>
				<a href="<?php echo $row['video_upload'] ?>" target="_blank"><?= $row['video_upload']; ?></a>
            </td>
            <td>
              <a href="<?= base_url('admin/videos/edit/'.$row['id']); ?>" class="btn btn-info btn-flat btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>
              <a data-href="<?= base_url('admin/videos/del/'.$row['id']); ?>" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
            </td>
		  </tr>
          <?php endforeach; ?>
        </tbody>

      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>


<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>As you sure you want to delete.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
  <script type="text/javascript">
      $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
  </script>

<script>
$("#view_users").addClass('active');
</script>
