 <!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">

 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h4><i class="fa fa-list"></i> &nbsp; Tv Series</h4>
        </div>
		<?php $admin = $this->session->userdata('name');
		if($admin == "Admin"){?>
        
          <div class="col-md-6 text-right">
            <div class="btn-group margin-bottom-20" >
              <a href="<?= base_url('admin/tvshows_videos/add'); ?>" class="btn btn-success">Add a New Series</a>
              
            </div>
          </div>	
				
		<?php }
		?>
        <?php /*<div class="col-md-6 text-right">
          <div class="btn-group margin-bottom-20" style="display:none;">
            <a href="<?= base_url('admin/videos/create_users_pdf'); ?>" class="btn btn-success">Export as PDF</a>
            <a href="<?= base_url('admin/videos/export_csv'); ?>" class="btn btn-success">Export as CSV</a>
          </div>
        </div> */?>

      </div>
    </div>
  </div>
   <div class="box border-top-solid">
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
			<th>Series Name</th>
            <th>Cover Image</th>
			<th>Image <br>Appearance</th>
			<th>No.Seasons</th>
			<th>No.Episodes</th>
			<th>Hashtags</th>
			<th>Favourites</th>				
			<th class="text-center">Option</th>
        </tr>
        </thead>
        <tbody>
		  <?php  
		  ?>	
          <?php foreach($all_seasons as $row):
		  ?>
          <tr>
            <td><a href="<?php echo base_url('admin/seasons/seasons_by_series_id/'.$row['id']); ?>"><?= $row['series_name']?></a></td>
            <td><img src="<?= $row['cover_image']; ?>" width="150px" height="150px"></td>
            <td><?= $row['image_appearance']; ?></td>
            <td><?= $row['season_id']; ?></td>
            <td><?= $row['episode_id']; ?></td>
			<td><?= $row['hashtags']; ?></td>	
			<td class="text-center">
			<?php $admin = $this->session->userdata('name');
				if($admin == 'Admin'){
					$this->db->select('*');
					$this->db->from('ci_favourite');
					$this->db->where('ci_favourite.series_id', $row['id']);
					$query = $this->db->get();
					$result = $query->result_array();
					$i = 0;
					foreach($result as $data){
						//echo "<pre>"; print_r($data); echo "</pre>";
						if($data['mark_fav'] == "yes"){
							$i++;
						}
					}
					echo $i;
				}else{?>
					<form method="post" action="">
						<input type="hidden" name="mark_fav" id="mark_fav" value="">
						<input type="hidden" name="user_id" id="user_id" value="<?php $id = $this->session->userdata('admin_id');  echo $id;?>">
						<?php 
						$this->db->select('*');
						$this->db->from('ci_favourite');
						$this->db->where('ci_favourite.series_id', $row['id']);
						$query = $this->db->get();
						$result = $query->result_array();
						$j = array();
						foreach($result as $data){
							if($data['user_id'] == $id){
								$j[]= $data['mark_fav'];
							}
						}
						if(in_array('yes', $j)){?>
							<a id="favourite" onClick="sendajax('<?= $row['id']; ?>')"><i class="fa fa-star icon-to-change" aria-hidden="true" style="font-size:20px; color:red;"></i></a>
						<?php }else{?>
							<a id="favourite" onClick="sendajax('<?= $row['id']; ?>')"><i class="fa fa-star-o icon-to-change" aria-hidden="true" style="font-size:20px; color:red;"></i></a>
						<?php } ?>
					</form>
				<?php }?>
			</td>	
			<td class="text-center">
			  <?php /*<a href="<?= base_url('admin/tvshows_videos/all_episodes/'.$row['season_id']); ?>" class="btn btn-info btn-flat btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Episodes</a>*/?>
			  <a href="<?= base_url('admin/seasons/seasons_by_series_id/'.$row['id']); ?>" class="btn btn-warning btn-flat btn-xs">View Seasons</a>
			  <!--<a href="<?= base_url('admin/notifications/add_by_season/'.$row['id']); ?>" class="btn btn-success btn-flat btn-xs">Notify</a>-->
			<?php $admin = $this->session->userdata('name');
			if($admin == 'Admin'){?>
			  <a href="<?= base_url('admin/tvshows_videos/edit/'.$row['id']); ?>" class="btn btn-info btn-flat btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			  <a data-href="<?= base_url('admin/tvshows_videos/del/'.$row['id']); ?>" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
			<?php } ?>
			</td>
		  </tr>
          <?php endforeach; ?>
        </tbody>

      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>

<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>As you sure you want to delete.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Mark favourite</h4>
      </div>
      <div class="modal-body">
        <p>You have marked this web series as favourite.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Removed from favourite</h4>
      </div>
      <div class="modal-body">
        <p>Sucessfully removed from favourites.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
  <script type="text/javascript">
      $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
  </script>

<script>
//$("#view_users").addClass('active');

$(function () {
	$('.icon-to-change').on('click',function(){
		$(this).toggleClass('fa-star fa-star-o');
	});
});

$(function(){
	$('#favourite i.icon-to-change').on('click',function(){
		if ($(this).hasClass("fa-star")) {
			$('#mark_fav').val('yes');
		}else if($(this).hasClass("fa-star-o")){
			$('#mark_fav').val('no');
		}else{}
	});
});
</script>

<script>
function sendajax(id){ 
	var mark_fav = $("input#mark_fav").val();
	var user_id = $("input#user_id").val();
	var id1 = id;
	var title = "Test";
	var message = "A new episode was recently added. Don't miss it.";
	jQuery.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>" + "admin/tvshows_videos/mark_as_favourite",
		dataType: 'json',
		data: {mark_fav: mark_fav, user_id: user_id, id1: id1, title: title, message: message},
		success: function(res) {
			if (res.markfav == 'yes')
			{
				$('#myModal').modal({
					'show': true
				});
			}else if(res.makfav == 'no')
			{
				$('#myModal1').modal({
					'show': true
				});
			}else{
			}
		}
	});
}
</script>


