<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Presaved_notifications extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/user_model', 'user_model');
			$this->load->model('admin/notification_model', 'notification_model');
			$this->load->model('admin/tvshow_video_model', 'tvshow_video_model');
			$this->load->model('admin/presaved_notification_model', 'presaved_notification_model');
		}

		public function index(){
      //die("sdas");
			$data['all_notifications'] =  $this->presaved_notification_model->get_all_notifications();
			$data['all_seasons'] =  $this->tvshow_video_model->get_all_seasons();
			$data['title'] = 'Notifications List';
			$data['view'] = 'admin/presaved_notifications/presaved_notification_list';
			$this->load->view('admin/layout', $data);
		}

		//---------------------------------------------------------------
		//  Add User
		public function add(){
			$data['all_users'] =  $this->user_model->get_all_users();
			if($this->input->post('submit')){

				$this->form_validation->set_rules('title', 'title', 'trim|min_length[5]|required');
				$this->form_validation->set_rules('description', 'description', 'trim|required');


				if ($this->form_validation->run() == FALSE) {
					$data['title'] = 'Add Notification';
					$data['view'] = 'admin/presaved_notifications/presaved_notification_add';
					$this->load->view('admin/layout', $data);
				}
				else{
					$data = array(
						'title' => $this->input->post('title'),
						'description' => $this->input->post('description'),
						'color' => $this->input->post('color'),
						'mobile_sound' => $this->input->post('mobile_sound'),	
						'created_at' => date('Y-m-d : h:m:s'),
						'updated_at' => date('Y-m-d : h:m:s'),
					);

					$data = $this->security->xss_clean($data);
					//$userData = $this->user_model->get_user_by_id($this->input->post('to_user_id'));
					$result = $this->presaved_notification_model->add_notification($data);
					if($result){
						$this->session->set_flashdata('msg', 'Notification has been Added Successfully!');
						redirect(base_url('admin/presaved_notifications'));
					}
				}
			}
			else
			{
				$data['title'] = 'Add Notification';
				$data['view'] = 'admin/presaved_notifications/presaved_notification_add';
				$this->load->view('admin/layout', $data);
			}

		}

		//---------------------------------------------------------------
		//  Edit Notification
		public function edit($id = 0){
			$data['all_users'] =  $this->user_model->get_all_users();
			if($this->input->post('submit')){
				$this->form_validation->set_rules('title', 'title', 'trim|min_length[5]|required');
				$this->form_validation->set_rules('description', 'description', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['notification'] = $this->notification_model->get_notification_by_id($id);
					//$data['notification_groups'] = $this->notification_model->get_notification_groups();
					$data['title'] = 'Edit Notification';
					$data['view'] = 'admin/presaved_notifications/presaved_notification_edit';
					$this->load->view('admin/layout', $data);
				}
				else{
					$data = array(
						'title' => $this->input->post('title'),
						'description' => $this->input->post('description'),
						'color' => $this->input->post('color'),
						'mobile_sound' => $this->input->post('mobile_sound'),
						'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					//$userData = $this->user_model->get_user_by_id($this->input->post('to_user_id'));
					$result = $this->presaved_notification_model->edit_notification($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'Notification has been updated successfully!');
						redirect(base_url('admin/presaved_notifications'));
					}
				}
			}
			else{
				$data['notification'] = $this->presaved_notification_model->get_notification_by_id($id);
				//$data['notification_groups'] = $this->notification_model->get_notification_groups();
				$data['title'] = 'Edit Notification';
				$data['view'] = 'admin/presaved_notifications/presaved_notification_edit';
				$this->load->view('admin/layout', $data);
			}
		}

		//---------------------------------------------------------------
		//  Delete Notifications
		public function del($id = 0){
			$this->db->delete('ci_pre-saved_notifications', array('id' => $id));
			$this->session->set_flashdata('msg', 'Notification has been Deleted Successfully!');
			redirect(base_url('admin/presaved_notifications'));
		}
		// Export data in CSV format
		public function export_csv(){
		   // file name
		   $filename = 'notifications_'.date('Y-m-d').'.csv';
		   header("Content-Description: File Transfer");
		   header("Content-Disposition: attachment; filename=$filename");
		   header("Content-Type: application/csv; ");

		   // get data
		   $notification_data = $this->notification_model->get_all_notifications_for_csv();

		   // file creation
		   $file = fopen('php://output', 'w');

		   $header = array("ID", "Username", "First Name", "Last Name", "Email", "Mobile_no", "Created Date");
		   fputcsv($file, $header);
		   foreach ($notification_data as $key=>$line){
		     fputcsv($file,$line);
		   }
		   fclose($file);
		   exit;
		  }
}
?>
