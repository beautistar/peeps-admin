<?php
	class Notification_model extends CI_Model{

		public function add_notification($data){
			$this->db->insert('ci_notifications', $data);
			return true;
		}
		
		public function get_all_notifications(){
			$this->db->where('is_admin', 0);
			$query = $this->db->get('ci_notifications');
			return $result = $query->result_array();
		}
		public function get_all_notifications_list(){
			//$this->db->where('is_admin', 0);
			$this->db->select ('ci_notifications.*,ci_users.username, ci_users.firstname, ci_users.lastname, ci_user_category.user_category_name');
			$this->db->from('ci_notifications');
			$this->db->join('ci_users', 'ci_users.id = ci_notifications.to_user_id',"LEFT");
			$this->db->join('ci_user_category', 'ci_user_category.id = ci_users.role',"LEFT");
			$query = $this->db->get();
			//$result = $query->result_array();
			return $result = $query->result_array();
		}
		public function get_all_notifications_for_csv(){
			$this->db->where('is_admin', 0);
			$this->db->select('id, username, firstname, lastname, email, mobile_no, created_at');
			$this->db->from('ci_notifications');
			$query = $this->db->get();
			return $result = $query->result_array();
		}


		public function get_notification_by_id($id){
			$query = $this->db->get_where('ci_notifications', array('id' => $id));
			return $result = $query->row_array();
		}

		public function edit_notification($data, $id){
			$this->db->where('id', $id);
			$this->db->update('ci_notifications', $data);
			return true;
		}

		/*public function get_notification_groups(){
			$query = $this->db->get('ci_notification_groups');
			return $result = $query->result_array();
		}*/
		
		public function get_all_user_category(){
			$this->db->select('*');
			$this->db->from('ci_users');
			$this->db->join('ci_user_category', 'ci_user_category.id = ci_users.role');
			$query = $this->db->get();
			return $result = $query->result_array();
		}
		
		public function get_all_seasons($id){
			$this->db->select('ci_favourite.*,ci_tv_series.id, ci_users.id');
			$this->db->from('ci_favourite');
			$this->db->join('ci_tv_series', 'ci_tv_series.id = ci_favourite.series_id');
			$this->db->join('ci_users', 'ci_users.id = ci_favourite.user_id');
			$this->db->where('ci_favourite.series_id', $id);
			$query = $this->db->get();
			return $result = $query->row_array();
			//echo "<pre>"; print_r($result); echo "</pre>";
		}
		public function get_favourite_data($id){
			$this->db->select('ci_favourite.*,ci_tv_series.id, ci_users.id, ci_users.token');
			$this->db->from('ci_favourite');
			$this->db->join('ci_tv_series', 'ci_tv_series.id = ci_favourite.series_id');
			$this->db->join('ci_users', 'ci_users.id = ci_favourite.user_id');
			$this->db->where('ci_favourite.series_id', $id);
			$query = $this->db->get();
			return $result = $query->result_array();
		}
	}
?>
