<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-5">
          <h4><i class="fa fa-pencil"></i> &nbsp; Edit Season</h4>
        </div>
		<div class="col-md-3 text-right">
			<a href="<?= base_url('admin/tvshows_videos'); ?>" class="btn btn-success"> Back</a>
		</div>
        <div class="col-md-4 text-left">
          <a href="<?= base_url('admin/tvshows_videos'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Series List</a>
        </div>
        
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open_multipart(base_url('admin/tvshows_videos/edit/'.$all_seasons['id']), 'class="form-horizontal"' )?> 
				<div class="form-group">
					<label for="series_name" class="col-sm-2 control-label">Series Name</label>
					<div class="col-sm-9">
					  <input type="text" name="series_name" value="<?= $all_seasons['series_name']; ?>" class="form-control" id="series_name" placeholder="">
					</div>
				</div>
				<div class="form-group">
					<label for="video_title" class="col-sm-2 control-label">Cover Image</label>
					<div class="col-sm-3">
						<img src="<?php echo $all_seasons['cover_image'] ?>" width="100px" height="100px">
					</div>
					<div class="col-sm-6">
					  <input type="hidden" name="cover_image_old" id="cover_image_old" value="<?php echo $all_seasons['cover_image']; ?>"> 	
					  <input type="file" name="cover_image" class="form-control" id="cover_image">
					</div>
				</div>
				<div class="form-group">
					<label for="image_appearance" class="col-sm-2 control-label">Image Size Appearance</label>
					<div class="col-sm-9">
						<select name="image_appearance" class="form-control">
							<option value="">Select Image Size</option>
							<option value="small" <?php echo ($all_seasons['image_appearance'] == 'small')?"selected":"" ?>>Small</option>
							<option value="medium" <?php echo ($all_seasons['image_appearance'] == 'medium')?"selected":"" ?>>Medium</option>
							<option value="large" <?php echo ($all_seasons['image_appearance'] == 'large')?"selected":"" ?>>Large</option>
						</select>
					</div>
				</div>	
				<div class="form-group">
					<label for="season_description" class="col-sm-2 control-label">Description</label>
					<div class="col-sm-9">
					  <textarea type="text" id="season_description" name="season_description" class="form-control" rows="3" placeholder=""><?php echo $all_seasons['description']; ?></textarea>
					</div>
				</div> 
				<div class="form-group">
					<label for="season_id" class="col-sm-2 control-label">Season no.</label>
					<div class="col-sm-9">
						<input type="number" class="form-control" name="season_id"  id="season_id" value="<?php echo $all_seasons['season_id']; ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="episode_id" class="col-sm-2 control-label">No. of Episodes</label>
					<div class="col-sm-9">
						<input type="number" class="form-control" name="episode_id"  id="episode_id" value="<?php echo $all_seasons['episode_id']; ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="hashtags" class="col-sm-2 control-label">Hashtags</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="hashtags"  id="hashtags" value="<?php echo $all_seasons['hashtags']; ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="category_id" class="col-sm-2 control-label">Select Category</label>
					<div class="col-sm-9">
						<select class="form-control" id="category-list" name="category_id">
							<option value="">Select Category</option>
							<?php foreach($category as $cat):
								if($cat['belongs_to'] == 'tvshows'){
									if($cat['id'] == $all_seasons['category_id']){ ?>
										<option value="<?= $all_seasons['category_id']; ?>" selected><?= $all_seasons['category_name']; ?></option>
									<?php }else{ ?>
									<option value="<?= $cat['id']; ?>"><?= $cat['category_name']; ?></option>
								<?php } 
								}
							endforeach; ?>
						</select>
					</div>
				</div>
				<div class="form-group" id="subcategory_id_div" >
					<label for="title" class="col-sm-2 control-label">Select Subcategory</label>
					<div class="col-sm-9">
						<select name="subcategory_id" id="subcategory_id" class="form-control">
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-11">
					  <input type="submit" name="submit" value="Update" class="btn btn-info pull-right">
					</div>
				</div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  
</section> 
<script>
/* JQuery to bind Subcategory according to Category selection */
$(document).ready(function() {
  //$('#subcategory_id_div').hide();
  $('select[name="subcategory_id"]').append('<option value="<?php echo $all_seasons['subcategory_id']; ?>"><?php echo $all_seasons['subcategory_name'];?></option>');
	$('#category-list').on('change', function() {
		var category_id = $(this).val();
		if(category_id) {
			$.ajax({
				url:"<?php echo base_url(); ?>admin/videos/subcategory/"+category_id,
				type: "GET",
				dataType: "json",
				success:function(response)
        {
          //('#subcategory-list').empty();
          $('#subcategory_id_div').show();
          $('#subcategory_id').empty();
          $.each(response, function(index,value)
          {
            //alert(data.subcategory_name);
            $('#subcategory_id').append('<option value="'+ value.id +'">'+ value.subcategory_name +'</option>');
          });
				}
			});
		}else{
      //$('#subcategory_id_div').hide();
			$('#subcategory_id').empty();
		}
	});
});
</script>
<script>
$(document).ready(function(){
/* 	if ($('#file').attr("value") == "file") {
		//$('#filetype').val('File');
		$("#video_url").hide();
		$("#video_file").show();
	}else if($('#url').attr("value")== "url"){
		//$('#filetype').val('URL');
		$("#video_file").hide();
		$("#video_url").show();
	} */
	$('#url').on('click',function(){
		$('#video_file').hide();
		$('#video_url').show();
	});
	$('#file').on('click',function(){
		$('#video_url').hide();
		$('#video_file').show();
	});
});
</script>
