<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body with-border">
        <div class="col-md-6">
          <h4><i class="fa fa-plus"></i> &nbsp; Add New Category</h4>
        </div>
        <div class="col-md-6 text-right">
          <a href="<?= base_url('admin/category'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Category List</a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
			<?php if(isset($msg) || validation_errors() !== ''): ?>
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-warning"></i> Alert!</h4>
					<?= validation_errors();?>
					<?= isset($msg)? $msg: ''; ?>
				</div>
            <?php endif; ?>
            <?php echo form_open_multipart(base_url('admin/category/add'), 'class="form-horizontal"');  ?> 
            <?php //echo form_open_multipart(base_url('admin/video/video_upload'), 'class="form-horizontal"');  ?> 
				<div class="form-group">
					<div class="col-sm-3">
						<label for="category_name" control-label">Category Name</label>
					</div>	
					<div class="col-sm-9">
						<input type="text" name="category_name" class="form-control" id="category_name" placeholder="">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-3">
						<label for="belongs_to" class="control-label">Belongs To</label>
					</div>
					<div class="col-sm-9">
						<input type="radio" name="belongs_to" value="movies" checked> Movies &nbsp;
						<input type="radio" name="belongs_to" value="tvshows"> TV Shows &nbsp;
					</div>
				</div>
				<?php /*<div class="form-group">
					<div class="col-sm-3">
						<label for="category_access" class="control-label">User Category Access</label>
					</div>
					<div class="col-sm-9">
						<select id="" name="category_access[]" class="form-control" multiple> 
							<option class="select">Select User Category</option>
							<?php foreach($user_category as $role): ?>
							<option value="<?= $role['id']; ?>"><?= $role['user_category_name']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div> */?>
				<div class="form-group">
					<label for="display_order" class="col-sm-3 control-label">Select the order</label>
					<div class="col-sm-9">
						<select class="form-control" id="category-list" name="display_order">
						<option value="" selected>Select Order</option>
						<?php 
							$count = 1;
							foreach($all_category as $category){?>
							<option value="<?php echo $count; ?>"><?php echo $count; ?></option>
						<?php
							$count++;
						} ?>
							<option value="<?php echo $count; ?>"><?php echo $count; ?></option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
					  <input type="submit" name="submit" value="Add category" class="btn btn-info pull-right">
					</div>
				</div>
            <?php echo form_close( ); ?>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>  
</section> 
<script>
/*$('select[name="category_access"]').on("click", function(){      
  if ($(this).find(":selected").text() == "All"){
    if ($(this).attr("data-select") == "false"){
		$(this).find("option").prop("selected", true);
		$(this).find('.select').prop("selected", false);
		$('.all').val('0');
	}else{
		$(this).attr("data-select", "false").find("option").prop("selected", false);
	}
  }
});*/
</script>