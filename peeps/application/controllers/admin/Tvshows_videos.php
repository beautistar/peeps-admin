<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Tvshows_videos extends MY_Controller{

	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('admin/user_model', 'user_model');
		$this->load->model('admin/video_model', 'video_model');
		$this->load->model('admin/video_listing_model', 'video_listing_model');
		$this->load->model('admin/subcategory_model', 'subcategory_model');
		$this->load->model('admin/category_model', 'category_model');
		$this->load->model('admin/tvshow_video_model', 'tvshow_video_model');
		$this->load->model('admin/episode_model', 'episode_model');
		$this->load->model('admin/presaved_notification_model', 'presaved_notification_model');
	}

	public function index()
	{
		$data['all_users'] = $this->tvshow_video_model->all_users();	
		$data['all_seasons'] =  $this->tvshow_video_model->get_all_seasons();
		$data['title'] = 'Video List';
		$data['view'] = 'admin/tvshows_videos/tvshows_video_list';
		$this->load->view('admin/layout', $data);
	}
	
	public function add(){
		//$data['all_seasons'] =  $this->episode_model->get_all_seasons();
		if($this->input->post('submit'))
		{	
		/* 	$new_name = time();
			$file_ext = pathinfo($_FILES["image_appearance"]['name'],PATHINFO_EXTENSION);
			echo $image = $new_name.".".$file_ext;
			$file_ext2 = pathinfo($_FILES["cover_image"]['name'],PATHINFO_EXTENSION);
			$cover_image = $new_name.".".$file_ext2; */
			$this->form_validation->set_rules('series_name', 'Series Name', 'trim|required');
			$this->form_validation->set_rules('season_description', 'Description', 'trim|required');
			$this->form_validation->set_rules('category_id', 'Category', 'trim|required');
			$this->form_validation->set_rules('image_appearance', 'Select image size', 'trim|required');
			$this->form_validation->set_rules('cover_image', '', 'callback_file_check2');
			if ($this->form_validation->run() == FALSE)
			{
				$data['title'] = 'Add Video';
				$data['category'] = $this->video_model->get_all_category();
				$data['view'] = 'admin/tvshows_videos/tvshows_video_add';
				$this->load->view('admin/layout', $data);
			}else
			{
				$config1 = array
					(
						'upload_path'   => "./uploads/images/",
						'allowed_types' => 'jpg|jpeg|bmp|png',
						'overwrite' => TRUE,
						'max_size' => "30720"
					);
					$this->load->library('upload', $config1);
					$this->upload->do_upload('cover_image'); 
					$cover_upload = $this->upload->data(); 
					
					$cover_image = base_url().'/uploads/images/'.$_FILES["cover_image"]['name'];
					$data = array(
						'series_name' => $this->input->post('series_name'),
						'cover_image' => $cover_image,
						'image_appearance' => $this->input->post('image_appearance'),
						'description' => $this->input->post('season_description'),
						'season_id'  => $this->input->post('season_id'),
						'episode_id' => $this->input->post('episode_id'),
						'hashtags'  => $this->input->post('hashtags'),
						'category_id' => $this->input->post('category_id'),
						'subcategory_id' => $this->input->post('subcategory_id'),
						//'display_order' => $this->input->post('display_order'),
 						'created_at' => date('Y-m-d h:m:s'),
						'updated_at' => date('Y-m-d h:m:s')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->tvshow_video_model->add_season($data);
					if($result)
					{
						$this->session->set_flashdata('msg', 'Season has been added sucessfully!');
						redirect(base_url('admin/tvshows_videos'));
					} 	
					
			}
		}
		else
		{
			//$data['playlistData']=$this->video_listing_model->get_listing_by_id($id);
			$data['category'] = $this->video_model->get_all_category();
			$data['title'] = 'Add Videos';
			$data['view'] = 'admin/tvshows_videos/tvshows_video_add';
			$this->load->view('admin/layout', $data);
		}
	}	


	//Edit
	public function edit($id = 0){
		if($this->input->post('submit')){
			$this->form_validation->set_rules('series_name', 'Series Name', 'trim|required');
			$this->form_validation->set_rules('season_description', 'Description', 'trim|required');
			$this->form_validation->set_rules('category_id', 'Category', 'trim|required');
			if(!empty($_FILES['cover_image']['name'])){
				$this->form_validation->set_rules('cover_image', '', 'callback_file_check2');
			}
			if ($this->form_validation->run() == FALSE) {
				$data['title'] = 'Add Video';
				$data['all_seasons'] = $this->tvshow_video_model->get_all_seasons_id($id);
				$data['category'] = $this->video_model->get_all_category();
				$data['view'] = 'admin/tvshows_videos';
				$this->load->view('admin/layout', $data);
			}else{
				if(!empty($_FILES['cover_image']['name'])){
					$config1 = array
					(
						'upload_path'   => "./uploads/images/",
						'allowed_types' => 'jpg|jpeg|bmp|png',
						'overwrite' => TRUE,
						'max_size' => "30720"
					);
					$this->load->library('upload', $config1);
					$this->upload->do_upload('cover_image'); 
					$cover_upload = $this->upload->data(); 
					
					$cover_image = base_url().'/uploads/images/'.$_FILES["cover_image"]['name'];
					$data = array(
						'series_name' => $this->input->post('series_name'),
						'cover_image' => $cover_image,
						'image_appearance' => $this->input->post('image_appearance'),
						'description' => $this->input->post('season_description'),
						'season_id'  => $this->input->post('season_id'),
						'episode_id' => $this->input->post('episode_id'),
						'hashtags'  => $this->input->post('hashtags'),
						'category_id' => $this->input->post('category_id'),
						'subcategory_id' => $this->input->post('subcategory_id'),
						'updated_at' => date('Y-m-d h:m:s')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->tvshow_video_model->edit_season($data, $id);
					if($result)
					{
						$this->session->set_flashdata('msg', 'Season has been updated sucessfully!');
						redirect(base_url('admin/tvshows_videos'));
					} 	
				}else{
					$cover_image = $this->input->post('cover_image_old');
					$data = array(
						'series_name' => $this->input->post('series_name'),
						'cover_image' => $cover_image,
						'image_appearance' => $this->input->post('image_appearance'),
						'description' => $this->input->post('season_description'),
						'season_id'  => $this->input->post('season_id'),
						'episode_id' => $this->input->post('episode_id'),
						'hashtags'  => $this->input->post('hashtags'),
						'category_id' => $this->input->post('category_id'),
						'subcategory_id' => $this->input->post('subcategory_id'),
						'updated_at' => date('Y-m-d h:m:s')
					);

					$data = $this->security->xss_clean($data);
					$result = $this->tvshow_video_model->edit_season($data, $id);
					if($result)
					{
						$this->session->set_flashdata('msg', 'Season has been updated sucessfully!');
						redirect(base_url('admin/tvshows_videos'));
					} 
				}
			}
		}
		else{
			$data['category'] = $this->video_model->get_all_category();
			$data['all_seasons'] = $this->tvshow_video_model->get_all_seasons_id($id);
			$data['title'] = 'Add Videos';
			$data['view'] = 'admin/tvshows_videos/tvshows_video_edit';
			$this->load->view('admin/layout', $data);
		}
	}

	//  Delete Video
	public function del($id = 0){
		$this->db->select('*');
		$this->db->from('ci_tv_series');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->row_array();
		$coverimage = $result['cover_image'];
		$image = $result['image_appearance'];
		$this->db->delete('ci_tv_series', array('id' => $id));
		unlink('uploads/images/'.$coverimage);
		unlink('uploads/images/'.$image);
        $this->db->delete('ci_seasons', array('series_id' => $id));
		$this->session->set_flashdata('msg', 'Season has been Deleted Successfully!');
		redirect(base_url('admin/tvshows_videos'));
	}


	public function subcategory($id) {
	 //die($id);
		$this->db->select('id,subcategory_name');
		$this->db->where('category_id', $id);
		$query = $this->db->get('ci_subcategory');
		$result = $query->result_array();
		echo json_encode($result);
   }

	//File type validation
	public function file_check($str){
		$allowed_mime_type_arr = array('image/jpg','image/jpeg','image/png','image/pjpeg','image/bmp');
		//$file_size_max = 100000;
        $mime = get_mime_by_extension($_FILES['image_appearance']['name']);
        if(isset($_FILES['image_appearance']['name']) && $_FILES['image_appearance']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            } else{
                $this->form_validation->set_message('file_check', 'Please select only jpg,png and jpeg formats.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    }
	
		//File type validation
	public function file_check2($str){
		$allowed_mime_type_arr = array('image/jpg','image/jpeg','image/png','image/pjpeg','image/bmp');
		//$file_size_max = 100000;
        $mime = get_mime_by_extension($_FILES['cover_image']['name']);
        if(isset($_FILES['cover_image']['name']) && $_FILES['cover_image']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            } else{
                $this->form_validation->set_message('file_check', 'Please select only jpg,png and jpeg formats.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    }


	public function all_episodes(){
		$data['all_episodes'] =  $this->episode_model->all_episode_by_id();
		$data['title'] = 'Video List';
		$data['view'] = 'admin/episodes/episode_list_byid';
		$this->load->view('admin/layout', $data);
	}
	
	public function mark_as_favourite(){
		if(isset($_POST['mark_fav'])){
			$json = array(
				'mark_fav' => $this->input->post('mark_fav'),
				'user_id '=>$this->input->post('user_id'),
				'id1' => $this->input->post('id1'),
				'title' => $this->input->post('title'),
				'message' => $this->input->post('message'),
				'old_season_id' => $this->input->post('old_season_id'),
				'old_user_id' => $this->input->post('old_user_id')
			);
			$data1 = json_encode($json);
			$id = $json['id1'];
			$userid = $json['old_user_id'];
			$seasonid = $json['old_season_id'];
			$data = array(
				'mark_fav' =>$this->input->post('mark_fav'),
				'user_id' => $this->input->post('user_id'),
				'series_id' => $id,
				'date' => date('Y-m-d h:m:s')
			);
			if($data['mark_fav'] == "yes"){
				$return = $this->db->insert('ci_favourite', $data);
			}else if($data['mark_fav'] == "no"){
				$array = array('user_id' => $data['user_id'], 'series_id' => $data['series_id']);
				$this->db->where($array);
				$return = $this->db->update('ci_favourite', $data);
			}else{
				
			}
			$arr = array('msg' => true,
			'markfav' => $data['mark_fav'],
			'userid' => $data['user_id'],
			'id' => $id
			);
			echo json_encode($arr);
		}
	}
	
	public function episode_by_series($id){
		$data['all_seasons_id'] =  $this->tvshow_video_model->get_all_seasons_by_id($id);
		$data['title'] = 'Episodes List';
		$data['view'] = 'admin/episodes/episode_list_byseason';
		$this->load->view('admin/layout', $data);
	}
}

?>
