/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 5.7.26-0ubuntu0.16.04.1 : Database - peeps
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`peeps` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `peeps`;

/*Table structure for table `ci_category` */

DROP TABLE IF EXISTS `ci_category`;

CREATE TABLE `ci_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(200) NOT NULL,
  `subcatid` int(11) NOT NULL,
  `is_enabled` int(2) NOT NULL DEFAULT '0',
  `discount` int(10) NOT NULL,
  `belongs_to` varchar(200) NOT NULL,
  `user_category_id` int(10) NOT NULL,
  `display_order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_category_id` (`user_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `ci_category` */

insert  into `ci_category`(`id`,`category_name`,`subcatid`,`is_enabled`,`discount`,`belongs_to`,`user_category_id`,`display_order`) values (1,'testing',0,0,0,'tvshows',11,3),(2,'Sports',0,0,0,'tvshows',13,1),(3,'testcat',0,0,0,'movies',11,2),(4,'weweewqea',0,0,0,'tvshows',13,2),(5,'Fashion',0,0,0,'movies',11,5),(7,'Women life',0,0,0,'tvshows',11,1),(9,'Trending Now',0,0,0,'movies',0,1);

/*Table structure for table `ci_episode` */

DROP TABLE IF EXISTS `ci_episode`;

CREATE TABLE `ci_episode` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `hashtags` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `episode_video` text COLLATE utf8_unicode_ci NOT NULL,
  `episode_number` int(50) NOT NULL,
  `season_id` int(50) NOT NULL,
  `duration` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `series_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `series_id` (`series_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_episode` */

insert  into `ci_episode`(`id`,`name`,`description`,`hashtags`,`episode_video`,`episode_number`,`season_id`,`duration`,`series_id`,`created_at`,`updated_at`) values (2,'episode1','dfgfgbfbgfgghgghh','#dsffsf #lkjlkj','http://13.127.134.33/uploads/videos/1556529576.mp4',1,6,'22222',1,'2019-04-19','2019-04-29'),(5,'Test1','test1','#fighting','http://13.127.134.33/uploads/videos/1556095708.mp4',2,6,'1000000',6,'2019-04-23','2019-04-24'),(9,'episode1','wdeewee','#fgkfhhlkh #lkhlfhflh #glhklhl #hkjglkj','http://13.127.134.33/uploads/videos/1556097474.mp4',1,6,'1:02',6,'2019-04-24','2019-04-24'),(10,'Test episode-1','Test episode-1','#test','http://192.168.0.33/peeps/uploads/videos/1556103921.mp4',2,4,'1000000',6,'2019-04-24','2019-04-24'),(11,'Test episode-2','Test episode-2','#test','http://192.168.0.33/peeps/uploads/videos/1556104787.mp4',2,7,'1000000',4,'2019-04-24','2019-04-24'),(12,'Test episode-2','Test episode-2','#test','http://192.168.0.33/peeps/uploads/videos/1556104824.mp4',2,4,'1000000',4,'2019-04-24','2019-04-24'),(13,'Test episode-2','Test episode-2','#test','http://192.168.0.33/peeps/uploads/videos/1556107191.mp4',2,4,'1000000',4,'2019-04-24','2019-04-24'),(14,'Test episode-3','Test episode-3','#test','http://192.168.0.33/peeps/uploads/videos/1556110453.mp4',5,4,'10000',0,'2019-04-24','2019-04-24'),(15,'Test episode-4','Test episode-4','#test','http://192.168.0.33/peeps/uploads/videos/1556110580.mp4',5,4,'10000',0,'2019-04-24','2019-04-24'),(17,'Test episode-5','Test episode-5','#test','http://192.168.0.33/peeps/uploads/videos/1556110787.mp4',5,4,'10000',0,'2019-04-24','2019-04-24'),(18,'Test episode-5','Test episode-5','#test','http://192.168.0.33/peeps/uploads/videos/1556110896.mp4',5,4,'10000',6,'2019-04-24','2019-04-24'),(19,'Test episode-51111','Test episode-51111','#test1111','http://192.168.0.33/peeps/uploads/videos/1556112450.mp4',5,4,'10000111',6,'2019-04-24','2019-04-24'),(20,'Test episode-5','Test episode-5','#test','http://192.168.0.33/peeps/uploads/videos/1556111051.mp4',5,4,'10000',6,'2019-04-24','2019-04-24'),(21,'Test episode-5','Test episode-5','#test','http://192.168.0.33/peeps/uploads/videos/1556111373.mp4',5,4,'10000',6,'2019-04-24','2019-04-24'),(22,'Test episode-5','Test episode-5','#test','http://192.168.0.33/peeps/uploads/videos/1556111495.mp4',5,4,'10000',6,'2019-04-24','2019-04-24'),(24,'Test episode-424-1','Test episode-424-1','#test','http://13.127.134.33/uploads/videos/1556113619.mp4',1,5,'10000',7,'2019-04-24','2019-04-24'),(25,'Test episode-424-2','Test episode-424-2','#test','http://13.127.134.33/uploads/videos/1556113672.mp4',2,5,'1000000',7,'2019-04-24','2019-04-24'),(26,'Test episode-424-4','Test episode-424-4','#test','http://13.127.134.33/uploads/videos/1556113828.mp4',3,7,'10000',7,'2019-04-24','2019-04-24'),(27,'Episode 429-1','This is test Episode 429-1','#test','http://13.127.134.33/uploads/videos/1556521326.mp4',1,8,'1000000',8,'2019-04-29','2019-04-29'),(28,'Episode 429-2','This is episode 429-2','#test','http://13.127.134.33/uploads/videos/1556521682.mp4',2,8,'10000',8,'2019-04-29','2019-04-29'),(29,'Episode 429-3','This is Episode 429-3','#test','http://13.127.134.33/uploads/videos/1556521716.mp4',3,8,'10000',8,'2019-04-29','2019-04-29'),(30,'Test episode-2-1','Test episode-2-1','#test','http://13.127.134.33/uploads/videos/1556529431.mp4',1,9,'1000000',8,'2019-04-29','2019-04-29'),(31,'Test episode-2-2','Test episode-2-2','#test','http://13.127.134.33/uploads/videos/1556529454.mp4',2,9,'10000',8,'2019-04-29','2019-04-29'),(32,'Test episode-3-1','Test episode-3-1','#test','http://13.127.134.33/uploads/videos/1556529491.mp4',1,10,'10000',8,'2019-04-29','2019-04-29'),(33,'Test episode-3-2','Test episode-3-2','#test1111','http://13.127.134.33/uploads/videos/1556529514.mp4',2,10,'10000',8,'2019-04-29','2019-04-29'),(34,'Test episode-1','Test episode-1-1','#test','http://13.127.134.33/uploads/videos/1556529625.mp4',1,1,'50000',6,'2019-04-29','2019-04-29'),(35,'Test episode-2-1','222','#test','http://13.127.134.33/uploads/videos/1556529647.mp4',2,1,'10000111',6,'2019-04-29','2019-04-29');

/*Table structure for table `ci_favourite` */

DROP TABLE IF EXISTS `ci_favourite`;

CREATE TABLE `ci_favourite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `series_id` int(20) NOT NULL,
  `movie_id` int(15) NOT NULL,
  `user_id` int(20) NOT NULL,
  `mark_fav` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_favourite` */

insert  into `ci_favourite`(`id`,`series_id`,`movie_id`,`user_id`,`mark_fav`,`date`) values (1,9,0,62,'yes','2019-05-04 05:05:38'),(2,8,0,62,'yes','2019-05-04 05:05:59'),(3,0,8,62,'yes','2019-05-04 05:05:49'),(4,0,6,62,'no','2019-05-04 05:05:00'),(5,0,3,62,'yes','2019-05-04 05:05:23'),(6,0,7,62,'yes','2019-05-04 05:05:28');

/*Table structure for table `ci_movies` */

DROP TABLE IF EXISTS `ci_movies`;

CREATE TABLE `ci_movies` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `hashtags` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cover_image` text COLLATE utf8_unicode_ci NOT NULL,
  `image_appearance` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `movie_video` text COLLATE utf8_unicode_ci NOT NULL,
  `duration` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_movies` */

insert  into `ci_movies`(`id`,`name`,`description`,`hashtags`,`cover_image`,`image_appearance`,`category_id`,`subcategory_id`,`movie_video`,`duration`,`created_at`,`updated_at`) values (3,'Test movieffg','Test movie','#test','http://13.127.134.33//uploads/images/006.jpg','large',3,3,'http://13.127.134.33/uploads/videos/1556167948.mp4','10000','2019-04-25','2019-05-01'),(4,'movie2','dfvdfvfvf','#erwrrr','http://13.127.134.33//uploads/images/download.jpg','small',3,2,'http://13.127.134.33/uploads/videos/1556528254.mp4','50000','2019-04-29','2019-04-30'),(5,'Test movie 425','This is test movie 425','#test','http://13.127.134.33//uploads/images/thumb-100-the-division-1.jpg','medium',5,9,'http://13.127.134.33/uploads/videos/1556183742.mp4','1000000','2019-04-25','2019-04-30'),(6,'Test movie 425-1','Test movie 425-1','#test','http://13.127.134.33//uploads/images/9.jpg','medium',5,0,'http://13.127.134.33/uploads/videos/1556191908.mp4','10000','2019-04-25','2019-05-01'),(7,'Movie-429-1','This is test movie created at April 29','#test','http://13.127.134.33//uploads/images/jQuery-Plugin-For-Pretty-Image-Slider-With-Thumbnails-desoSlide.jpg','medium',3,2,'http://13.127.134.33/uploads/videos/1556521907.mp4','10000','2019-04-29','2019-04-30'),(8,'testmovie','jhjhjkjkj','#dsffsf #lkjlkj','http://13.127.134.33//uploads/images/3.jpg','small',5,9,'http://13.127.134.33/uploads/videos/1556522151.mp4','10000','2019-04-29','2019-04-30');

/*Table structure for table `ci_notifications` */

DROP TABLE IF EXISTS `ci_notifications`;

CREATE TABLE `ci_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(30) NOT NULL,
  `color` varchar(50) NOT NULL,
  `device_sound` text NOT NULL,
  `season_id` int(15) NOT NULL,
  `to_user_id` varchar(30) NOT NULL,
  `is_sent_to_all` varchar(50) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `token` varchar(512) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ci_notifications` */

insert  into `ci_notifications`(`id`,`title`,`description`,`color`,`device_sound`,`season_id`,`to_user_id`,`is_sent_to_all`,`is_active`,`is_admin`,`last_ip`,`token`,`created_at`,`updated_at`) values (1,'test notification','ersefsefszfrfe','blue','',0,'59','',1,0,'','','2019-04-22 04:04:22','2019-04-22 04:04:22'),(2,'TEST','TEST TEST TEST','','',0,'61','',1,0,'','','2019-04-22 12:04:21','2019-04-22 12:04:55');

/*Table structure for table `ci_pre-saved_notifications` */

DROP TABLE IF EXISTS `ci_pre-saved_notifications`;

CREATE TABLE `ci_pre-saved_notifications` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_pre-saved_notifications` */

insert  into `ci_pre-saved_notifications`(`id`,`title`,`description`,`created_at`,`updated_at`) values (2,'rtedrtr','rtertdtr','2019-04-08','2019-04-08'),(5,'Test','A new episode was recently added. Don\'t miss it.','2019-04-18','2019-04-18');

/*Table structure for table `ci_seasons` */

DROP TABLE IF EXISTS `ci_seasons`;

CREATE TABLE `ci_seasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `series_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_seasons` */

insert  into `ci_seasons`(`id`,`series_id`,`name`,`description`,`created_at`,`updated_at`) values (1,6,'Test season 1','This is test season 1','2019-04-24 08:04:57','2019-04-24 09:04:41'),(4,6,'Test season 2','This is test season 2','2019-04-24 09:04:26','2019-04-24 09:04:26'),(5,7,'Test season 424-1','This is test season 424-1','2019-04-24 01:04:40','2019-04-24 01:04:40'),(6,7,'Test season 424-2','This is test season 424-2','2019-04-24 01:04:24','2019-04-24 01:04:24'),(7,7,'Test season 424-4','Test season 424-4','2019-04-24 01:04:19','2019-04-24 01:04:19'),(8,8,'Season - 429 - 1','This is test Season - 429 - 1','2019-04-29 07:04:27','2019-04-29 07:04:27'),(9,8,'Season - 429 - 2','This is test season 429-2','2019-04-29 07:04:52','2019-04-29 07:04:52'),(10,8,'Season - 429 - 3','This is test Season - 429 - 3','2019-04-29 07:04:10','2019-04-29 07:04:10'),(11,9,'test','test description','2019-04-29 12:04:38','2019-04-29 12:04:38');

/*Table structure for table `ci_subcategory` */

DROP TABLE IF EXISTS `ci_subcategory`;

CREATE TABLE `ci_subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subcategory_name` varchar(250) NOT NULL,
  `is_enabled` int(2) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL,
  `user_category_id` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `user_category_id` (`user_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `ci_subcategory` */

insert  into `ci_subcategory`(`id`,`subcategory_name`,`is_enabled`,`category_id`,`user_category_id`,`created_at`,`updated_at`) values (1,'subcat1',0,2,13,'2019-04-05 04:04:48','2019-04-05 04:04:48'),(2,'subcat2',0,3,11,'2019-04-05 04:04:19','2019-04-05 04:04:19'),(3,'subcat3',0,1,11,'2019-04-05 04:04:44','2019-04-05 04:04:44'),(4,'subcat1',0,2,13,'2019-04-12 06:04:19','2019-04-12 06:04:19'),(7,'frdertt',0,3,13,'2019-04-12 07:04:48','2019-04-12 07:04:48'),(8,'rtettrrttrt',0,4,13,'2019-04-12 07:04:31','2019-04-12 07:04:31'),(9,'subcat4',0,5,11,'2019-04-12 08:04:32','2019-04-12 08:04:32'),(10,'subcat456',0,5,13,'2019-04-12 08:04:32','2019-04-30 07:04:46'),(11,'testsubcat',0,5,0,'2019-04-30 07:04:25','2019-04-30 07:04:25');

/*Table structure for table `ci_tv_series` */

DROP TABLE IF EXISTS `ci_tv_series`;

CREATE TABLE `ci_tv_series` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `series_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `cover_image` text COLLATE utf8_unicode_ci NOT NULL,
  `image_appearance` text COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `season_id` int(20) NOT NULL,
  `episode_id` int(50) NOT NULL,
  `hashtags` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_tv_series` */

insert  into `ci_tv_series`(`id`,`series_name`,`cover_image`,`image_appearance`,`description`,`season_id`,`episode_id`,`hashtags`,`category_id`,`subcategory_id`,`created_at`,`updated_at`) values (6,'Test-423','http://13.127.134.33//uploads/images/thumb-100-the-division-1.jpg','large','Large',10,5,'#fighting',1,3,'2019-04-23','2019-04-23'),(7,'Test-424','http://13.127.134.33//uploads/images/img_forest.jpg','medium','This is test series on April 24',4,10,'#test',2,3,'2019-04-24','2019-05-01'),(8,'Test-429','http://13.127.134.33//uploads/images/1.jpg','medium','This is test TV shows.',3,15,'#test',2,1,'2019-04-29','2019-04-29'),(9,'test','http://13.127.134.33//uploads/images/9.jpg','small','test',1,1,'folder',1,3,'2019-04-29','2019-05-04'),(10,'test ios','http://13.127.134.33//uploads/images/jQuery-Plugin-For-Pretty-Image-Slider-With-Thumbnails-desoSlide.jpg','large','test',1,1,'1',2,1,'2019-04-29','2019-05-01');

/*Table structure for table `ci_user_category` */

DROP TABLE IF EXISTS `ci_user_category`;

CREATE TABLE `ci_user_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_category_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_category_check` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_discount` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_user_category` */

insert  into `ci_user_category`(`id`,`user_category_name`,`user_category_check`,`user_discount`) values (11,'Customer','No','9%'),(13,'Member','Yes','20%');

/*Table structure for table `ci_user_groups` */

DROP TABLE IF EXISTS `ci_user_groups`;

CREATE TABLE `ci_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `ci_user_groups` */

insert  into `ci_user_groups`(`id`,`group_name`) values (1,'member'),(2,'customer'),(4,'accountant');

/*Table structure for table `ci_user_watch_history` */

DROP TABLE IF EXISTS `ci_user_watch_history`;

CREATE TABLE `ci_user_watch_history` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `episode_id` int(20) NOT NULL,
  `movie_id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ci_user_watch_history` */

insert  into `ci_user_watch_history`(`id`,`episode_id`,`movie_id`,`user_id`,`created_at`,`updated_at`) values (1,0,6,3,'2019-04-29 07:04:48','2019-04-29 07:04:48'),(2,10,0,3,'2019-04-29 07:04:20','2019-04-29 07:04:20'),(3,15,0,3,'2019-04-29 07:04:35','2019-04-29 07:04:35'),(4,0,7,3,'2019-04-29 08:04:07','2019-04-29 08:04:07'),(5,0,4,3,'2019-04-29 08:04:11','2019-04-29 08:04:11'),(6,0,5,3,'2019-04-29 08:04:15','2019-04-29 08:04:15'),(7,0,4,3,'2019-04-29 08:04:21','2019-04-29 08:04:21'),(8,0,4,3,'2019-04-29 08:04:37','2019-04-29 08:04:37'),(9,0,8,3,'2019-04-29 08:04:57','2019-04-29 08:04:57'),(10,9,0,3,'2019-04-29 09:04:07','2019-04-29 09:04:07'),(11,2,0,3,'2019-04-29 09:04:12','2019-04-29 09:04:12'),(12,2,0,3,'2019-04-29 09:04:20','2019-04-29 09:04:20'),(13,33,0,3,'2019-04-29 09:04:47','2019-04-29 09:04:47'),(14,32,0,3,'2019-04-29 09:04:51','2019-04-29 09:04:51'),(15,35,0,3,'2019-04-29 09:04:50','2019-04-29 09:04:50'),(16,35,0,45,'2019-04-30 01:04:53','2019-04-30 01:04:53'),(17,0,8,45,'2019-04-30 01:04:05','2019-04-30 01:04:05'),(18,35,0,45,'2019-04-30 03:04:33','2019-04-30 03:04:33'),(19,0,8,45,'2019-04-30 05:04:07','2019-04-30 05:04:07'),(20,35,0,45,'2019-04-30 05:04:09','2019-04-30 05:04:09'),(21,0,8,45,'2019-04-30 01:04:07','2019-04-30 01:04:07'),(22,35,0,45,'2019-04-30 01:04:21','2019-04-30 01:04:21'),(23,35,0,450,'2019-04-30 01:04:35','2019-04-30 01:04:35'),(24,35,0,45044,'2019-04-30 01:04:39','2019-04-30 01:04:39'),(25,10,0,3,'2019-05-04 05:05:11','2019-05-04 05:05:11');

/*Table structure for table `ci_users` */

DROP TABLE IF EXISTS `ci_users`;

CREATE TABLE `ci_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `country` varchar(20) NOT NULL,
  `address` varchar(50) NOT NULL,
  `gender` text NOT NULL,
  `begin_date` varchar(50) NOT NULL,
  `end_date` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `photo_url` varchar(255) NOT NULL,
  `subscription_type` varchar(10) NOT NULL,
  `playlist_id` int(10) NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '1',
  `is_active` int(10) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `status` int(2) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `token` varchar(512) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `playlist_id` (`playlist_id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

/*Data for the table `ci_users` */

insert  into `ci_users`(`id`,`username`,`firstname`,`lastname`,`email`,`mobile_no`,`country`,`address`,`gender`,`begin_date`,`end_date`,`password`,`photo_url`,`subscription_type`,`playlist_id`,`role`,`is_active`,`is_admin`,`status`,`last_ip`,`token`,`created_at`,`updated_at`) values (3,'Admin','admin','admin','admin@admin.com','12345','','','0','2019-03-13 09:03:25','2019-03-13 09:03:25','$2y$10$kqiwRiCcIBsS/i0FC9k3YOE.sSVgu/PKCcO.baV8T4EDru4.qMXrS','','',0,1,1,1,1,'','','2017-09-29 10:09:44','2019-04-10 04:04:27'),(59,'YousifBassit','Yousif','Bassit','yousifbassit@gmail.com','12345','','','','','','$2y$10$m3bDcnt7S5KfCT/1.JXTfujUhev5pV/PX6itbaUWuhPzHdh7yruxO','http://13.127.134.33/uploadfiles/2019/04/20190415558562453.jpg','',0,1,1,0,0,'','cESScl8hmWc:APA91bFTUTwYiNPvSldhstSIwxhAQ73HBPwZo89oT1AADTljpSCgvDoJB6QrBX7nTqRuXYxW-GChm3WqCOaxeowLNgDnmr-BrmqOnweMaHzX4XqziJIXhvqLMz5trTlxVzWHfwoypvOx','2019-04-21 02:04:25','0000-00-00 00:00:00'),(45,'test1','Test','notification','beautistar1112@gmail.com','12345','China','','0','2019-03-13 09:03:25','2019-03-13 09:03:25','$2y$10$VpNtWjNnfq/hCz78gpuff.3ymhHyeAQQMBMwcDRN9W.wMH9W1PoaS','','',19,1,1,0,0,'','fPvT0-Pk1Io:APA91bEblzA_TFddHNqUPOZ4W8raVLcb-PCC4opig48J3Qjkly9pWsQNaFOAKJYTLKfCnJ7ItQsxJKah9SWEEaJdwaZwd-6bjSHe2HIQ7C13pvKEkGRtFudVLY5SSsjoY6XbfG95jexw','2019-02-07 12:02:55','2019-02-07 12:02:55'),(62,'testuser','test','test','test123@gmail.com','5657768678','China','fdkfdlfklfkldf','female','2019-04-26 02:41','2019-05-31 02:41','$2y$10$gNTKrnGHq.A7diXq2poK7.hPfDkDq71Pwd2bC2/iqfmABczSkbA3K','http://13.127.134.33/uploads/profile-pictures/1556270199.png','paid',0,11,0,0,1,'','','2019-04-26 09:04:39','2019-04-26 09:04:39'),(63,'12341234','1234','1234','1234@gmail.com','','','','','','','$2y$10$F0oevpEs8CrJ0b7PwuZkKOWYKx3OKLFDQyPv/cC7kQDcQ4R0XkaO6','http://13.127.134.33/uploadfiles/2019/04/20190415564743306.png','',0,1,0,0,0,'','','2019-04-28 05:04:50','0000-00-00 00:00:00'),(64,'1234512345','12345','12345','12345@gmail.com','','','','','','','$2y$10$6KrR0JD0ATMX/o3nVWKDo.k55/9rCT9gWjuhIURR1z280pUXjdswq','http://13.127.134.33/uploadfiles/2019/04/20190415564744239.jpg','',0,1,0,0,0,'','','2019-04-28 06:04:23','0000-00-00 00:00:00'),(65,'adityakumar','aditya','kumar','aditkuma@gmail.com','','','','','','','$2y$10$ImLiXfD/lOZBGyPuH3Lnu.1d5yP31a0S05sPDKKk4pWwR5tGrmpL2','http://13.127.134.33/uploadfiles/2019/04/20190415565574491.jpg','',0,1,0,0,0,'','cgUjh8UC_aw:APA91bHkfjG11nEaPet8DWMa16v-DAjbPLYXw2tGbNJOhAR1TK0LPmC1GGf6OcUabVGndlTFzwEBtrvQbwTUOYnW76nNKB0oDpV8iXvYB527A5CZ2VQ2vW44T30_vMw5eRvSmru5whFL','2019-04-29 05:04:09','0000-00-00 00:00:00'),(60,'YinTest','Yin','Test','test@user.com','12345','China','','','','','$2y$10$F3E2O/KEoCRGrT1fqvXxXeBhMGUb4JhxLLiaIBcuxjIgELN5Jq8V2','http://13.127.134.33/uploadfiles/2019/04/20190415558587061.jpg','',0,1,0,0,0,'','c4sRyqbtbvE:APA91bGtZsUgoZojexu3vTgcA2AeyrVwU9Xj0mWyow94Ou4LkesK2Ac93XPh1amk8_S7c5SqDadbKXXUqsq9SwWQXm-9RZVCL3uyoNtqjgg9QfYU_0WCQQwTn1xgiC5FVPG_O6xUi-iP','2019-04-21 02:04:26','0000-00-00 00:00:00');

/*Table structure for table `ci_video` */

DROP TABLE IF EXISTS `ci_video`;

CREATE TABLE `ci_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `video_title` varchar(200) NOT NULL,
  `video_description` varchar(400) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `video_upload` varchar(500) NOT NULL,
  `file_type` text NOT NULL,
  `playlist_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

/*Data for the table `ci_video` */

insert  into `ci_video`(`id`,`user_id`,`video_title`,`video_description`,`category_id`,`subcategory_id`,`video_upload`,`file_type`,`playlist_id`,`created_at`,`updated_at`) values (19,35,'test video 2','test2',1,1,'http://web24.live:8080/get.php?username=2493496367&password=xrhJ1Dgy9U&type=m3u_plus&output=ts','URL',15,'2019-01-30','2019-01-30'),(24,41,'Not play','Not play',1,1,'http://web24.live:8080/get.php?username=2493496367&password=xrhJ1Dgy9U&type=m3u_plus&output=ts','URL',20,'2019-01-30','2019-01-30'),(27,41,'test video file','AAAAAA',2,2,'http://13.127.134.33/uploads/videos/1548924963.MP4','File',20,'2019-01-31','2019-01-31'),(28,45,'list111','list111',1,1,'http://13.127.134.33/uploads/videos/1548927853.mp4','File',3,'2019-01-31','2019-01-31'),(30,62,'test','testvideo',4,0,'http://13.127.134.33/uploads/videos/1554723120.mp4','File',8,'2019-03-29','2019-04-08'),(31,60,'test','test',3,7,'http://13.127.134.33/uploads/videos/1554785166.mp4','File',22,'2019-03-30','2019-04-09'),(51,62,'test','gjlfgjlkjkghkjh',1,3,'http://13.127.134.33/uploads/videos/1554201985.mp4','File',10,'2019-04-02','2019-04-02'),(52,44,'test','ftrgyytrytry',1,3,'http://13.127.134.33/uploads/videos/1554785166.mp4','File',3,'2019-04-08','2019-04-09'),(53,45,'test','ghfghnfghnghfhhh',2,1,'http://13.127.134.33/uploads/videos/1556524250.mp4','File',25,'2019-04-29','2019-04-29'),(54,62,'testing video for playlist','testting video for playlist',2,2,'https://youtu.be/p2E30wWcBjY','URL',7,'2019-04-29','2019-04-29'),(55,45,'111','111',1,3,'http://13.127.134.33/uploads/videos/1556526972.mp4','File',25,'2019-04-29','2019-04-29'),(56,45,'111','111111111',1,3,'http://13.127.134.33/uploads/videos/1556535647.mp4','File',23,'2019-04-29','2019-04-29');

/*Table structure for table `ci_video_list` */

DROP TABLE IF EXISTS `ci_video_list`;

CREATE TABLE `ci_video_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_title` varchar(200) NOT NULL,
  `videos` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

/*Data for the table `ci_video_list` */

insert  into `ci_video_list`(`id`,`video_title`,`videos`,`user_id`,`created_at`,`updated_at`) values (3,'Playlist2','test3',45,'2019-01-24','2019-01-24'),(7,'list-2','',62,'2019-01-25','0000-00-00'),(8,'list-1','',62,'2019-01-26','0000-00-00'),(9,'list-5','',34,'2019-01-26','2019-01-29'),(10,'tre','',62,'2019-01-26','0000-00-00'),(12,'test','',33,'2019-01-26','0000-00-00'),(13,'testddddd','',33,'2019-01-26','0000-00-00'),(15,'remote','',35,'2019-01-28','0000-00-00'),(16,'test','',36,'2019-01-28','0000-00-00'),(17,'123','',12,'2019-01-29','0000-00-00'),(18,'qer','',43,'2019-01-29','0000-00-00'),(19,'gffdx','',33,'2019-01-30','0000-00-00'),(20,'123','',45,'2019-01-30','0000-00-00'),(21,'tres','',45,'2019-02-13','0000-00-00'),(22,'helloo','',60,'2019-02-13','2019-04-09'),(23,'test','',45,'2019-02-13','0000-00-00'),(24,'yyyyy','',45,'2019-02-13','0000-00-00'),(25,'12','',45,'2019-02-27','0000-00-00'),(26,'k','',45,'2019-03-07','0000-00-00'),(27,'24 mE','',45,'2019-03-12','0000-00-00'),(28,'http://web24.live:8080/get.php?username=2493496367&password=xrhJ1Dgy9U&type=m3u_plus&output=ts','',45,'2019-03-12','2019-04-08'),(29,'hii','',44,'2019-03-12','0000-00-00'),(30,'hhhnn','',44,'2019-03-12','2019-04-08'),(31,'4','',35,'2019-03-12','0000-00-00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
