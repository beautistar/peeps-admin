<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Seasons extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('admin/video_model', 'video_model');
        $this->load->model('admin/video_listing_model', 'video_listing_model');
        $this->load->model('admin/subcategory_model', 'subcategory_model');
        $this->load->model('admin/category_model', 'category_model');
        $this->load->model('admin/tvshow_video_model', 'tvshow_video_model');
        $this->load->model('admin/episode_model', 'episode_model');
        $this->load->model('admin/notification_model', 'notification_model');
        $this->load->model('admin/season_model', 'season_model');

    }

    public function index()
    {
        $data['all_seasons'] =  $this->episode_model->get_all_seasons();
        $data['all_episodes'] = $this->episode_model->get_all_episodes();
        $data['title'] = 'Episode List';
        $data['view'] = 'admin/episodes/episode_list';
        $this->load->view('admin/layout', $data);
    }
    
    public function seasons_by_series_id($id = 0) {
        
        $data['all_seasons'] =  $this->season_model->get_seasons_by_series_id($id);
        $data['series_id'] = $id;
        $data['title'] = 'Season List';
        $data['view'] = 'admin/seasons/season_list';
        $this->load->view('admin/layout', $data);
        
    }

    // $id : series id
    public function add($id=0){        
        
        if($this->input->post('submit')){
            
            
            $this->form_validation->set_rules('name', 'Season Name', 'trim|required');
            $this->form_validation->set_rules('description', 'Description', 'trim|required');
            if ($this->form_validation->run() == FALSE)
            {
                $data['series_id'] = $id;
                $data['title'] = 'Add Season';
                $data['view'] = 'admin/seasons/season_add';
                $this->load->view('admin/layout', $data);
            }else{
                 $data = array(
                    'series_id' => $id,
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description'),                     
                    'created_at' => date('Y-m-d h:m:s'),
                    'updated_at' => date('Y-m-d h:m:s')
                );
                $data = $this->security->xss_clean($data);
                $result = $this->season_model->add($data);
                if($result)
                {
                    $this->session->set_flashdata('msg', 'Season has been added sucessfully!');                     
                    redirect(base_url('admin/seasons/seasons_by_series_id/'.$id));                     
                }
                
            }
        }else{

            $data['series_id'] = $id;
            $data['title'] = 'Add Season';
            $data['view'] = 'admin/seasons/season_add';
            $this->load->view('admin/layout', $data);
        }
    }
    
    
    //Edit
    public function edit($id = 0){
        
        if($this->input->post('submit')){
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('description', 'Description', 'trim|required');
            
            if ($this->form_validation->run() == FALSE) {
                $data['season'] = $this->season_model->get_season_by_id($id);
                $data['title'] = 'Edit Season';
                $data['view'] = 'admin/seasons/season_edit';
                $this->load->view('admin/layout', $data);
            }
            else{
                $series_id = $this->input->post('series_id');
                $data = array(
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description'),                    
                    'updated_at' => date('Y-m-d h:m:s')
                );
                $data = $this->security->xss_clean($data);
                $result = $this->season_model->edit_season($data, $id);
                if($result){
                    $this->session->set_flashdata('msg', 'Season has been Updated Successfully!');
                    redirect(base_url('admin/seasons/seasons_by_series_id/'.$series_id));
                }
            }
            
        } else {
            
            $data['season'] = $this->season_model->get_season_by_id($id);
            $data['title'] = 'Edit Season';
            $data['view'] = 'admin/seasons/season_edit';
            $this->load->view('admin/layout', $data);
        }
    }
    
    //Delete
    public function del($id=0){
        $series_id = $this->db->where('id', $id)->get('ci_seasons')->row_array()['series_id'];
        $this->db->delete('ci_seasons', array('id' => $id));
        $this->db->delete('ci_episode', array('season_id' => $id));
        $this->session->set_flashdata('msg', 'Season has been Deleted Successfully!');
        redirect(base_url('admin/seasons/seasons_by_series_id/'.$series_id));
    }    

}

?>
