<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h4><i class="fa fa-pencil"></i> &nbsp; Edit Playlist</h4>
        </div>
		<div class="col-md-6 text-right">
		<?php foreach($playlists as $playlist){  }?>
		<a href="<?= base_url('admin/users/view_playlist/'.$playlist['user_id']); ?>" class="btn btn-success"> Back</a>
		</div>
        <?php /*<div class="col-md-4 text-left">
          <a href="<?= base_url('admin/videos'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Video List</a>
          <a href="<?= base_url('admin/videos/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add New Video</a>
        </div> */?>
        
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; 
			foreach($playlists as $playlist){
				$id = $playlist['id'];
				$video_title = $playlist['video_title'];
				$videos = $playlist['videos'];
				$userid = $playlist['user_id'];
				$filetype = $playlist['file_type'];
				$video = $playlist['video_upload'];
			}?>
            <?php echo form_open_multipart(base_url('admin/videos_listing/edit_playlist/'.$id), 'class="form-horizontal"' )?> 
				<div class="form-group">
					<label for="video_title" class="col-sm-2 control-label">Playlist Title</label>
					<div class="col-sm-9">
					  <input type="text" name="video_title" value="<?= $video_title; ?>" class="form-control" id="video_title" placeholder="">
					</div>
				</div>
						<div class="form-group">
					<label for="video" class="col-sm-2 control-label">Type</label>
					<div class="col-sm-4">
					   <input id="file" name="file" type="radio" class="file" value="file" <?php if($filetype == "File"){echo "checked"; } ?>/> File
					</div>
					<div class="col-sm-4">
					  <input id="url" name="file" type="radio" class="url" value="url" <?php if($filetype == "URL"){echo "checked"; } ?>/> Url
					</div>
				</div>
				<div class="form-group">
					<label for="video" class="col-sm-2 control-label">Current Video File/URL</label>
					<div class="col-sm-9">	
						<label style="padding:6px 12px;"><?php echo $video ?></label>
					</div>
				</div>
				
				<div class="form-group">
					<label for="video" class="col-sm-2 control-label">Add a video</label>
					<div class="col-sm-9">	
						<input type="file" name="video_update" class="form-control" id="video_file" placeholder="" />
						<input type="text" id="video_url"  name="video_url" class="form-control" style="display:none;">
					</div>
				</div>
				<input type="hidden"  id="old"  name="user_id"  value="<?php echo $userid;?>">
				<input type="hidden"  id="old"  name="videos"  value="<?php echo $videos;?>">
				<input type="hidden"  id="old"  name="old_video"  value="<?php echo $playlist['video_upload'];?>">
				<input type="hidden"  id="old"  name="old_filetype"  value="<?php echo $playlist['file_type'];?>">
				<div class="form-group">
					<div class="col-md-11">
					  <input type="submit" name="submit" value="Update" class="btn btn-info pull-right">
					</div>
				</div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  
</section>
<script>
$(document).ready(function(){
	$('#url').on('click',function(){
		$('#video_file').hide();
		$('#video_url').show();
	});
	$('#file').on('click',function(){
		$('#video_url').hide();
		$('#video_file').show();
	});
});
</script> 
