<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('admin/user_model', 'user_model');
		$this->load->model('admin/notification_model', 'notification_model');
		$this->load->model('admin/user_category_model', 'user_category_model');
		$this->load->model('admin/tvshow_video_model', 'tvshow_video_model');
	}
	
	function sendPush($user_id, $title, $body) {
    
    // send FCM push notification
    
    $url = "https://fcm.googleapis.com/fcm/send";
    $api_key = "AAAArlvJd04:APA91bGB6h4Rf5B1imiYnzYuquziITAuXqrsGU6cBpooVTxXBVi4Vc_zt4RNNapK4INHeSixFXgzXasPpGKz-KX6ik-wff8Y0QeTPeFjAjLjyiagrT2gimmytvkFBD3zXMSLHg3YyOzp";
    
    $token = "";
    $token = $this->user_model->getToken($user_id);
    
    if (strlen($token) == 0 ) {

        return;
    }
    
    /*
    if(is_array($target)){
        $fields['registration_ids'] = $target;
    } else{
        $fields['to'] = $target;
    }

    // Tpoic parameter usage
    $fields = array
                (
                    'to'  => '/topics/alerts',
                    'notification'          => $msg
                );
    $data = array('msgType' => $type,
                  'content' => $content);
    */
    $msg = array
            (
                'body'          => $body,
                'title'         => $title,   
                'badge'         => 1,             
                'sound'         => 'default'/*Default sound*/
            );
            
    $data = array('msgType'     => "notification",
                  'content'     => $body);
                  
    $fields = array
        (
            //'registration_ids'    => $tokens,
            'to'                => $token,
            'notification'      => $msg,
            'priority'          => 'high',
            'data'              => $data
        );

    $headers = array(
        'Authorization: key=' . $api_key,
        'Content-Type: application/json'
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);

    //@curl_exec($ch);
    
    $result['result'] = curl_exec($ch); 

    curl_close($ch); 
    
    return $result;
    
    }
    
    public function index(){
  
		$data['all_notifications'] =  $this->notification_model->get_all_notifications_list();
		$data['title'] = 'Notifications List';
		$data['view'] = 'admin/notifications/notification_list';
		$this->load->view('admin/layout', $data);
	}

	//---------------------------------------------------------------
	//  Add User
	public function add(){
		$data['all_users'] =  $this->user_model->get_all_users();
		$data['all_user_category'] =  $this->user_category_model->get_all_user_category();
		if($this->input->post('submit')){
			$this->form_validation->set_rules('title', 'title', 'trim|required');
			$this->form_validation->set_rules('description', 'description', 'trim|required');
			//$this->form_validation->set_rules('to_user_id', 'Select User', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data['title'] = 'Add Notification';
				$data['view'] = 'admin/notifications/notification_add';
				$this->load->view('admin/layout', $data);
			}
			else{
				$users = $this->input->post('to_user_id');
				//echo "<pre>"; print_r($users); echo "</pre>";
				foreach($users as $user){
					$array = explode(',', $user);
				}
				foreach($array as $user_id){
					if(!empty($user_id)){
						$data = array(
							'title' => $this->input->post('title'),
							'description' => $this->input->post('description'),
							'to_user_id' => $user_id,
							//'device_sound' =>$this->input->post('mobile_sound'),
							'color' => $this->input->post('color'),
							'created_at' => date('Y-m-d : h:m:s'),
							'updated_at' => date('Y-m-d : h:m:s'),
						);
						//echo "<pre>"; print_r($data); echo "</pre>";
						$data = $this->security->xss_clean($data);
						$userData = $this->user_model->get_user_by_id($user);
						$response['push'] = $this->sendPush($user_id, $this->input->post('title'), $this->input->post('description')); 
						//print_r($response);
						$result = $this->notification_model->add_notification($data);
					}
				}
				if($result)
				{
					$this->load->library('email');
					$this->email->from('apncoders@gmail.com', 'Peeps');
					$this->email->to($userData['email']);
					$this->email->subject($this->input->post('title'));
					$this->email->message( $this->input->post('description'));
					if($this->email->send())
					{
						$this->notification->send();
						$this->session->set_flashdata('msg', 'Notification has been Added Successfully!');
					}
					else
					{
						$this->session->set_flashdata('msg', 'Unable to Send Email, Notification has been Added Successfully!');
					} 
					redirect(base_url('admin/notifications'));
				 } 
			}
		}
		else
		{
			$data['title'] = 'Add Notification';
			$data['view'] = 'admin/notifications/notification_add';
			$this->load->view('admin/layout', $data);
		}

	}
	
	//  Add User
	public function add_by_season($id=null){
		$data['all_users'] =  $this->user_model->get_all_users();
		$data['all_user_category'] =  $this->user_category_model->get_all_user_category();
		$data['all_seasons'] =  $this->notification_model->get_all_seasons($id);
		if($this->input->post('submit')){
			$this->form_validation->set_rules('title', 'title', 'trim|min_length[5]|required');
			$this->form_validation->set_rules('description', 'description', 'trim|required');
			//$this->form_validation->set_rules('to_user_id', 'to_user_id', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data['title'] = 'Add Notification';
				$data['view'] = 'admin/notifications/notification_add_by_season';
				$this->load->view('admin/layout', $data);
			}
			else{
				$favdata = $this->notification_model->get_favourite_data($id);
				foreach($favdata as $fav){
					$data = array(
						'season_id' => $id,
						'token' => $fav['token'],	
						'title' => $this->input->post('title'),
						'description' => $this->input->post('description'),
						'to_user_id' => $fav['user_id'],
						'device_sound' =>$this->input->post('mobile_sound'),
						'color' => $this->input->post('color'),
						'created_at' => date('Y-m-d : h:m:s'),
						'updated_at' => date('Y-m-d : h:m:s'),
					);	
					$data = $this->security->xss_clean($data);
					if($fav['mark_fav'] != "no"){
						$result = $this->notification_model->add_notification($data);
						if($result){
							$this->load->library('notification');
							$registrationIds = array($fav['token']);
							$msg = array(
								'message' 	=> $this->input->post('description'),
								'title'		=> $this->input->post('title'),
								'subtitle'	=> $this->input->post('title'),
								'tickerText'	=> 'Ticker text here..',
								'vibrate'	=> 1,
								'sound'		=> 1,
								'largeIcon'	=> 'large_icon',
								'smallIcon'	=> 'small_icon'
							);
							if($this->notification->send($registrationIds, $msg)){
								$this->session->set_flashdata('msg', 'Notification has been sucessfully send to users who marked this series as favourite!');
							}else
							{
								$this->session->set_flashdata('msg', 'Unable to Send notifications to users, Notification has been Added Successfully!');
							} 
						}							
					}
				}
				redirect(base_url('admin/notifications'));
			}
		}
		else
		{
			$data['title'] = 'Add Notification';
			$data['view'] = 'admin/notifications/notification_add_by_season';
			$this->load->view('admin/layout', $data);
		}

	}

	//---------------------------------------------------------------
	//  Edit Notification
	public function edit($id = 0){
		$data['all_users'] =  $this->user_model->get_all_users();
		if($this->input->post('submit')){
			$this->form_validation->set_rules('title', 'title', 'trim|required');
			$this->form_validation->set_rules('description', 'description', 'trim|required');
			$this->form_validation->set_rules('to_user_id', 'to_user_id', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$data['notification'] = $this->notification_model->get_notification_by_id($id);
				//$data['notification_groups'] = $this->notification_model->get_notification_groups();
				$data['title'] = 'Edit User';
				$data['view'] = 'admin/notifications/notification_edit';
				$this->load->view('admin/layout', $data);
			}
			else{
				$data = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'to_user_id' => $this->input->post('to_user_id'),
					'device_sound' => $this->input->post('mobile_sound'),
					'color'  => $this->input->post('color'),
					'updated_at' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$userData = $this->user_model->get_user_by_id($this->input->post('to_user_id'));
				$result = $this->notification_model->edit_notification($data, $id);
				$this->sendPush($this->input->post('to_user_id'), $this->input->post('title'), $this->input->post('description'));
				if($result)
				{?>
					<script>
						alert('Notification has been sent..');
						window.location.href = "<?php echo base_url('admin/notifications'); ?>";
					</script>
					<?php 
						/* $this->load->library('email');
						$this->email->from('apncoders@gmail.com', 'Peeps');
						$this->email->to($userData['email']);
						$this->email->subject($this->input->post('title'));
						$this->email->message( $this->input->post('description'));
						if($this->email->send())
						{
							$this->session->set_flashdata('msg', 'Notification has been Added Successfully!');
						}
						else
						{
							$this->session->set_flashdata('msg', 'Unable to Send Email, Notification has been Added Successfully!');
						}
						redirect(base_url('admin/notifications')); */
				}
			}
		}
		else{
			$data['notification'] = $this->notification_model->get_notification_by_id($id);
			//$data['notification_groups'] = $this->notification_model->get_notification_groups();
			$data['title'] = 'Edit Notification';
			$data['view'] = 'admin/notifications/notification_edit';
			$this->load->view('admin/layout', $data);
		}
	}

	//---------------------------------------------------------------
	//  Delete Notifications
	public function del($id = 0){
		$this->db->delete('ci_notifications', array('id' => $id));
		$this->session->set_flashdata('msg', 'Notification has been Deleted Successfully!');
		redirect(base_url('admin/notifications'));
	}
	
}
?>
