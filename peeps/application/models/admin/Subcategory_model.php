<?php
	class Subcategory_model extends CI_Model{

		public function add_subcategory($data){
			$this->db->insert('ci_subcategory', $data);
			return true;
		}
/* 
	 	public function get_all_subcategory(){
			//$this->db->where('is_admin', 0);
			$this->db->select ('ci_subcategory.*,ci_category.category_name');
			$this->db->from('ci_subcategory');
			$this->db->join('ci_category', 'ci_category.id = ci_subcategory.category_id',"LEFT");
			$query = $this->db->get();
			//$result = $query->result_array();	
			return $result = $query->result_array();
		}	  */
		
		public function get_all_subcategory(){
			$this->db->select('ci_subcategory.*,ci_category.category_name,ci_user_category.user_category_name');
			$this->db->from('ci_subcategory');
			$this->db->join('ci_category','ci_category.id = ci_subcategory.category_id',"LEFT");
			$this->db->join('ci_user_category','ci_user_category.id = ci_subcategory.user_category_id',"LEFT");
			$query = $this->db->get();
			return $result = $query->result_array();
		}
		
	/* 	public function get_all_subcategory(){
			//$this->db->where('is_admin', 0);
			$this->db->select ('ci_subcategory.*,ci_category.category_name,ci_user_category.*');
			$this->db->from('ci_subcategory');
			$this->db->join('ci_category', 'ci_category.id = ci_subcategory.category_id',"LEFT");
			$this->db->join('ci_user_category', 'ci_subcategory.user_category_id = ci_user_category.id',"RIGHT");
			$query = $this->db->get();
			$last = $this->db->last_query();
			//$result = $query->result_array();	
			return $result = $query->result_array();
		} */
		
		
		/* public function get_all_category_for_csv(){
			$this->db->where('is_admin', 0);
			$this->db->select('id, category_name');
			$this->db->from('ci_subcategory');
			$query = $this->db->get();
			return $result = $query->result_array();
		} */


		public function get_subcategory_by_id($id){
			$query = $this->db->get_where('ci_subcategory', array('id' => $id));
			return $result = $query->row_array();
		}

		public function edit_subcategory($data, $id){
			$this->db->where('id', $id);
			$this->db->update('ci_subcategory', $data);
			return true;
		}

		/* public function get_user_groups(){
			$query = $this->db->get('ci_user_groups');
			return $result = $query->result_array();
		} */
		
		public function get_user_category(){
			$this->db->select ('ci_user_category.*');
			$this->db->from('ci_user_category');
			$this->db->join('ci_subcategory', 'ci_user_category.id = ci_category.user_category_id',"LEFT");
			//$this->db->where('ci_user_category.id =','ci_category.user_category_id');
			$query = $this->db->get();
			$last = $this->db->last_query();
			//echo $last;
			return $result = $query->row_array();
		}

	}

?>
