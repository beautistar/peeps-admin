<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body with-border">
        <div class="col-md-6">
          <h4><i class="fa fa-plus"></i> &nbsp; Add New Notification</h4>
        </div>
        <div class="col-md-6 text-right">
          <a href="<?= base_url('admin/notifications'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Notifications List</a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>

            <?php echo form_open(base_url('admin/notifications/add'), 'class="form-horizontal"');  ?>
              <div class="form-group">
                <label for="color" class="col-sm-2 control-label">Color of light in the mobile</label>
                <div class="col-sm-9">
					<select name="color" class="form-control">
						<option selected>Select color</option>
						<option value="red">Red</option>
						<option value="blue">Blue</option>
						<option value="yellow">Yellow</option>
					</select>
                </div>
              </div> 
			  <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-9">
                  <input type="text" name="title" class="form-control" id="title" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Description</label>

                <div class="col-sm-9">
                  <?php
                  $data = array(
                                'name'        => 'description',
                                'id'          => 'description',
                                'value'       => set_value('description'),
                                'rows'        => '10',
                                'cols'        => '10',
                                'class'       => 'form-control'
                                );

                  echo form_textarea($data);
                  ?>
                </div>
              </div>

              <div class="form-group">
                  <label for="to_user_id" class="col-sm-2 control-label">Select User</label>
                  <div class="col-sm-9">
                    <select name="to_user_id[]" class="form-control" multiple>
                      <option value="">Select User</option>
						<option value="<?php foreach($all_users as $user){
							echo $user['id'].',';
						}
						foreach($all_user_category as $cat){
							echo $cat['id'].',';
						}
						?>">All</option>
                      <?php foreach($all_users as $user): ?>
                        <option value="<?= $user['id']; ?>"><?= $user['firstname']." ".$user['lastname']; ?></option>
                      <?php endforeach; ?> 
					  <?php foreach($all_user_category as $cat): ?>
                        <option value="<?= $cat['id']; ?>"><?= $cat['user_category_name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <!--input type="text" name="to_user_id" class="form-control" id="to_user_id" placeholder=""-->


              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Send Notification" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>

</section>
