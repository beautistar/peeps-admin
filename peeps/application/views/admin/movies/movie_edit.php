<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-5">
          <h4><i class="fa fa-pencil"></i> &nbsp; Edit Movie</h4>
        </div>
		<div class="col-md-3 text-right">
			<a href="<?= base_url('admin/movies'); ?>" class="btn btn-success"> Back</a>
		</div>
        <div class="col-md-4 text-left">
          <a href="<?= base_url('admin/movies'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Movies List</a>
        </div>

      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           <?php foreach($video as $movie){?>
            <?php echo form_open_multipart(base_url('admin/movies/edit/'.$movie['id']), 'class="form-horizontal"' )?>
				<div class="form-group">
					<label for="name" class="col-sm-3 control-label">Name</label>
					<div class="col-sm-9">
					  <input type="text" name="name" value="<?= $movie['name']; ?>" class="form-control" id="name" placeholder="">
					</div>
				</div>
				<div class="form-group">
					<label for="description" class="col-sm-3 control-label">Description</label>
					<div class="col-sm-9">
					  <textarea type="text" id="description" name="description" class="form-control" rows="3" placeholder=""><?php echo $movie['description']; ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="hashtags" class="col-sm-3 control-label">Hashtags</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="hashtags" value="<?php echo $movie['hashtags']; ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="cover_image" class="col-sm-3 control-label">Cover Image</label>
					<div class="col-sm-3">
						<img src="<?php echo $movie['cover_image']; ?>" width="130px" height="100px">
						<input type="hidden" name="cover_image_old" value="<?php echo $movie['cover_image']; ?>">
					</div>
					<div class="col-sm-6">
						<input type="file" class="form-control" name="cover_image"  id="cover_image">
					</div>
				</div>
				<div class="form-group">
					<label for="image_appearance" class="col-sm-3 control-label">Image Size Appearance</label>
					<div class="col-sm-9">
						<select name="image_appearance" class="form-control">
							<option value="">Select Image Size</option>
							<option value="small" <?php echo ($movie['image_appearance'] == 'small')?"selected":"" ?>>Small</option>
							<option value="medium" <?php echo ($movie['image_appearance'] == 'medium')?"selected":"" ?>>Medium</option>
							<option value="large" <?php echo ($movie['image_appearance'] == 'large')?"selected":"" ?>>Large</option>
						</select>
					</div>
				</div>
				<div class="form-group">
				  <div class="col-sm-3">
					<label for="Category_name" class="control-label">Category Name</label>
				  </div>
				  <div class="col-sm-9">
					<select class="form-control" id="category-list" name="category_id">
					  <option value="">Select Category</option>
						<?php foreach($category_groups as $category): 
							if($category['belongs_to'] == 'movies'){
								if($category['id'] == $movie['category_id']): ?>
									<option value="<?= $category['id']; ?>" selected><?= $category['category_name']; ?></option>
								<?php else: ?>
									<option value="<?= $category['id']; ?>"><?= $category['category_name']; ?></option>
								<?php endif; 
							}
					  endforeach;  ?>
					</select>
				  </div>
				</div>
				<div class="form-group" id="subcategory_id_div" >
					<div class="col-sm-3">
						<label for="subcategory_name" class="control-label">Sub Category Name</label>
					</div>
					<div class="col-sm-9">
            	<?php /*?><input type="select" name="subcategory_id" value="<?= $subcategory['subcategory_name']; ?>" class="form-control" id="subcategory_id" placeholder=""> <?php */ ?>
            <select name="subcategory_id" id="subcategory_id" class="form-control">

						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="current" class="col-sm-3 control-label">Current Video</label>
					<div class="col-sm-9">
						<a href="<?php echo $movie['movie_video']; ?>"><span><?php echo $movie['movie_video']; ?></span></a>
						<input type="hidden" name="video_old" value="<?php echo $movie['movie_video']; ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="movie_video" class="col-sm-3 control-label">Add a video</label>
					<div class="col-sm-9">
						<input type="file" class="form-control" name="movie_video"  id="movie_video">
					</div>
				</div>
				<div class="form-group">
					<label for="duration" class="col-sm-3 control-label">Duration</label>
					<div class="col-sm-9">
					  <input type="text" class="form-control" name="duration"  id="duration" value="<?php echo $movie['duration']; ?>">
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-11">
					  <input type="submit" name="submit" value="Update" class="btn btn-info pull-right">
					</div>
				</div>
            <?php echo form_close(); ?>
			<?php }?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
<script>
/* JQuery to bind Subcategory according to Category selection */
$(document).ready(function() {
  //$('#subcategory_id_div').hide();
  //alert("<?php echo $subcategory['id'];?>");
  $('select[name="subcategory_id"]').append('<option value="<?php echo $subcategory['id']; ?>"><?php echo $subcategory['subcategory_name'];?></option>');
	$('#category-list').on('change', function() {
		var category_id = $(this).val();
		if(category_id) {
			$.ajax({
				url:"<?php echo base_url(); ?>admin/videos/subcategory/"+category_id,
				type: "GET",
				dataType: "json",
				success:function(response)
        {
          //('#subcategory-list').empty();
          $('#subcategory_id_div').show();
          $('select[name="subcategory_id"]').empty();
          $.each(response, function(index,value)
          {
            //alert(value.subcategory_name);
            $('select[name="subcategory_id"]').append('<option value="'+ value.id +'">'+ value.subcategory_name +'</option>');
          });
				}
			});
		}else{
      //$('#subcategory_id_div').hide();
			$('select[name="subcategory_id"]').empty();
		}
	});
});
</script>
<script>
$(document).ready(function(){
/* 	if ($('#file').attr("value") == "file") {
		//$('#filetype').val('File');
		$("#video_url").hide();
		$("#video_file").show();
	}else if($('#url').attr("value")== "url"){
		//$('#filetype').val('URL');
		$("#video_file").hide();
		$("#video_url").show();
	} */
	$('#url').on('click',function(){
		$('#video_file').hide();
		$('#video_url').show();
	});
	$('#file').on('click',function(){
		$('#video_url').hide();
		$('#video_file').show();
	});
});
</script>
