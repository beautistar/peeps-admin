<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class SubCategory extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/subcategory_model', 'subcategory_model');
			$this->load->model('admin/category_model', 'category_model');
			$this->load->model('admin/user_category_model', 'user_category_model');
		}

		public function index(){
			$data['all_subcategory'] =  $this->subcategory_model->get_all_subcategory();
			$data['user_category'] = $this->user_category_model->get_all_user_category();
			$data['title'] = 'Sub Category List';
			$data['view'] = 'admin/subcategory/subcategory_list';
			$this->load->view('admin/layout', $data);
		}

		//---------------------------------------------------------------
		//  Add Category
		public function add(){
			if($this->input->post('submit'))
			{
				//$user_cats = $this->input->post('category_access');
				$this->form_validation->set_rules('subcategory_name', 'Category Name', 'trim|min_length[5]|required');
				//$this->form_validation->set_rules('category_access', 'User Category', 'required');
				if ($this->form_validation->run() == FALSE)
				{
					$data['title'] = 'Add Sub Category';
					$data['user_category'] = $this->user_category_model->get_all_user_category();
					$data['view'] = 'admin/subcategory/subcategory_add';
					$this->load->view('admin/layout', $data);
				}
				else
				{
					/* foreach($user_cats as $cat){ */
					$data = array(
						'subcategory_name' => $this->input->post('subcategory_name'),
						'category_id' => $this->input->post('category_id'),
						//'user_category_id' => $cat,
						'created_at' => date('Y-m-d : h:m:s'),
						'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->subcategory_model->add_subcategory($data);
					/* } */
					if($result)
					{
						$this->session->set_flashdata('msg', 'Sub Category has been Added Successfully!');
						redirect(base_url('admin/subcategory'));
					}
				}
			}
			else
			{
				$data['category_groups'] = $this->category_model->get_all_category();
				$data['user_category'] = $this->user_category_model->get_all_user_category();
				$data['title'] = 'Add Sub Category';
				$data['view'] = 'admin/subcategory/subcategory_add';
				$this->load->view('admin/layout', $data);
			}
		}
		//---------------------------------------------------------------
		//  Edit Category
		public function edit($id = 0){
			if($this->input->post('submit')){
				$this->form_validation->set_rules('subcategory_name', 'subcategory_name', 'trim|required');
				//$this->form_validation->set_rules('firstname', 'Username', 'trim|required');
				//$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
				//$this->form_validation->set_rules('email', 'Email', 'trim|required');
				//$this->form_validation->set_rules('mobile_no', 'Number', 'trim|required');
				//$this->form_validation->set_rules('status', 'Status', 'trim|required');
				//$this->form_validation->set_rules('group', 'Group', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['subcategory'] = $this->subcategory_model->get_subcategory_by_id($id);
					//$data['user_groups'] = $this->user_model->get_user_groups();
					$data['user_category'] = $this->user_category_model->get_all_user_category();
					$data['title'] = 'Edit Sub Category';
					$data['view'] = 'admin/subcategory/subcategory_edit';
					$this->load->view('admin/layout', $data);
				}
				else{
					$data = array(
						'subcategory_name' => $this->input->post('subcategory_name'),
						'category_id' => $this->input->post('category_id'),
						//'user_category_id' => $this->input->post('category_access'),
						'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->subcategory_model->edit_subcategory($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'Sub Category has been Updated Successfully!');
						redirect(base_url('admin/subcategory'));
					}
				}
			}
			else{
				$data['subcategory'] = $this->subcategory_model->get_subcategory_by_id($id);
				$data['category_groups'] = $this->category_model->get_all_category();
				//$data['user_groups'] = $this->user_model->get_user_groups();
				$data['user_category'] = $this->user_category_model->get_all_user_category();
				$data['title'] = 'Edit Sub Category';
				$data['view'] = 'admin/subcategory/subcategory_edit';
				$this->load->view('admin/layout', $data);
			}
		}

		//---------------------------------------------------------------
		//  Delete Users
		public function del($id = 0){
			$this->db->delete('ci_subcategory', array('id' => $id));
			$this->session->set_flashdata('msg', 'Sub Category has been Deleted Successfully!');
			redirect(base_url('admin/subcategory'));
		}

		//---------------------------------------------------------------
		//  Export Users PDF
		/*public function create_users_pdf(){
			$this->load->helper('pdf_helper'); // loaded pdf helper
			$data['all_users'] = $this->user_model->get_all_users();
			$this->load->view('admin/users/users_pdf', $data);
		}*/

		//---------------------------------------------------------------
		// Export data in CSV format
		/* public function export_csv(){
		   // file name
		   $filename = 'users_'.date('Y-m-d').'.csv';
		   header("Content-Description: File Transfer");
		   header("Content-Disposition: attachment; filename=$filename");
		   header("Content-Type: application/csv; ");

		   // get data
		   $user_data = $this->user_model->get_all_users_for_csv();

		   // file creation
		   $file = fopen('php://output', 'w');

		   $header = array("ID", "Username", "First Name", "Last Name", "Email", "Mobile_no", "Created Date");
		   fputcsv($file, $header);
		   foreach ($user_data as $key=>$line){
		     fputcsv($file,$line);
		   }
		   fclose($file);
		   exit;
		  } */
	}


?>
