<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h4><i class="fa fa-pencil"></i> &nbsp; Edit User</h4>
        </div>
		<div class="col-md-6 text-right">
			<a href="<?= base_url('admin/users'); ?>" class="btn btn-success"> Back</a>
		</div>
        <?php /*<div class="col-md-4 text-left">
          <a href="<?= base_url('admin/users'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Users List</a>
          <a href="<?= base_url('admin/users/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add New Invoice</a>
        </div> */?>
        
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open_multipart(base_url('admin/users/edit/'.$user['id']), 'class="form-horizontal"' )?> 
              <div class="form-group">
                <label for="username" class="col-sm-3 control-label">User Name</label>
                <div class="col-sm-8">
                  <input type="text" name="username" value="<?= $user['username']; ?>" class="form-control" id="username" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-8">
                  <input type="text" name="firstname" value="<?= $user['firstname']; ?>" class="form-control" id="firstname" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="lastname" class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-8">
                  <input type="text" name="lastname" value="<?= $user['lastname']; ?>" class="form-control" id="lastname" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-8">
                  <input type="email" name="email" value="<?= $user['email']; ?>" class="form-control" id="email" placeholder="">
                </div>
              </div>
             <div class="form-group">
                <label for="mobile_no" class="col-sm-3 control-label">Mobile No</label>
                <div class="col-sm-8">
                  <input type="number" name="mobile_no" value="<?= $user['mobile_no']; ?>" class="form-control" id="mobile_no" placeholder="">
                </div>
              </div>  
			  <div class="form-group">
                <label for="country" class="col-sm-3 control-label">Country</label>
                <div class="col-sm-8">
                  <input type="text" name="country" value="<?= $user['country']; ?>" class="form-control" id="country" placeholder="">
                </div>
              </div>  
			  <div class="form-group">
                <label for="address" class="col-sm-3 control-label">Address</label>
                <div class="col-sm-8">
                  <input type="text" name="address" value="<?= $user['address']; ?>" class="form-control" id="address" placeholder="">
                </div>
              </div>  
			  <div class="form-group">
                <label for="gender" class="col-sm-3 control-label">Gender</label>
                <div class="col-sm-8">
					<input type="radio" name="gender" value="male" <?php echo ($user['gender']=='male')?'checked':'' ?> > Male
					<input type="radio" name="gender" value="female" <?php echo ($user['gender']=='female')?'checked':'' ?> > Female
                </div>
              </div> 
			<div class="form-group">
				<label for="role" class="col-sm-3 control-label">User Category</label>
				<div class="col-sm-8">
				  <select name="group" class="form-control">
					<option value="">Select Category</option>
					<?php foreach($user_category as $role): ?>
					  <?php if($role['id'] == $user['role']): ?>
					  <option value="<?= $role['id']; ?>" selected><?= $role['user_category_name']; ?></option>
					  <?php else: ?>
					  <option value="<?= $role['id']; ?>"><?= $role['user_category_name']; ?></option>
					  <?php endif; ?>
					<?php endforeach; ?>
				  </select>
				</div>
			</div>
			<div class="form-group">
				<label for="photo_url" class="col-sm-3 control-label">Profile Picture</label>
				<div class="col-sm-8">
					<img src="<?php echo $user['photo_url'] ?>" height="120px" width="120px">
					<input type="hidden" name="old_photo" value="<?php echo $user['photo_url']; ?>">		
					<input type="file" class="" name="photo_url"  id="photo_url" style="margin-top:10px;">
				</div>
			</div>
			<div class="form-group">
				<label for="subscription_type" class="control-label col-sm-3">Subscription Type</label>
				<div class="col-sm-8">
					<select name="subscription_type" class="form-control">
						<?php if($user['subscription_type'] == ''){ ?>
							<option value="" selected="selected">Select Subscription Type</option>
							<option value="paid">Paid</option>
							<option value="unpaid">Unpaid</option>
						<?php }else if($user['subscription_type'] == 'paid'){?>
							<option value="">Select Subscription Type</option>
							<option value="paid" selected="selected">Paid</option>
							<option value="unpaid">Unpaid</option>
						<?php }else{ ?>
							<option value="">Select Subscription Type</option>
							<option value="paid">Paid</option>
							<option value="unpaid" selected="selected">Unpaid</option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="begin_date" class="col-sm-3 control-label">Subscription Beginning Date</label>
				<div class="col-sm-8">
					<input type="hidden" name="old_begin_date" value="<?php echo $user['begin_date']; ?>">
                    <input type='text' class="form-control" name="begin_date" value="<?php echo $user['begin_date']; ?>"  disabled/>
				</div>
				<?php /*<div class="col-sm-2">
					<label class="control-label">Change Date</label>
				</div>
				<div class="col-sm-3">
					<div class='input-group date' id='datetimepicker1'>
						<input type='text' class="form-control" name="begin_date" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>*/?>
            </div> 
			<div class="form-group">
				<label for="end_date" class="col-sm-3 control-label">Subscription End Date</label>
				<div class="col-sm-8"> 
					<input type="hidden" name="old_end_date" value="<?php echo $user['end_date']; ?>">
                    <input type='text' class="form-control" name="end_date" value="<?php echo $user['end_date']; ?>"  disabled/>
				</div>
				<?php /*<div class="col-sm-2">
					<label class="control-label">Change Date</label>
				</div>
				<div class="col-sm-3">	
					<div class='input-group date' id='datetimepicker2'>
						<input type='text' class="form-control" name="end_date"/>
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div> */?>
            </div>
			<?php /*  <div class="form-group">
                <label for="playlist_id" class="col-sm-3 control-label">Playlist</label>
                <div class="col-sm-8">
                  <select name="playlist_id" class="form-control">
                    <option value="">Select Playlist</option>
                    <?php foreach($playlists as $playlist):?>
                      <?php if($playlist['id'] == $user['playlist_id']): ?>
                      <option value="<?= $playlist['id']; ?>" selected><?= $playlist['video_title']; ?></option>
                      <?php else: ?>
                      <option value="<?= $playlist['id']; ?>"><?= $playlist['video_title']; ?></option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
             <div class="form-group">
                <label for="role" class="col-sm-2 control-label">Select Status</label>

                <div class="col-sm-9">
                  <select name="status" class="form-control">
                    <option value="">Select Status</option>
                    <option value="1" <?= ($user['is_active'] == 1)?'selected': '' ?> >Active</option>
                    <option value="0" <?= ($user['is_active'] == 0)?'selected': '' ?>>Deactive</option>
                  </select>
                </div>
              </div>
             <div class="form-group">
                <label for="role" class="col-sm-2 control-label">Select Group</label>

                <div class="col-sm-9">
                  <select name="group" class="form-control">
                    <option value="">Select Group</option>
                    <?php foreach($user_groups as $group): ?>
                      <?php if($group['id'] == $user['role']): ?>
                      <option value="<?= $group['id']; ?>" selected><?= $group['group_name']; ?></option>
                      <?php else: ?>
                      <option value="<?= $group['id']; ?>"><?= $group['group_name']; ?></option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div> */?>
              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Update User" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  
</section> 
<script>
$(function () {
   $('#datetimepicker1').datetimepicker();
   $('#datetimepicker2').datetimepicker();
});
 </script>