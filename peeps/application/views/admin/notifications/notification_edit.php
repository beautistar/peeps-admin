<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h4><i class="fa fa-pencil"></i> &nbsp; Edit Notification</h4>
        </div>
		<div class="col-md-6 text-right">
			<a href="<?= base_url('admin/notifications'); ?>" class="btn btn-success"> Back</a>
		</div>
        <?php /*<div class="col-md-4 text-left">
          <a href="<?= base_url('admin/notifications'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Notifications List</a>
          <a href="<?= base_url('admin/notifications/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add New Invoice</a>
        </div> */?>

      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>

            <?php echo form_open(base_url('admin/notifications/edit/'.$notification['id']), 'class="form-horizontal"' )?>
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title</label>

                <div class="col-sm-9">
                  <input type="text" name="title" value="<?= $notification['title']; ?>" class="form-control" id="title" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Description</label>

                <div class="col-sm-9">
                  <?php
                  $data = array(
                                'name'        => 'description',
                                'id'          => 'description',
                                'value'       => $notification['description'],
                                'rows'        => '10',
                                'cols'        => '10',
                                'class'       => 'form-control'
                                );

                  echo form_textarea($data);
                  ?>
                </div>
              </div>

              <div class="form-group">
                  <label for="to_user_id" class="col-sm-2 control-label">Select User</label>
                  <div class="col-sm-9">
                    <select name="to_user_id" class="form-control">
                      <option value="">Select User</option>


                      <?php foreach($all_users as $user): ?>
                        <?php if($user['id'] == $notification['to_user_id']): ?>
                        <option value="<?= $user['id']; ?>" selected><?= $user['firstname']." ".$user['lastname']; ?></option>
                        <?php else: ?>
                        <option value="<?= $user['id']; ?>"><?= $user['firstname']." ".$user['lastname']; ?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>

                    </select>
                  </div>
                </div>

              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Resend Notification" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>

</section>
